import {
    logout,
    login
} from './../common/auth/loginPage';
import {
    getUsername
} from './auth/xhrRequests';
import getPath from './getPath';

export const openPage = ( desiredDestination = '/intern/' ) => {
    const destination = typeof desiredDestination === 'number' ? `/intern/zaak/${desiredDestination}` : desiredDestination;

    // when a save is in progress, the navigation will be interupted by an alert
    // this should be avoided by only navigating after the saves have been completed
    // however, this alert will cause all following tests to fail
    // therefor we simply accept the dialog so that the rest of the tests can at least run properly
    browser
        .get(destination)
        .catch(() =>
            browser
                .switchTo()
                .alert()
                .then(alert => {
                    alert.accept();

                    return browser.get(destination);
                })
        );
};

export const openPageAs = ( username = 'admin', destination = '/intern/' ) => {
    // using Promise.all([getUserName(), getPath()]) causes all sorts of issues with execution order
    getUsername().then(loggedInUser => {
        getPath().then(currentLocation => {
            const loggedInWithWrongUser = loggedInUser !== undefined && loggedInUser !== username;
            const notLoggedInOrWrongUser = loggedInUser === undefined || loggedInWithWrongUser;
            const noNeedToNavBeforeLogin = notLoggedInOrWrongUser && destination === '/intern/';

            // when logged in with the wrong user, we must first log out
            if ( loggedInWithWrongUser ) {
                logout();
            }

            // we navigate to the desired page if we're not already there
            // note:    this navigation is better done before the login,
            //          because it's quicker to reload the loginpage than the dashboard,
            //          with the exception of /intern/ because we will end up there anyways
            if ( destination !== currentLocation  && !noNeedToNavBeforeLogin ) {
                openPage(destination);
            }

            // when logged out we log in with the desired user
            if ( notLoggedInOrWrongUser ) {
                login(username);
            }
        });
    });
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
