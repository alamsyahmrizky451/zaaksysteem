import waitForElement from './../common/waitForElement';
import inputFileSimple from './../common/input/inputFileSimple';

export const dialog = $('.ui-dialog');

export const toggleActionMenu = () => {
    $('.block-header-actions').click();
};

export const waitForDialog = () => {
    waitForElement('.ui-dialog');
};

export const openAction = ( action, aDialogAction = true ) => {
    const options = $$('.block-header-actions .buttons a');

    toggleActionMenu();
    options
        .filter(option =>
            option
                .getText()
                .then(text =>
                    text === action
                )
        )
        .click();

    if (aDialogAction) {
        waitForDialog();
    }

};

export const closeDialog = () => {
    $('.ui-dialog .ui-dialog-titlebar-close .ui-icon-closethick').click();
};

export const closeModal = () => {
    $('.modal button.modal-close .mdi-close').click();
};

export const submitAction = () => {
    dialog.$('[type="submit"]').click();
    browser.sleep(5000);
};

export const createFolder = ( title ) => {
    openAction('Categorie');
    dialog.$('[name="naam"]').sendKeys(title);
    submitAction();
};

export const waitForMagicstring = () => {
    browser
        .driver
        .wait( () =>
            $('[name="kenmerk_magic_string"]').getAttribute('value').then( (text) =>
                text !== ''
            )
        );
};

export const createCaseAttribute = ( data ) => {
    openAction('Kenmerk');
    $('[name="kenmerk_naam"]').sendKeys(data.name);
    $('[name="kenmerk_type"]').sendKeys(data.type);
    waitForMagicstring();

    if (data.options) {
        data.options
            .forEach(option => {
                $('.multiple-options [name="addopt"] textarea').sendKeys(option);

                $('.multiple-options [name="addopt"] button').click();
            });
    }

    submitAction();
};

export const createEmailTemplate = ( data ) => {
    openAction('E-mail', false);
    $('[data-ng-model="label"]').sendKeys(data.name);
    $('[data-ng-model="subject"]').sendKeys(data.subject);
    $('[data-ng-model="message"]').sendKeys(data.content);
    $('[name="emailTemplateForm"] .form-actions button').click();
};

export const createDocumentTemplate = ( data ) => {
    openAction('Sjabloon');
    $('[name="naam"]').sendKeys(data.name);
    inputFileSimple($('[type="file"]'), 'casenumber.odt');
    submitAction();
   
};

export const startCreateCasetype = () => {
    openAction('Zaaktype', false);
};

export const startCreateObjecttype = () => {
    openAction('Objecttype', false);
};

export const startItemAction = (name, action, aDialogAction = true) => {
    const rows = $$('.table-closed tr');

    rows
        .filter(row =>
            row.getText().then(text =>
                text.indexOf(name) !== -1
            )
        )
        .each(row => {
            const itemActions = row.$$('.ezra_actie_container a');
            
            row.$('.ezra_dropdown_init').click();
            itemActions.filter(itemAction =>
                itemAction.getText().then(text =>
                    text === action
                )
            )
            .click();
        });

    if (aDialogAction) {
        waitForDialog();
    }

};

export const startEditItem = ( name, aDialogAction = true ) => {
    startItemAction(name, 'Bewerken', aDialogAction);
};

export const deleteItem = ( name, reason = 'Reason' ) => {
    startItemAction(name, 'Verwijderen');
    $('[name="commit-message"]').sendKeys(reason);
    submitAction();
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
