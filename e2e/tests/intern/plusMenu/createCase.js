import {
    openPageAs
} from './../../../functions/common/navigate';
import {
    createCase
} from './../../../functions/intern/plusMenu';
import {
    goNext
} from './../../../functions/common/form';

describe('when starting a case registration', () => {
    beforeAll(() => {
        const newCase = {
            casetype: 'basic casetype',
            requestorType: 'natuurlijk_persoon',
            requestor: '111222333'
        };

        openPageAs();
        createCase(newCase);
    });

    it('should redirect to zaak/create', () => {
        expect(browser.getCurrentUrl()).toContain('/intern/aanvragen/');
    });

    describe('and completing the steps', () => {
        beforeAll(() => {
            goNext();
        });

        it('should move to the next phase', () => {
            expect(browser.getCurrentUrl()).toContain('/bevestig/');
        });

        describe('and submitting the form', () => {
            beforeAll(() => {
                goNext();
            });

            it('should redirect to the case', () => {
                expect($('.case-view').isPresent()).toBe(true);
            });
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
