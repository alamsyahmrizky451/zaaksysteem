import {
    openPageAs
} from './../../../functions/common/navigate';
import {
    createWidget
} from './../../../functions/intern/plusMenu';

describe('when viewing a completely full dashboard and adding another widget', () => {

    beforeAll(() => {

        openPageAs('dashboardcompletelyfull');

        createWidget('Zoekopdracht', 'Does not exist');

    });

    it('there should be shown an error message', () => {

        $('.snack-message').getText()
            .then(text => {
                expect(text).toContain('vol');
            })
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
