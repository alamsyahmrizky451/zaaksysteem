import {
    openPageAs
} from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    goNext,
    getAssignmentType,
    getAssignmentDepartment,
    getAssignmentRole,
    assign
} from './../../../../functions/common/form';
import inputDate from './../../../../functions/common/input/inputDate';
import {
    openTab
} from './../../../../functions/intern/caseView/caseNav';
import {
    getAboutValue
} from './../../../../functions/intern/caseView/caseMenu';
import {
    checkDocument
} from './../../../../functions/intern/caseView/caseDocuments';

const choice = $('[data-name="fase_actie_keuze"]');
const date = $('[data-name="fase_actie_datum"]');

describe('when opening a registration form with the phase actions and activating the phase actions', () => {

    beforeAll(() => {

        openPageAs();

        const data = {
            casetype: 'Fase actie bij registratie',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

        choice.$('[value="Template"]').click();
        choice.$('[value="Email"]').click();
        choice.$('[value="Case"]').click();
        choice.$('[value="Allocation"]').click();
        choice.$('[value="Contact"]').click();
        inputDate(date, '01-01-2017');
        choice.$('[value="Change date of registration"]').click();

        goNext();

    });

    it('the allocation should have been set', () => {

        expect(getAssignmentType()).toEqual('org-unit');
        expect(getAssignmentDepartment()).toEqual('-Frontoffice');
        expect(getAssignmentRole()).toEqual('Behandelaar');

    });

    describe('and when registaring the case', () => {
    
        beforeAll(() => {

            assign('me', { changeDepartment: false });
    
            goNext();
    
        });
    

        it('the template should have been created', () => {

            openTab('docs');

            expect(checkDocument('Fase actie sjabloon.odt')).toBe(true);

            openTab('phase');

        });

        // this testscenarios will be added when the timeline tab are upgraded

        it('the email should have been sent', () => {

        });

        it('the case should have been created', () => {

            openTab('relations');

            expect($('.related-cases a').isPresent()).toBe(true);

            openTab('phase');

        });

        it('the registration date and target date should have been changed', () => {

            expect(getAboutValue('Registratiedatum')).toEqual('01-01-2017');
            expect(getAboutValue('Streefafhandeldatum')).toEqual('02-01-2017');

        });

        it('the contact should have been related', () => {

            openTab('relations');

            expect($('.related_subjects [href="/betrokkene/12?gm=1&type=natuurlijk_persoon"]').isPresent()).toBe(true);

            openTab('phase');

        });
    
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
