import {
    openPageAs
} from './../../../../../functions/common/navigate';
import {
    openPhase
} from './../../../../../functions/intern/caseView/caseNav';
import {
    getClosedValue
} from './../../../../../functions/common/input/caseAttribute';

const choice = $('[data-name="boolean"]');
const confidentialityPublic = $('[data-name="vertrouwelijkheid_openbaar"]');
const confidentialityIntern = $('[data-name="vertrouwelijkheid_intern"]');
const confidentialityConfidential = $('[data-name="vertrouwelijkheid_vertrouwelijk"]');

describe('when opening case 59 with confidentiality public', () => {

    beforeAll(() => {

        openPageAs('admin', 59);

        openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(getClosedValue(confidentialityPublic)).toEqual('True');
        expect(getClosedValue(confidentialityIntern)).toEqual('False');
        expect(getClosedValue(confidentialityConfidential)).toEqual('False');

    });

    afterAll(() => {

        browser.get('/intern');

    });

});

describe('when opening case 60 with confidentiality internal', () => {

    beforeAll(() => {

        openPageAs('admin', 60);

        openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(getClosedValue(confidentialityPublic)).toEqual('False');
        expect(getClosedValue(confidentialityIntern)).toEqual('True');
        expect(getClosedValue(confidentialityConfidential)).toEqual('False');

    });

    afterAll(() => {

        browser.get('/intern');

    });

});

describe('when opening case 61 with confidentiality internal', () => {

    beforeAll(() => {

        openPageAs('admin', 61);

        openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(getClosedValue(confidentialityPublic)).toEqual('False');
        expect(getClosedValue(confidentialityIntern)).toEqual('False');
        expect(getClosedValue(confidentialityConfidential)).toEqual('True');

    });

    afterAll(() => {

        browser.get('/intern');

    });

});


/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
