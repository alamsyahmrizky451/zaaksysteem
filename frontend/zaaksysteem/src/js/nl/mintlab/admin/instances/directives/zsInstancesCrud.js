// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.admin.instances').directive('zsInstancesCrud', [
    '$q',
    'instancesService',
    'systemMessageService',
    'translationService',
    'dateFilter',
    function (
      $q,
      instancesService,
      systemMessageService,
      translationService,
      dateFilter
    ) {
      var otapLabels = _(instancesService.getOriginOptions())
        .indexBy('value')
        .mapValues('label')
        .value();

      return {
        require: ['zsInstancesCrud', '^zsInstancesView'],
        controller: [
          function () {
            var ctrl = this;
            var dataProvider;
            var zsInstancesView;
            var config = {
              columns: [
                {
                  id: 'status',
                  label: 'Status',
                  templateUrl: '/html/admin/instances/instances.html#status',
                  locked: true,
                  dynamic: true,
                },
                {
                  id: 'label',
                  label: 'Titel',
                  resolve: 'instance.label',
                  locked: true,
                  dynamic: true,
                  template:
                    '<i class="icon-font-awesome icon-lock icon-only" data-ng-show="instancesCrud.isProtected(item)" data-zs-title="Deze omgeving is beschermd"></i> <[item.instance.label]>',
                },
                {
                  id: 'fqdn',
                  label: 'Webadres',
                  resolve: 'instance.fqdn',
                  template:
                    '<a' +
                    ' href="<[instancesCrud.getLoginURL(item)]>"' +
                    ' target="_blank"' +
                    ' rel="noopener"' +
                    ' >' +
                    '<[item.instance.fqdn]>' +
                    '</a>',
                  locked: true,
                  dynamic: true,
                },
                {
                  id: 'hosts',
                  label: 'Hosts',
                  resolve: 'instance.hosts',
                  locked: true,
                  dynamic: true,
                  template:
                    '<ul data-ng-show="item.instance.hosts.rows.length">' +
                    '<li ng-repeat="host in item.instance.hosts.rows">' +
                    '<a' +
                    ' href="<[instancesCrud.getLoginURL(item, host)]>"' +
                    ' target="_blank"' +
                    ' rel="noopener"' +
                    ' >' +
                    '<[host.instance.fqdn]>' +
                    '</a>' +
                    '</li>' +
                    '</ul>' +
                    '<span data-ng-show="!item.instance.hosts.rows.length">Geen</span>',
                },
                {
                  id: 'template',
                  label: 'Template',
                  resolve: 'instance.template',
                  filter: 'capitalize',
                  locked: true,
                  dynamic: true,
                },
                {
                  id: 'otap',
                  label: 'Gebruiksdoel',
                  template: '<[instancesCrud.getOtapLabel(item)]>',
                  locked: true,
                  dynamic: true,
                },
                {
                  id: 'actions',
                  label: 'Acties',
                  templateUrl: '/html/admin/instances/instances.html#crud-menu',
                  locked: true,
                  dynamic: true,
                  sort: false,
                },
              ],
              options: {
                select: 'single',
                resolve: 'reference',
              },
              actions: [
                {
                  id: 'details',
                  type: 'popup',
                  label: 'Details',
                  data: {
                    url: '/html/admin/instances/instances.html#details',
                  },
                },
                {
                  id: 'edit',
                  type: 'popup',
                  label: 'Bewerken',
                  data: {
                    url: '/html/admin/instances/instances.html#edit',
                  },
                },
                {
                  id: 'protect',
                  type: 'click',
                  label: 'Beschermen',
                },
                {
                  id: 'unprotect',
                  type: 'click',
                  label: 'Vrijgeven',
                },
                {
                  id: 'disable',
                  type: 'click',
                  label: 'Offline',
                },
                {
                  id: 'enable',
                  type: 'click',
                  label: 'Online',
                },
                {
                  id: 'delete',
                  type: 'confirm',
                  label: 'Verwijderen',
                  data: {
                    verb: 'Verwijderen',
                    label:
                      'Weet u zeker dat u deze omgeving wil verwijderen? Dit kan niet ongedaan gemaakt worden.',
                  },
                },
                {
                  id: 'recover',
                  type: 'click',
                  label: 'Herstellen',
                },
              ],
              style: {
                classes: {
                  'instance-crud-item-deleted': 'instance.status==="deleted"',
                  'instance-crud-item-active': 'instance.status==="active"',
                  'instance-crud-item-processing':
                    'instance.status==="processing"',
                  'instance-crud-item-disabled': 'instance.status==="disabled"',
                  'instance-crud-item-protected':
                    'instance.status=="protected"',
                },
              },
              url: undefined,
            };

            config.actions.forEach(function (action) {
              if (action.type !== 'popup') {
                action.data = _.extend(action.data || {}, {
                  click:
                    'instancesCrud.handleClick("' +
                    action.id +
                    '", $selection[0])',
                });
              }
              action.when =
                'selectedItems.length&&instancesCrud.isAllowed("' +
                action.id +
                '", selectedItems[0])';
            });

            ctrl.options = {
              showDeleted: false,
            };

            function getParams(action) {
              var key, value;

              switch (action) {
                case 'protect':
                case 'unprotect':
                  key = 'protected';
                  value = action === 'protect';
                  break;

                case 'delete':
                case 'recover':
                  key = 'deleted';
                  value = action === 'delete';
                  break;

                case 'disable':
                case 'enable':
                  key = 'disabled';
                  value = action === 'disable';
                  break;
              }

              if (key) {
                var params = {};
                params[key] = value;
                return params;
              }
              return null;
            }

            ctrl.link = function (controllers) {
              zsInstancesView = controllers[0];
            };

            ctrl.getInstances = function () {
              return zsInstancesView ? zsInstancesView.getInstances() : null;
            };

            ctrl.getCrudConfig = function () {
              return config;
            };

            ctrl.handleClick = function (action, instance) {
              var params = getParams(action);
              if (params) {
                instancesService
                  .updateInstance(ctrl.getControlPanelId(), instance, params)
                  .then(function () {
                    systemMessageService.emitSave();
                    zsInstancesView.onHostUpdate();
                  })
                  .catch(function () {
                    systemMessageService.emitSaveError();
                  });
              }
            };

            ctrl.isAllowed = function (action, instance) {
              var unallowedActions = Array('delete', 'recover');
              if (unallowedActions.includes(action)) {
                return false;
              }

              var params = getParams(action);
              var active = false;
              var allowed = false;

              switch (action) {
                default:
                  allowed = ctrl.canEdit() || ctrl.canEditInstance(instance);
                  break;

                case 'details':
                  allowed = ctrl.canEdit();
                  break;

                case 'protect':
                case 'unprotect':
                  allowed = ctrl.canEdit() && ctrl.canEditInstance(instance);
                  break;
              }

              if (!allowed) {
                return false;
              }

              active = _.every(params, function (value, key) {
                var instanceVal = instance.instance[key];
                return value ? !!instanceVal : !instanceVal;
              });

              var allowedActions = Array('edit', 'details', 'login');
              if (allowedActions.includes(action) || !active) {
                return true;
              }

              return false;
            };

            ctrl.isActive = function (instance) {
              return ctrl.getStatus(instance) === 'active';
            };

            ctrl.isDisabled = function (instance) {
              return ctrl.getStatus(instance) === 'disabled';
            };

            ctrl.isDeleted = function (instance) {
              return ctrl.getStatus(instance) === 'deleted';
            };

            ctrl.isProcessing = function (instance) {
              return ctrl.getStatus(instance) === 'processing';
            };

            ctrl.isProtected = function (instance) {
              return instance.instance.protected === true;
            };

            ctrl.getStatus = function (instance) {
              return instance.instance.status;
            };

            ctrl.getStatusLabel = function (instance) {
              var status = ctrl.getStatus(instance),
                label;

              switch (status) {
                case 'processing':
                  label = translationService.get('Bezig');
                  break;

                case 'active':
                  label = translationService.get('Actief');
                  break;

                case 'disabled':
                  label = translationService.get('Inactief');
                  break;

                case 'deleted':
                  label = translationService.get('Wordt verwijderd');
                  if (instance.instance.delete_on !== undefined) {
                    label +=
                      'op ' +
                      dateFilter(instance.instance.delete_on, 'mediumDate');
                  }
                  break;
              }

              return label;
            };

            ctrl.getLoginURL = function (instance, host) {
              if (!zsInstancesView.isPip()) {
                var uri =
                  '/auth/token/remote_login?instance_id=' + instance.reference;
                if (host) {
                  uri += '&host_id=' + host.reference;
                }
                return uri;
              }

              if (host) {
                return 'https://' + host.instance.fqdn;
              } else {
                return 'https://' + instance.instance.fqdn;
              }
            };

            ctrl.setDataProvider = function (provider) {
              dataProvider = provider;
            };

            ctrl.getCustomerConfig = function () {
              return zsInstancesView
                ? zsInstancesView.getCustomerConfig()
                : null;
            };

            ctrl.getControlPanelId = function () {
              var config = ctrl.getCustomerConfig();
              return config && config.reference;
            };

            ctrl.canEdit = function () {
              return zsInstancesView && !zsInstancesView.isPip();
            };

            ctrl.canEditInstance = function (instance) {
              return (
                ctrl.canEdit() ||
                (!instance.instance.protected &&
                  zsInstancesView.isPipWritable())
              );
            };

            ctrl.getOtapLabel = function (item) {
              return otapLabels[item.instance.otap] || 'Onbekend';
            };

            return ctrl;
          },
        ],
        controllerAs: 'instancesCrud',
        templateUrl: '/html/admin/instances/instances.html',
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.splice(1));
        },
      };
    },
  ]);
})();
