// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsEzraDialog', [
    function () {
      var cancelEvent = window.zsFetch('nl.mintlab.utils.events.cancelEvent');

      return {
        link: function (scope, element, attrs) {
          var isOpen = false,
            isDestroyed = false;

          function safeApply(fn) {
            if (!scope.$root) {
              fn();
              return;
            }

            var phase = scope.$root.$$phase;

            if (phase === '$apply' || phase === '$digest') {
              fn();
            } else {
              scope.$apply(fn);
            }
          }

          function onDialogOpen() {
            isOpen = true;
            safeApply(function () {
              scope.$emit('zs.ezra.dialog.open');
            });
          }

          function onDialogClose() {
            isOpen = false;
            $.ztWaitStop();

            safeApply(function () {
              scope.$emit('zs.ezra.dialog.close');
            });

            if (!isDestroyed) {
              removeListeners();
            }
          }

          function addListeners() {
            $('#dialog').on('dialogopen', onDialogOpen);
            $('#dialog').on('dialogclose', onDialogClose);
          }

          function removeListeners() {
            $('#dialog').off('dialogopen', onDialogOpen);
            $('#dialog').off('dialogclose', onDialogClose);
          }

          function onClick(event) {
            scope.$apply(function () {
              var params = scope.$eval(attrs.zsEzraDialog) || {};
              addListeners();
              ezra_dialog(params, function () {});
              cancelEvent(event);
            });
          }

          element.bind('click', onClick);

          scope.$on('$destroy', function () {
            element.unbind('click', onClick);
            isDestroyed = true;
            if (!isOpen) {
              removeListeners();
            }
          });
        },
      };
    },
  ]);
})();
