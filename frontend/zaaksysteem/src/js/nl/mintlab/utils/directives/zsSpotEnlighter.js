// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  var TEMPLATES = {
    default: '/html/directives/spot-enlighter/default.html',
    contact: '/html/directives/spot-enlighter/contact.html',
    'contact/medewerker': '/html/directives/spot-enlighter/medewerker.html',
    'contact/bedrijf': '/html/directives/spot-enlighter/bedrijf.html',
    'contact/natuurlijk_persoon':
      '/html/directives/spot-enlighter/natuurlijk_persoon.html',
    bag: '/html/directives/spot-enlighter/address.html',
    'bag-street': '/html/directives/spot-enlighter/street.html',
    zaak: '/html/directives/spot-enlighter/case.html',
    'casetype/object': '/html/directives/spot-enlighter/casetype_object.html',
  };

  var LABELS = {
    default: undefined,
    contact: "voorletters + ' ' + geslachtsnaam",
    'contact/medewerker': 'naam',
  };

  function getBagLabel(item) {
    var huisNummerString = item.huisnummer
      ? ' ' +
        item.huisnummer +
        item.huisletter +
        (item.huisnummer_toevoeging ? '-' : '') +
        item.huisnummer_toevoeging
      : '';
    var postcodeString = item.postcode ? item.postcode + ' ' : '';

    return (
      item.straatnaam + huisNummerString + ', ' + postcodeString + item.plaats
    );
  }

  var bagTypes = {
    bag: 'nummeraanduiding',
    'bag-street': 'openbareruimte',
    'bag-mixed': ['nummeraanduiding', 'openbareruimte'],
  };

  angular.module('Zaaksysteem.directives').directive('zsSpotEnlighter', [
    '$rootScope',
    '$window',
    '$document',
    '$timeout',
    '$parse',
    'templateCompiler',
    'smartHttp',
    function (
      $rootScope,
      $window,
      $document,
      $timeout,
      $parse,
      templateCompiler,
      smartHttp
    ) {
      var addEventListener = window.zsFetch(
          'nl.mintlab.utils.events.addEventListener'
        ),
        removeEventListener = window.zsFetch(
          'nl.mintlab.utils.events.removeEventListener'
        ),
        cancelEvent = window.zsFetch('nl.mintlab.utils.events.cancelEvent'),
        fromLocalToGlobal = window.zsFetch(
          'nl.mintlab.utils.dom.fromLocalToGlobal'
        ),
        getViewportSize = window.zsFetch(
          'nl.mintlab.utils.dom.getViewportSize'
        ),
        trim = window.zsFetch('nl.mintlab.utils.shims.trim'),
        isObject = angular.isObject,
        body = $document.find('body'),
        parent;

      function createMenu() {
        parent = angular.element('<div></div>');
        parent.addClass('zs-spot-enlighter-container');

        body.append(parent);
      }

      function moveToFront() {
        body.append(parent);
      }

      createMenu();

      return {
        restrict: 'A',
        scope: false,
        require: 'ngModel',
        priority: 1000,
        link: function (scope, element, attrs, ngModel) {
          var menu,
            query,
            highlighted,
            loading,
            parent,
            loadingEl,
            timeoutCancel,
            tagParent,
            bag = false,
            interval = 125,
            isOpen = false,
            multi = scope.$eval(attrs.zsSpotEnlighterMulti),
            selectedEntries = [];

          element.attr('autocomplete', 'off');

          if (!element.parent().hasClass('spot-enlighter-wrapper')) {
            parent = angular.element(
              '<div class="spot-enlighter-wrapper"></div>'
            );
            element.after(parent);
            parent.append(element);
          } else {
            parent = element.parent();
          }

          loadingEl = angular.element(
            '<div class="spot-enlighter-loading"></div>'
          );
          element.after(loadingEl);

          var tagWrapperMultiClass = multi
            ? ' spot-enlighter-tag-wrapper-multi'
            : '';
          tagParent = angular.element(
            '<div class="spot-enlighter-tag-wrapper' +
              tagWrapperMultiClass +
              '"></div>'
          );
          tagParent.css('display', 'none');
          element.after(tagParent);

          function openMenu() {
            if (isOpen) {
              return;
            }

            isOpen = true;

            if (!scope.$$phase && !scope.$root.$$phase) {
              scope.$apply(getTemplate);
            } else {
              getTemplate();
            }
          }

          function hideMenu() {
            if (!isOpen) {
              return;
            }

            isOpen = false;

            unhighlight();
            setData([]);

            parent.removeClass('spot-enlighter-visible');
            if (menu) {
              menu.unbind('mousedown', onMenuMouseDown);
              menu.unbind('click', onMenuClick);
              menu.remove();
              menu = undefined;
            }
          }

          function getTemplate() {
            var url = getTemplateUrl();

            templateCompiler.getCompiler(url).then(function (compiler) {
              if (element[0] !== $document[0].activeElement) {
                menu = undefined;
                isOpen = false;
                return;
              }

              moveToFront();

              compiler(scope, function (clonedElement /*, scope*/) {
                menu = clonedElement;
                menu.bind('mousedown', onMenuMouseDown);
                menu.bind('click', onMenuClick);

                parent.append(menu);
                parent.addClass('spot-enlighter-visible');

                if (query) {
                  getData();
                }

                positionMenu();
              });
            });
          }

          function getTemplateUrl() {
            var url;
            if (attrs.zsSpotEnlighterTemplate) {
              url = scope.$eval(attrs.zsSpotEnlighterTemplate);
            } else {
              url =
                TEMPLATES[scope.$eval(attrs.zsSpotEnlighterRestrict)] ||
                TEMPLATES['default'];
            }
            return url;
          }

          function getObjectType() {
            return scope.$eval(attrs.zsSpotEnlighterRestrict);
          }

          function getLabel(obj) {
            var toParse =
              attrs.zsSpotEnlighterLabel ||
              LABELS[getObjectType()] ||
              LABELS['default'];

            return $parse(toParse)(obj);
          }

          function getLabels(objs) {
            return (
              objs
                .map(function (obj) {
                  return getLabel(obj);
                })
                // some instances intentionally do not have labels (e.g. BAG on /form)
                .filter(function (label) {
                  return label;
                })
            );
          }

          function positionMenu() {
            var pos,
              viewportSize,
              elWidth,
              elHeight,
              menuWidth,
              menuHeight,
              vOrient,
              hOrient,
              x,
              y;

            if (!menu) {
              return;
            }

            viewportSize = getViewportSize();
            elWidth = element[0].clientWidth;
            elHeight = element[0].clientHeight;

            pos = fromLocalToGlobal(element[0], { x: 0, y: 0 });

            menu.css('width', elWidth + 'px');

            menuWidth = menu[0].clientWidth;
            menuHeight = menu[0].clientHeight;

            if (pos.y + elHeight + menuHeight > viewportSize.height) {
              vOrient = 'top';
              y = -menuHeight;
            } else {
              vOrient = 'bottom';
              y = elHeight;
            }

            if (pos.x + menuWidth > viewportSize.width) {
              x = -menuWidth;
              hOrient = 'left';
            } else {
              x = 0;
              hOrient = 'right';
            }

            menu.css('top', y + 'px');
            menu.css('left', x + 'px');
            menu.attr('data-zs-spot-enlighter-horizontal-orientation', hOrient);
            menu.attr('data-zs-spot-enlighter-vertical-orientation', vOrient);

            scope.reversed = vOrient === 'top';
          }

          function invalidate() {
            if (!loading) {
              if (timeoutCancel) {
                $timeout.cancel(timeoutCancel);
              }
              timeoutCancel = $timeout(function () {
                timeoutCancel = null;
                getData();
              }, interval);
            }
          }

          function getData() {
            var loadedQuery,
              params,
              objType = getObjectType(),
              postfix = '',
              url,
              zsSpotEnlighterParams =
                scope.$eval(attrs.zsSpotEnlighterParams) || {};

            if (!query || query.length < 3) {
              return;
            }

            loadedQuery = query;
            loading = true;

            params = _.clone(zsSpotEnlighterParams);
            params.zapi_num_rows = 20;
            params.query = query;

            if (objType) {
              if (
                objType === 'contact' ||
                objType === 'zaak' ||
                objType === 'natuurlijk_persoon'
              ) {
                params.object_type = objType;
              } else {
                postfix = objType;
              }
            }

            if (
              postfix === 'bag' ||
              postfix === 'bag-street' ||
              postfix === 'bag-mixed'
            ) {
              bag = true;
            }

            // TODO: no urls should start with objectsearch
            if (postfix.indexOf('casetype') === 0) {
              url = postfix;
            } else if (bag) {
              url = '/zsnl_bag/bag/search';
              params = {
                search_string: query,
                prio_municipalities: scope.config().bag_priority_gemeentes,
                prio_only: scope.config().bag_local_only ? 1 : 0,
                bag_object_type: bagTypes[postfix],
              };
            } else {
              url = 'objectsearch/' + postfix;
            }

            setLoader();

            smartHttp
              .connect({
                url: url,
                method: 'GET',
                params: params,
                // backend needs this to "know" it's a JSON request
                headers: {
                  'X-Requested-With': 'XMLHttpRequest',
                },
              })
              .success(handleData)
              .error(handleError)
              .then(
                function () {
                  handleEnd(loadedQuery);
                },
                function () {
                  handleEnd(loadedQuery);
                }
              );
          }

          function handleData(data, status, headers, config) {
            var entries,
              currentQuery = query,
              performedQuery = bag
                ? config.params.search_string
                : config.params.query;

            if (data.result !== undefined) {
              entries = data.result;
            } else if (bag) {
              entries = data.data.map(function (entry) {
                return {
                  id: entry.id,
                  label: getBagLabel(entry),
                  object: {
                    label: getBagLabel(entry),
                    city: entry.plaats,
                    id: entry.id,
                    number: entry.huisnummer,
                    letter: entry.huisletter,
                    suffix: entry.huisnummer_toevoeging,
                    postal_code: entry.postcode,
                    street: entry.straatnaam,
                    streetname: entry.straatnaam,
                    type: 'address',
                  },
                  object_type: 'bag_address',
                };
              });
            } else {
              entries = data.json.entries;
            }

            if (currentQuery === performedQuery) {
              setData(entries);
            }
          }

          function handleError() {}

          function handleEnd(loadedQuery) {
            loading = false;
            if (query !== loadedQuery) {
              invalidate();
            }
            setLoader();
          }

          function setData(entries) {
            function updateEntries() {
              scope.entries = entries;
              $timeout(function () {
                positionMenu();
              });
            }

            if (!scope.$$phase && !scope.$root.$$phase) {
              scope.$apply(updateEntries);
            } else {
              updateEntries();
            }
          }

          function setQuery(q) {
            q = trim(q);
            if (query !== q) {
              if (!menu) {
                openMenu();
              }
              query = q;
              if (!query) {
                setData([]);
              }
              invalidate();
            }
          }

          function selectCurrent() {
            var data;
            if (highlighted) {
              data = highlighted.scope().entry;
            }
            unhighlight();
            selectEntry(data);
            hideMenu();
          }

          function highlight(el) {
            unhighlight();

            if (!el.length) {
              return;
            }

            el.addClass('spot-enlighter-entry-highlight');

            highlighted = el;
          }

          function unhighlight() {
            if (highlighted) {
              highlighted.removeClass('spot-enlighter-entry-highlight');

              highlighted = null;
            }
          }

          function getIndexOfSelectedItem(htmlCollection) {
            return _.findIndex(
              Array.prototype.slice.call(htmlCollection),
              function (el) {
                return _.includes(
                  el.className,
                  'spot-enlighter-entry-highlight'
                );
              }
            );
          }

          function highlightNext() {
            var next, nextSibling, index;

            if (highlighted) {
              nextSibling = angular.element(highlighted[0].nextSibling);
              while (
                nextSibling.length &&
                (!nextSibling.scope() ||
                  nextSibling.scope().entry === undefined)
              ) {
                nextSibling = angular.element(nextSibling[0].nextSibling);
              }
              next = nextSibling;

              if (next.parent()[0]) {
                index = getIndexOfSelectedItem(next.parent()[0].children);
              }
            }

            if (!next || !next.length) {
              next = menu.find('li').eq(0);
            }

            parent.children()[3].scrollTop = index * 48;

            highlight(next);
          }

          function highlightPrev() {
            var prev, prevSibling, list, index;

            if (highlighted) {
              prevSibling = angular.element(highlighted[0].previousSibling);
              while (
                prevSibling.length &&
                (!prevSibling.scope() ||
                  prevSibling.scope().entry === undefined)
              ) {
                prevSibling = angular.element(prevSibling[0].previousSibling);
              }
              prev = prevSibling;
            }

            if (!prev || !prev.length) {
              list = menu.find('li');
              prev = list.eq(list.length - 1);
            }

            if (prev.parent()[0]) {
              index = getIndexOfSelectedItem(prev.parent()[0].children);

              if (index === 0) {
                index = list.length;
              }
            }

            parent.children()[3].scrollTop = index * 48 - 48;

            highlight(prev);
          }

          function onChange(event) {
            var val;

            if (event.keyCode === 13 || event.keyCode === 9) {
              return cancelEvent(event, true);
            }

            val = element.val();

            setQuery(val);

            if (event.keyCode) {
              switch (event.keyCode) {
                case 27:
                  if (menu) {
                    hideMenu();
                    return cancelEvent(event, true);
                  }
                  break;

                case 38:
                  highlightPrev();
                  break;

                case 40:
                  highlightNext();
                  break;
              }
            }
          }

          function onKeyDown(event) {
            switch (event.keyCode) {
              case 13:
              case 9:
                if (highlighted) {
                  selectCurrent();
                  return cancelEvent(event, true);
                }
                break;

              case 38:
              case 40:
                event.preventDefault();
                break;

              case 8:
                var getter = $parse(attrs.ngModel),
                  val = getter(scope);

                if (val && typeof val !== 'string' && typeof val !== 'number') {
                  selectEntry(null);
                }
                break;
            }
          }

          function onMenuMouseDown(/*event*/) {
            element.unbind('blur', onBlur);
            menu.bind('mouseup', function onMenuMouseUp(/*event*/) {
              menu.unbind('mouseup', onMenuMouseUp);
              element.bind('blur', onBlur);
            });
          }

          function onMenuClick(event) {
            var el = angular.element(event.target),
              elScope = el.scope();

            if (elScope && elScope.entry) {
              selectEntry(elScope.entry);
              hideMenu();
            }
          }

          function onFocus(/*event*/) {
            element.val('');
            if (!multi) {
              selectEntry(null);
            }
            openMenu();
          }

          function onBlur(/*event*/) {
            hideMenu();
          }

          function fromUser(val) {
            // make sure user changes never result in model changes
            return val && angular.isObject(val) ? val : null;
          }

          function toUser(obj) {
            var output;

            setValidity();
            element.val(obj ? getLabel(obj) : '');

            output = obj ? getLabel(obj) : '';

            return output;
          }

          function selectEntry(entry) {
            query = undefined;

            // deal with different object structures
            var obj = entry ? entry.object || entry : null;

            if (obj && getObjectType() === 'attributes') {
              obj = entry;
            }

            if (obj) {
              selectedEntries.push(obj);
            }

            scope.$apply(function () {
              ngModel.$setViewValue(multi ? selectedEntries : obj);
              element.val(obj && !multi ? ' ' : '');
              setValidity();
              ngModel.$render();

              scope.$emit('zs.spotenlighter.object.select', obj);

              if (obj && attrs.zsSpotEnlighterSelect !== undefined) {
                $parse(attrs.zsSpotEnlighterSelect)(scope, {
                  $object: entry,
                });
              }
            });
          }

          function getModelData() {
            return $parse(attrs.ngModel)(scope);
          }

          function setValidity() {
            var data = getModelData(),
              isRequired =
                attrs.zsObjectRequired && scope.$eval(attrs.zsObjectRequired);

            ngModel.$setValidity(
              'zs-object',
              (data && isObject(data)) || !isRequired
            );
          }

          function onRemoveClick(event) {
            var label = event.currentTarget.getAttribute('name');

            selectedEntries = selectedEntries.filter(function (selectedUser) {
              return selectedUser[attrs.zsSpotEnlighterLabel] !== label;
            });

            scope.$apply(function () {
              ngModel.$setViewValue(
                selectedEntries.length ? selectedEntries : null
              );
              element.val('');
              setValidity();
              ngModel.$render();
            });
          }

          function setLoader() {
            loadingEl.css('display', loading ? 'block' : 'none');
          }

          function onScroll() {
            if (isOpen) {
              positionMenu();
            }
          }

          scope.getEntryLabel = function (entry) {
            return (
              getLabel(entry.object) ||
              entry.label ||
              entry.searchable_object_label
            );
          };

          setLoader();

          addEventListener($window, 'scroll', onScroll);

          scope.$on('$destroy', function () {
            hideMenu();
            removeEventListener($window, 'scroll', onScroll);
          });

          element.bind('focus', onFocus);
          element.bind('blur', onBlur);
          element.bind('change paste keyup input', onChange);
          element.bind('keydown', onKeyDown);

          ngModel.$formatters.push(toUser);
          ngModel.$parsers.push(fromUser);

          ngModel.$render = function () {
            var data = getModelData() || [];
            var dataArray = Array.isArray(data) ? data : [data];
            var val = getLabels(dataArray);

            tagParent.css('display', val.length ? 'block' : 'none');

            tagParent.empty();
            val.forEach(function (label) {
              var tag = angular.element(
                '<div class="spot-enlighter-tag" tabindex="0">' +
                  label +
                  '</div>'
              );
              var removeButton = angular.element(
                '<button type="button" class="spot-enlighter-clear" name="' +
                  label +
                  '"><i class="mdi mdi-close"></i></button>'
              );
              tagParent.append(tag);
              tag.append(removeButton);
              removeButton.bind('click', onRemoveClick);
            });

            if (!multi) {
              if (val.length) {
                element.attr('disabled', 'disabled');
              } else {
                element.removeAttr('disabled');
              }
            }
          };

          if (attrs.zsObjectRequired === undefined) {
            setValidity();
          } else {
            attrs.$observe('zsObjectRequired', function () {
              setValidity();
            });
          }

          if (attrs.zsSpotEnlighterLabel === undefined) {
            ngModel.$render();
          } else {
            attrs.$observe('zsSpotEnlighterLabel', function () {
              ngModel.$render();
            });
          }
        },
      };
    },
  ]);
})();
