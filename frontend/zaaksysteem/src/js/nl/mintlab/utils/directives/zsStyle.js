// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem.directives').directive('zsStyle', [
    function () {
      return {
        compile: function (/*tElement, tAttrs, transclude*/) {
          return function link(scope, element, attrs) {
            attrs.$observe('zsStyle', function () {
              setStyle(element);
            });

            function setStyle() {
              element.attr('style', attrs.zsStyle);
            }

            setStyle();
          };
        },
      };
    },
  ]);
})();
