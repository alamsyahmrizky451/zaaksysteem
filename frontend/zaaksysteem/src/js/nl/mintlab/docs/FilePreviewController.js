// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.docs')
    .controller('nl.mintlab.docs.FilePreviewController', [
      '$scope',
      '$window',
      'smartHttp',
      function ($scope, $window, smartHttp) {
        $scope.$on('popupclose', function (/*event, key, value*/) {
          $scope.reloadData();
        });

        $scope.onNameClick = function (event) {
          if (event.type !== 'click' && event.key !== 'Enter') {
            return;
          }

          if ($scope.entity.getEntityType() === 'folder') {
            $scope.onFolderNameClick(event);
          } else {
            $scope.deselectAll();
            $scope.selectEntity($scope.entity);

            $scope.openPopup();
          }

          event.stopPropagation();
        };
      },
    ]);
})();
