// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.object').factory('objectService', [
    '$q',
    '$http',
    'userService',
    function ($q, $http, userService) {
      var objectService = {};

      function createEntityId(orgUnitId, roleId) {
        return orgUnitId + '|' + roleId;
      }

      objectService.getObject = function (uuid, params) {
        var deferred = $q.defer();

        $http({
          method: 'GET',
          url: '/api/object/' + uuid,
          params: params,
        })
          .success(function (response) {
            var object = response.result[0];

            if (!object) {
              deferred.reject();
            } else {
              deferred.resolve(object);
            }
          })
          .error(function (response) {
            deferred.reject(response);
          });

        return deferred.promise;
      };

      objectService.getObjectId = function (object, type) {
        var id;
        switch (type) {
          default:
            id = object.id;
            break;
        }
        return id;
      };

      objectService.saveObject = function (object, options) {
        var deferred = $q.defer(),
          params = {
            object: angular.copy(object),
          },
          related = params.object.related_objects;

        params.object.object_class = object.type;

        if (params.object.values.related_casetypes) {
          related = related.concat(params.object.values.related_casetypes);
        }

        _.each(related, function (obj) {
          obj.related_object = null;
        });

        _.merge(params, options);

        params.object.security_rules = [];
        params.object.actions = [];

        $http({
          method: 'POST',
          url: '/api/object/save',
          data: params,
        })
          .success(function (response) {
            var result = response.result[0],
              key;

            if (result) {
              for (key in result) {
                object[key] = result[key];
              }
            }

            deferred.resolve(object);
          })
          .error(function (response) {
            deferred.reject(
              response && response.result
                ? response.result[0]
                : new Error('Unknown error occured while saving the object.')
            );
          });

        return deferred.promise;
      };

      objectService.deleteObject = function (object) {
        var deferred = $q.defer();

        $http({
          method: 'POST',
          url: '/api/object/' + object.id + '/delete',
        })
          .success(function (/*response*/) {
            deferred.resolve(object);
          })
          .error(function (response) {
            deferred.reject(response);
          });

        return deferred.promise;
      };

      objectService.createObject = function (objectType) {
        var object = {
          actions: [],
          date_created: undefined,
          date_modified: undefined,
          values: {},
          relatable_types: [],
          related_objects: [],
          id: undefined,
          type: objectType,
          security_rules: [],
        };

        return object;
      };

      objectService.updateObject = function (object, values, parameters) {
        var current = angular.copy(object.values);

        angular.extend(object.values, values);

        return objectService
          .saveObject(object, parameters)
          ['catch'](function (response) {
            object.values = current;
            throw response.result ? response.result[0] : response;
          });
      };

      objectService.addRelation = function (target, related, relatedType) {
        var relation = {
            related_object_id: objectService.getObjectId(related, relatedType),
            related_object_type: relatedType,
            relationship_name_a: 'related',
            relationship_name_b: 'related',
            related_object: related,
          },
          arr;

        switch (relatedType) {
          case 'casetype':
            arr = target.values.related_casetypes;
            break;

          default:
            arr = target.related_objects;
            break;
        }

        arr.push(relation);

        return objectService
          .saveObject(target, { deep_relations: true })
          ['catch'](function (err) {
            _.pull(arr, relation);
            throw err;
          });
      };

      objectService.removeRelation = function (target, related, type) {
        var relatedObjects, relation;

        switch (type) {
          case 'casetype':
            relatedObjects = target.values.related_casetypes;
            break;

          default:
            relatedObjects = target.related_objects;
            break;
        }

        relation = _.find(relatedObjects, {
          related_object_id: objectService.getObjectId(related),
          related_object_type: type,
        });

        _.pull(relatedObjects, relation);

        return objectService
          .saveObject(target, { deep_relations: true })
          ['catch'](function (err) {
            relatedObjects.push(relation);
            throw err;
          });
      };

      objectService.getRelations = function (object) {
        var relations = object.related_objects;
        if (object.values.related_casetypes !== undefined) {
          relations = relations.concat(object.values.related_casetypes);
        }
        return relations;
      };

      objectService.addSecurityRule = function (object, rule) {
        var deferred = $q.defer();

        object.security_rules.push(rule);

        $http({
          method: 'POST',
          url: '/api/object/' + object.id + '/ratify',
          data: {
            entity_id: rule.entity_id,
            entity_type: rule.entity_type,
            capability: rule.capability,
          },
        })
          .success(function (/*response*/) {
            deferred.resolve();
          })
          .error(function (response) {
            _.pull(object.security_rules, rule);

            deferred.reject(response);
          });

        return deferred.promise;
      };

      objectService.removeSecurityRule = function (object, rule) {
        var deferred = $q.defer();

        _.pull(object.security_rules, rule);

        $http({
          method: 'POST',
          url: '/api/object/' + object.id + '/revoke',
          data: {
            entity_id: rule.entity_id,
            entity_type: rule.entity_type,
            capability: rule.capability,
          },
        })
          .success(function (/*response*/) {
            deferred.resolve();
          })
          .error(function (response) {
            object.security_rules.push(rule);
            deferred.reject(response);
          });

        return deferred.promise;
      };

      objectService.hasSecurityRule = function (object, rule) {
        return !!_.find(object.security_rules, {
          entity_id: rule.entity_id,
          entity_type: rule.entity_type,
          capability: rule.capability,
        });
      };

      objectService.createSecurityRule = function (
        entityId,
        entityType,
        capability
      ) {
        return {
          entity_id: entityId,
          entity_type: entityType,
          capability: capability,
        };
      };

      objectService.revokeAllRules = function (object, entity) {
        var deferred = $q.defer();

        _.remove(object.security_rules, function (rule) {
          return (
            rule.entity_id === entity.entity_id &&
            rule.entity_type === entity.entity_type
          );
        });

        $http({
          method: 'POST',
          url: '/api/object/' + object.id + '/revoke',
          data: {
            entity_id: entity.entity_id,
            entity_type: entity.entity_type,
          },
        })
          .success(function (/*response*/) {
            deferred.resolve();
          })
          .error(function (/*response*/) {});
      };

      objectService.createPositionEntity = function (orgUnitId, roleId) {
        return {
          entity_id: createEntityId(orgUnitId, roleId),
          entity_type: 'position',
        };
      };

      objectService.createPositionRule = function (
        orgUnitId,
        roleId,
        capability
      ) {
        var entity = objectService.createPositionEntity();

        return _.merge(entity, {
          capability: capability,
        });
      };

      objectService.isUserCapable = function (object, capability) {
        var isAdmin = userService.isAdmin(),
          capable = isAdmin;

        if (!isAdmin) {
          capable = _.indexOf(object.actions, capability) !== -1;
        }

        return capable;
      };

      objectService.isEqualValue = function (a, b) {
        var isEqual = false;

        if (_.isArray(a) !== _.isArray(b)) {
          if (!_.isArray(a) && !_.isEmpty(a)) {
            a = [a];
          }
          if (!_.isArray(b) && !_.isEmpty(b)) {
            b = [b];
          }
        }

        // intentional
        if (
          a == b ||
          (!_.isNumber(a) && !_.isNumber(b) && _.isEmpty(a) && _.isEmpty(b)) ||
          (_.isNaN(a) && _.isNaN(b)) ||
          angular.equals(a, b)
        ) {
          isEqual = true;
        }

        return isEqual;
      };

      return objectService;
    },
  ]);
})();
