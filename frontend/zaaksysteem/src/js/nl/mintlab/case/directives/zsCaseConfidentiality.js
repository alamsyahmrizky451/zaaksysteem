// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseConfidentiality', [
    function () {
      return {
        require: ['zsCaseConfidentiality', '^zsCaseView'],
        controller: [
          '$scope',
          '$element',
          '$attrs',
          function ($scope, element, $attrs) {
            var ctrl = this,
              options,
              constants,
              zsCaseView;

            function setConfidentiality() {
              var caseObj, val, option;

              if (zsCaseView && zsCaseView.isLoaded()) {
                caseObj = zsCaseView.getCase();
                val = caseObj.values['case.confidentiality'];

                option = _.find(options, { value: val });

                ctrl.confidentiality = option ? option.value : null;
              }
            }

            function onCaseUpdate() {
              setConfidentiality();
            }

            function processConstants() {
              constants = $scope.$eval($attrs.constants);

              options = _.map(constants, function (value, key) {
                return {
                  value: {
                    original: key,
                    mapped: value,
                  },
                  label: value,
                };
              });
            }

            ctrl.getOptions = function () {
              return options;
            };

            ctrl.canChange = function () {
              return $scope.$eval($attrs.canChange);
            };

            ctrl.getLabel = function () {
              var label;
              if (ctrl.confidentiality) {
                label = ctrl.confidentiality.mapped;
              }
              return label;
            };

            ctrl.handleConfidentialityChange = function () {
              zsCaseView.setCaseValue(
                'case.confidentiality',
                ctrl.confidentiality,
                ctrl.confidentiality.original
              );
            };

            ctrl.link = function (controllers) {
              zsCaseView = controllers[0];

              zsCaseView.updateListeners.push(onCaseUpdate);

              $scope.$on('$destroy', function () {
                _.pull(zsCaseView.updateListeners, onCaseUpdate);
              });

              setConfidentiality();

              processConstants();
            };

            ctrl.isConfidential = function () {
              var isConfidential;
              if (ctrl.confidentiality) {
                isConfidential =
                  ctrl.confidentiality.original === 'confidential';
              }

              return isConfidential;
            };

            return ctrl;
          },
        ],
        controllerAs: 'caseConfidentiality',
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.slice(1));
        },
      };
    },
  ]);
})();
