// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.core.error').constant('ERROR', {
    NOT_FOUND: {
      code: 'not_found',
      message: 'Object kon niet worden gevonden',
    },
  });
})();
