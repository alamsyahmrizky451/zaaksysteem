// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsCrudTableAutoSize', [
    '$window',
    function ($window) {
      return {
        require: 'zsCrudTableAutoSize',
        controller: [
          '$element',
          function ($element) {
            var rows = [],
              controller = {},
              invalidating,
              minColumnSize = 50,
              timerId = -1;

            function onTimeout() {
              timerId = -1;
              setAutoWidth();
            }

            function setAutoWidth() {
              var totalWidthByCol = {},
                numColumns = Number.MAX_VALUE,
                i,
                l,
                totalSize,
                minCellSize,
                percLeft;

              if (!rows.length) {
                return;
              }

              function resetColumnSize() {
                _.each(rows[0].children, function (element) {
                  angular.element(element).css('width', '');
                });
              }

              function getColumnSize(num) {
                var width = 0,
                  row,
                  i,
                  l,
                  cell,
                  el;

                for (i = 0, l = rows.length; i < l; ++i) {
                  row = rows[i];
                  cell = row.children[num];
                  if (i === 0) {
                    continue;
                  }
                  if (cell) {
                    el = cell.children.length ? cell.children[0] : cell;
                    width = Math.max(width, el.scrollWidth);
                  }
                }

                return width;
              }

              _.each(rows, function (row) {
                numColumns = Math.min(row.children.length, numColumns);
              });

              resetColumnSize();

              for (i = 0, l = numColumns; i < l; ++i) {
                totalWidthByCol[i] = getColumnSize(i);
              }

              totalSize = _.reduce(
                totalWidthByCol,
                function (sum, num) {
                  return sum + num;
                },
                0
              );

              minCellSize = 100 / numColumns / 2;
              percLeft = 100 - minCellSize * numColumns;

              $element.css(
                'min-width',
                rows[0].children.length * minColumnSize + 'px'
              );

              _.each(totalWidthByCol, function (width, index) {
                var cell = rows[0].children[index];

                cell.style.width =
                  parseInt((width / totalSize) * percLeft, 10) +
                  minCellSize +
                  '%';
              });
            }

            controller.register = function (row) {
              rows.push(row);
              controller.invalidate();
            };

            controller.unregister = function (row) {
              var index = _.indexOf(rows, row);
              if (index !== -1) {
                rows.splice(index, 1);
              }
              controller.invalidate();
            };

            controller.invalidate = function () {
              if (timerId !== -1) {
                clearTimeout(timerId);
              }
              timerId = setTimeout(onTimeout, 0);
            };

            return controller;
          },
        ],
        link: function (scope, element, attrs, zsCrudTableAutoSize) {
          zsCrudTableAutoSize.invalidate();
        },
      };
    },
  ]);
})();
