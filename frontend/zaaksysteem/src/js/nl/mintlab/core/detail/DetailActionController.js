// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.core.detail')
    .controller('nl.mintlab.core.detail.DetailActionController', [
      '$scope',
      function ($scope) {
        function confirm(action) {
          $scope.confirm(
            action.data.confirm.label,
            action.data.confirm.verb,
            function () {
              performAction(action);
            }
          );
        }

        function performAction(action) {
          switch (action.type) {
            case 'popup':
              $scope.openPopup();
              break;

            case 'update':
              $scope.update(action);
              break;

            case 'delete':
              $scope.del(action);
              break;
          }
        }

        $scope.handleActionClick = function (action /*, event*/) {
          if (action.data.confirm) {
            confirm(action);
          } else {
            performAction(action);
          }
        };
      },
    ]);
})();
