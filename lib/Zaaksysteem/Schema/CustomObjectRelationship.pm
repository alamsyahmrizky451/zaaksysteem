use utf8;
package Zaaksysteem::Schema::CustomObjectRelationship;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CustomObjectRelationship

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<custom_object_relationship>

=cut

__PACKAGE__->table("custom_object_relationship");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'custom_object_relationship_id_seq'

=head2 relationship_type

  data_type: 'text'
  is_nullable: 0

=head2 relationship_magic_string_prefix

  data_type: 'text'
  is_nullable: 1

=head2 custom_object_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 custom_object_version_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 related_document_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 related_case_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 related_custom_object_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 related_uuid

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=head2 related_person_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 related_organization_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 related_employee_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "custom_object_relationship_id_seq",
  },
  "relationship_type",
  { data_type => "text", is_nullable => 0 },
  "relationship_magic_string_prefix",
  { data_type => "text", is_nullable => 1 },
  "custom_object_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "custom_object_version_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "related_document_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "related_case_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "related_custom_object_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "related_uuid",
  { data_type => "uuid", is_nullable => 0, size => 16 },
  "related_person_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "related_organization_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "related_employee_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 custom_object_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::CustomObject>

=cut

__PACKAGE__->belongs_to(
  "custom_object_id",
  "Zaaksysteem::Schema::CustomObject",
  { id => "custom_object_id" },
);

=head2 custom_object_version_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::CustomObjectVersion>

=cut

__PACKAGE__->belongs_to(
  "custom_object_version_id",
  "Zaaksysteem::Schema::CustomObjectVersion",
  { id => "custom_object_version_id" },
);

=head2 related_case_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to(
  "related_case_id",
  "Zaaksysteem::Schema::Zaak",
  { id => "related_case_id" },
);

=head2 related_custom_object_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::CustomObject>

=cut

__PACKAGE__->belongs_to(
  "related_custom_object_id",
  "Zaaksysteem::Schema::CustomObject",
  { id => "related_custom_object_id" },
);

=head2 related_document_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->belongs_to(
  "related_document_id",
  "Zaaksysteem::Schema::File",
  { id => "related_document_id" },
);

=head2 related_employee_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to(
  "related_employee_id",
  "Zaaksysteem::Schema::Subject",
  { id => "related_employee_id" },
);

=head2 related_organization_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Bedrijf>

=cut

__PACKAGE__->belongs_to(
  "related_organization_id",
  "Zaaksysteem::Schema::Bedrijf",
  { id => "related_organization_id" },
);

=head2 related_person_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::NatuurlijkPersoon>

=cut

__PACKAGE__->belongs_to(
  "related_person_id",
  "Zaaksysteem::Schema::NatuurlijkPersoon",
  { id => "related_person_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-07-28 15:04:45
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Be9dJnJW5NjF0p87GemkeQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2020, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
