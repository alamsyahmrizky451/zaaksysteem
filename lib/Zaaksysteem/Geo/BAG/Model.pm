package Zaaksysteem::Geo::BAG::Model;
use Moose;

with 'MooseX::Log::Log4perl';

use BTTW::Tools;
use Moose::Util::TypeConstraints qw(enum);
use Zaaksysteem::Constants qw(BAG_TYPES);
use Zaaksysteem::Geo::BAG::Object;
use Zaaksysteem::Geo::BAG::Connection::ES;
use Zaaksysteem::Geo::BAG::Connection::Spoof;
use Zaaksysteem::Types qw(NonEmptyStr);

=head1 NAME

Zaaksysteem::Geo::BAG::Model - Model for the BAG, allows searching etc.

=head1 SYNOPSIS

    my $bag = Zaaksysteem::Geo::BAG::Model->new(
        connection => $connection,
        priority_gemeentes => ['Amsterdam'],
    );

    my $results1 = $bag->search(query => "1114AD 90");
    my $results2 = $bag->search(query => "Kerkstr 50");

=head1 ATTRIBUTES

=head2 connection

An object that implements L<Zaaksysteem::Geo::BAG::Connection>, used to search
the BAG.

=cut

has connection => (
    is       => 'ro',
    does     => 'Zaaksysteem::Geo::BAG::Connection',
    required => 1,
);

=head1 METHODS

=head2 connection_from_config

Create an appropriate new L<Zaaksysteem::Geo::BAG::Connection> instance, based
on the supplied configuration flags:

=over

=item * bag_spoof_mode

=item * bag_priority_gemeentes

=item * bag_local_only

=item * es_connection

Used to create an "Elasticsearch" connection instance.

=item * home

Used to create a "Spoof" connection instance.

=back

=cut

sub connection_from_config {
    my $class = shift;
    my %config = @_;

    my $connection;

    if ($config{bag_spoof_mode}) {
        $connection = Zaaksysteem::Geo::BAG::Connection::Spoof->new(
            home               => $config{home},
            priority_gemeentes => $config{bag_priority_gemeentes},
            priority_only      => $config{bag_local_only},
        );
    } else {
        $connection = Zaaksysteem::Geo::BAG::Connection::ES->new(
            es                 => $config{es_connection},
            priority_gemeentes => $config{bag_priority_gemeentes},
            priority_only      => $config{bag_local_only},
        );
    }

    return $connection;
}

=head2 new_from_config

Return a new L<Zaaksysteem::Geo::BAG::Model> instance, with a fresh connection.

See L</connection_from_config> for the required connection parameters/method
arguments.

=cut

sub new_from_config {
    my $class = shift;

    return $class->new(
        connection => $class->connection_from_config(@_)
    );
}

=head2 parse_search_term

Parse a "Google Maps" style attribute value into its component parts (if
possible), so the caller can decide whether call L</get_exact> or L</search>.

=cut

sub parse_search_term {
    my $self = shift;
    my $term = shift;

    # The Google Maps attribute saves the *human readable* address instead of the identification.
    # We need to parse it and do a BAG search to get it back.
    my ($huisnummer, $huisletter, $huisnummertoevoeging, $postcode) =
        $term =~ /([0-9]+)([a-zA-Z])?(?:-([^,]+))?, ([0-9]{4}\ ?[A-Z]{2})/;

    if (!$postcode || !$huisnummer) {
        # Possibly an old-style value
        # (Google doesn't know the difference between huisnummertoevoeging and huisletter)
        ($huisnummer, $postcode) =
            $term =~ /([0-9]+)(?:[^,]*), ([0-9]{4}\ ?[A-Z]{2})/;
    }

    return unless ($postcode && $huisnummer);

    return {
        postcode   => $postcode,
        huisnummer => $huisnummer,
        (defined $huisletter)
            ? (huisletter => $huisletter)
            : (),
        (defined $huisnummertoevoeging)
            ? (huisnummer_toevoeging => $huisnummertoevoeging)
            : (),
    }
}

=head2 search

Search the BAG for the given text string.

The string will be parsed and passed on to the L</connection>.

Accepts one named argument:

=over

=item * type [enum(BAG_TYPES)]

BAG type (or types) to search for.

If multiple types are specified, a multi-index search is performed.

=item * query [Str]

The text to search for.

=back

=cut

define_profile search => (
    required => {
        type => enum(BAG_TYPES),
        query => NonEmptyStr,
    },
);

sub search {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    $self->log->trace("Performing full-text BAG search, term = '$args->{query}'");

    if (!ref $args->{type}) {
        $args->{type} = [$args->{type}];
    }

    my $results = $self->connection->search(
        type  => $args->{type},
        query => $args->{query},
    );

    return [
        map { Zaaksysteem::Geo::BAG::Object->new(bag_object => $_) } @$results
    ];
}

=head2 get_exact

Search the BAG for a "nummeraanduiding" object that matches the given fields
(that have to match completely).

The values will be transformed and passed on to the L</connection>.

Requires the following named arguments:

=over

=item * postcode

=item * huisnummer

=item * huisletter (optional)

=item * huisnummer_toevoeging (optional)

=back

=cut

define_profile get_exact => (
    required => {
        postcode              => "Str",
        huisnummer            => "Num",
    },
    optional => {
        huisletter            => "Str",
        huisnummer_toevoeging => "Str",
    },
    field_filters => {
        # Normalize the postal code to no spaces, uppercase
        postcode => sub {
            my $pc = shift;
            $pc =~ s/ //g;
            return uc($pc);
        }
    }
);

sub get_exact {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $results = $self->connection->get_exact(
        type  => 'nummeraanduiding',
        fields => {
            postcode              => $args->{postcode},
            huisnummer            => $args->{huisnummer},
            huisletter            => $args->{huisletter} // '',
            huisnummer_toevoeging => $args->{huisnummer_toevoeging} // '',
        }
    );

    if (@$results == 0) {
        throw(
            'geo/bag/model/no_results',
            sprintf(
                'No results for exact BAG search for (postcode: %s, huisnummer: %s, huisletter: %s, toevoeging: %s)',
                $args->{postcode}              // '(none)',
                $args->{huisnummer}            // '(none)',
                $args->{huisletter}            // '(none)',
                $args->{huisnummer_toevoeging} // '(none)',
            ),
        );
    } elsif (@$results > 1) {
        $self->log->info(
            sprintf('Multiple (%d) results for exact BAG search; returning the first result', scalar @$results),
        );
    }

    return Zaaksysteem::Geo::BAG::Object->new(bag_object => $results->[0]);
}

=head2 find_nearest

Find the nearest BAG object of the specified type (with a maximum of 200 meters).

=cut

define_profile find_nearest => (
    required => {
        type      => enum(BAG_TYPES),
        latitude  => "Num",
        longitude => "Num",
    },
);

sub find_nearest {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    $self->log->trace(sprintf(
        "Performing nearest search, type = %s, lat = %s, lon = %s",
        $args->{type},
        $args->{latitude},
        $args->{longitude}
    ));

    my $result = $self->connection->find_nearest(
        type      => $args->{type},
        latitude  => $args->{latitude},
        longitude => $args->{longitude},
    );

    return unless $result;
    return unless @$result > 0;
    return Zaaksysteem::Geo::BAG::Object->new(bag_object => $result->[0]);
}

=head2 get

Retrieve a specific BAG object, specified by type and identifier.

=cut

sub get {
    my $self = shift;
    my $object_type = shift;
    my $object_id = shift;

    my $result = $self->connection->get(
        type => $object_type,

        id   => "$object_id",
    );

    if ($result) {
        return Zaaksysteem::Geo::BAG::Object->new(bag_object => $result);
    }

    $self->log->info("Unable to find BAG object for $object_id of type $object_type");
    return;
}

sub convert_googlemap_value_to_bag {
    my ($self, $value) = @_;

    my $bag_query = $self->parse_search_term($value);
    throw('bag/googlemaps/search_term/invalid',
        "No BAG query could be generated for '$value'")
        unless $bag_query;

    return $self->get_exact(%$bag_query);
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
