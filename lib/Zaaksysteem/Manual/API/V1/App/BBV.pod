=head1 NAME

Zaaksysteem::Manual::API::V1::App::BBV - BBV App related API functions

=head1 Description

This API-document describes the usage of our JSON BBV-App API. Via the BBV API it is
possible to update specific fields in zaaksysteem for logged in users.

=head1 URL

The base URL for this API is:

    /api/v1/map/app/bbvapp

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head1 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 update_attributes

   /api/v1/app/bbvapp/case/UUID/update_attributes

Update various attributes of a case by a single call.

B<Properties>

=over

=item values [required]

B<TYPE>: Object

A key-value pair of attributes you would like to create the case with. Make sure every value is wrapped in an array, like
you see in the example below. For more information about the format of providing values, please look at our
general API documentation: L<Zaaksysteem::Manual::API::V1>

=begin javascript

"values" : {
   "test_comment" : [
      "test1"
   ]
}

=end javascript

=back

B<Example call>

 https://localhost/api/v1/app/bbvapp/case/fed21f3d-9b61-46fb-969c-d387b8ef2ece/update_attributes

B<Request JSON>

=begin javascript

Request:
{
   "values" : {
      "test_comment" : [
         "test1"
      ]
   }
}

=end javascript

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-bfcc6e-58e455",
   "development" : false,
   "result" : {
      "instance" : {
         "attributes" : {
            "checkbox_agreed" : [
               null
            ],
            "test_comment" : [
               "test1"
            ],
            "test_conclusion" : [
               null
            ],
            "test_decision" : [],
            "test_paspoort" : []
         },
         "casetype" : {
            "instance" : {
               "name" : "QLmp9TXkb2"
            },
            "reference" : "11fa80c4-a0ab-4fc4-9183-b464a239b86e",
            "type" : "casetype"
         },
         "child_relations" : {
            "instance" : {
               "rows" : []
            },
            "type" : "set"
         },
         "date_of_registration" : "2016-02-25T15:27:42Z",
         "date_target" : "2016-03-01T15:27:42Z",
         "id" : "fed21f3d-9b61-46fb-969c-d387b8ef2ece",
         "number" : 615,
         "parent_relation" : {},
         "phase" : "afhandelfase",
         "relations" : {
            "instance" : {
               "rows" : []
            },
            "type" : "set"
         },
         "result" : null,
         "status" : "new",
         "subject_external" : null
      },
      "reference" : "fed21f3d-9b61-46fb-969c-d387b8ef2ece",
      "type" : "case"
   },
   "status_code" : 200
}

=end javascript

=head1 Objects

=head2 case

Returns a C<case> object. See L<Zaaksysteem::Manual::API::V1::Case> for more information

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::App>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
