=head1 NAME

Zaaksysteem::Manual::API::V1::Address - Zaaksysteem Address API

=head1 Description

This API-document describes the usage of our address API. This API will only work with a working interface configuration of <Zaaksysteem::Backend::Sysin::Modules::OverheidIO> which is enabled for C<BAG> search requests. You cannot add or remove addresses, as it currently only supports search functionality.

=head2 URL

The base URL for this API is:

    /api/v1/address

Make sure you use the HTTP Method C<GET> for retrieving.

=head2 search

   /api/v1/address/search

Searching for addresses using the OverheidIO BAG is possible on multiple fields. Currently the the following searchable fields are supported: C<zipcode>, C<street_number>, C<street_number_suffix> and C<street_number_letter>. You can only match on full text.

B<Example call>

 https://localhost/api/v1/address/search/?query:match:zipcode=1011PZ&query:match:street_number=1

B<Request JSON>

Any request data in JSON is ignored.

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "development" : true,
   "request_id" : "vagrant-90c223-2e8667",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 1,
            "total_rows" : 1
         },
         "rows" : [
            {
               "instance" : {
                  "city" : "Amsterdam",
                  "country" : {
                     "instance" : {
                        "alpha_one" : null,
                        "alpha_two" : null,
                        "alpha_three" : null,
                        "code" : null,
                        "dutch_code" : "6030",
                        "label" : "Nederland"
                     },
                     "reference" : null,
                     "type" : "country_code"
                  },
                  "date_created" : "2017-02-03T12:25:21Z",
                  "date_modified" : "2017-02-03T12:25:21Z",
                  "foreign_address_line1" : null,
                  "foreign_address_line2" : null,
                  "foreign_address_line3" : null,
                  "municipality" : {
                     "instance" : {
                        "date_created" : "2017-02-03T12:25:21Z",
                        "date_modified" : "2017-02-03T12:25:21Z",
                        "dutch_code" : "363",
                        "label" : "Amsterdam"
                     },
                     "reference" : "0ad5aae2-a609-4a7e-b87e-3b44c7f2c663",
                     "type" : "municipality_code"
                  },
                  "street" : "Muiderstraat",
                  "street_number" : "1",
                  "street_number_letter" : null,
                  "street_number_suffix" : null,
                  "zipcode" : "1011PZ"
               },
               "reference" : null,
               "type" : "address"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}


=end javascript

=head2 SEE ALSO

L<Zaaksysteem::Manual::API::V1>


=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
