=head1 NAME

Zaaksysteem::Manual::API::V1::Namespaces::Search - Search API reference
documentation

=head1 NAMESPACE URL

    /api/v1/search

=head1 DESCRIPTION

This page documents the endpoints within the C<search> API namespace.

=head1 ENDPOINTS

=head2 Core endpoints

=head3 C<GET />

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> instance.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
