package Zaaksysteem::Zaken::Roles::BetrokkenenSetup;

use Moose::Role;
use List::Util qw(any);
use BTTW::Tools;

requires qw(log);

with 'Zaaksysteem::Zaken::Betrokkenen';

sub _create_case_betrokkene_type {
    my ($self, $type, $betrokkene, $opts) = @_;

    my $rv = $self->betrokkene_set($opts, $type, $betrokkene);

    return unless $rv;

    return {
        id                    => $rv,
        verificatie           => $betrokkene->{verificatie},
        betrokkene_identifier => $betrokkene->{betrokkene},
    };
}

around '_create_zaak' => sub {
    my $orig            = shift;
    my $self            = shift;
    my ($opts)          = @_;

    my %update = ();
    for my $betrokkene_target (qw/aanvrager behandelaar coordinator/) {
        my $betrokkene_target_p = $betrokkene_target . 's'; # PLURAL

        next unless $opts->{$betrokkene_target_p};

        unless (ref $opts->{$betrokkene_target_p} eq 'ARRAY') {
            $self->log->warn(
                'Geen aanvragers meegegeven of aanvragers != ARRAY'
            );
            return;
        }

        for my $betrokkene (@{ $opts->{ $betrokkene_target_p } }) {
            $update{$betrokkene_target} = $self->_create_case_betrokkene_type(
                $betrokkene_target, $betrokkene, $opts
            );
        }
    }

    my $zaak = $self->$orig(@_);

    while (my ($betrokkene_target, $betrokkene_info) = each %update) {
        my $betrokkene = $self->result_source->schema->resultset('ZaakBetrokkenen')->find(
            $betrokkene_info->{id}
        );

        $betrokkene->zaak_id($zaak->id);
        $betrokkene->verificatie($betrokkene_info->{verificatie});
        $betrokkene->update;
    }

    my $requestor = $zaak->aanvrager_object;
    if (!$requestor) {
        throw('Zaaksysteem/case/create',
            'Geen aanvrager voor zaak, is toch echt verplicht');
    }

    my $id = $update{aanvrager}{betrokkene_identifier};
    my $preset = $zaak->zaaktype_node_id->zaaktype_definitie_id->preset_client;
    if ($id eq ($preset //'')) {
        $zaak->update({ preset_client => 1 });
    }

    $self->_betrokkene_setup_add_relaties(
        $zaak, $opts
    );

    return $zaak;
};

sub _betrokkene_setup_add_relaties {
    my ($self, $zaak, $opts) = @_;

    if ($opts->{betrokkene_relaties}) {
        for my $relatie (
            @{ $opts->{betrokkene_relaties} }
        ) {
            $zaak->betrokkene_relateren(
                $relatie
            );
        }
    }

    if ($opts->{ontvanger}) {
        $zaak->betrokkene_relateren(
            {
                betrokkene_identifier   => $opts->{ontvanger},
                rol                     => 'Ontvanger',
                magic_string_prefix     => 'ontvanger',
            }
        );
    }

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
