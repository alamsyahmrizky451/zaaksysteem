package Zaaksysteem::DB::Component::Logging::Case::SendBerichtenbox;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

use HTML::Strip;

use Encode qw(encode_utf8 decode_utf8);

=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::SendBerichtenbox
- Event message handler for case/send_berichtenbox events.

=head1 DESCRIPTION

See L<Zaaksysteem::DB::Component::Logging::Event>.

=head1 METHODS

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $self = shift;

    my $data = $self->data;
    my ($message_subject) = HTML::Strip->new->parse(encode_utf8($data->{ message_subject })) =~ m/^\s*(.*?)\s*$/;
    my $onderwerp = sprintf('Bericht "%s" verstuurd naar MijnOverheid Berichtenbox voor de aanvrager', decode_utf8($message_subject));

    return $onderwerp;
}

=head2 event_category

defines the category, used by the frontend in the timeline, to display the right
icons etc.

=cut

sub event_category { 'contactmoment'; }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
