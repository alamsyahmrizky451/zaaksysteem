package Zaaksysteem::General::Actions::Dispatch;
use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::General::Actions::Dispatch - Namespace for dispatching actions

=head1 DESCRIPTION

This class is here to be able to test dispatching actions in use for Zaaksysteem.pm

=cut

use Log::Log4perl::MDC;

=head2 dispatch_query_statistics

Dispatch DB query stats to statsd and log4perl

=cut

sub dispatch_query_statistics {
    my ($c, $schema, $reset) = @_;

    ### When reset is given, we are at the beginning of a request
    my $storage = $schema->storage;
    if ($reset) {
        $storage->reset_query_counters;
        return;
    }

    $c->set_log_context('database_mode', $schema->read_only ? 'ro' : 'rw' );

    my $queries     = $storage->query_list;
    my $query_count = scalar @$queries;
    return unless $query_count;

    Log::Log4perl::MDC->put('sql_query_count', $query_count);
    $c->statsd->increment('database.num_queries.count', $query_count);

    my $total_query_time = 0;
    $total_query_time += $_->{took} for @$queries;

    my $avg_query_time  = $total_query_time / $query_count;
    $total_query_time   = $total_query_time;

    if ($ENV{ZS_DBIC_TRACE}) {
        foreach (@$queries) {
            $c->log->info(sprintf("%s ms: %s", $_->{took}, $_->{sql}));
        };
        $c->log->info("Query time: $total_query_time ms");
        $c->log->info("DB queries: $query_count");
        $c->log->info("Avg query time: $avg_query_time ms");
    }

    $avg_query_time = int($avg_query_time);
    $total_query_time = int($total_query_time);

    Log::Log4perl::MDC->put('sql_query_time_avg', $avg_query_time);
    Log::Log4perl::MDC->put('sql_query_time', $total_query_time);

    $c->statsd->timing('database.query_time', $total_query_time);
    $c->statsd->timing('database.query_time_avg', $avg_query_time);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
