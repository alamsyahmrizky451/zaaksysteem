package Zaaksysteem::Controller::Beheer::Bibliotheek::CustomObject;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

sub search
    : Chained('/beheer/bibliotheek/base')
    : PathPart('custom_object/search')
    : Args() {
    my ($self, $c) = @_;

    $c->stash->{nowrapper} = 1;

    $c->stash->{bib_type} = "custom_object";

    $c->stash->{bib_cat} = $c->model('DB::BibliotheekCategorie')
        ->search({ 'system' => undef, }, { order_by => ['pid', 'naam'] });

    my $params = $c->req->params();

    if (!$params->{search}) {
        $c->stash->{template} = 'widgets/beheer/bibliotheek/search.tt';
        $c->detach();
        return;
    }

    my %where;
    if ($params->{naam}) {
        my $like = { ilike => "%$params->{naam}%" };
        %where = (
            -or => [
                { 'custom_object_type_version_id.name'  => $like },
                { 'custom_object_type_version_id.title'  => $like },
            ]
        );
    }
    if ($params->{bibliotheek_categorie_id}) {
        $where{'me.catalog_folder_id'} = $params->{bibliotheek_categorie_id};
    }

    my $rs = $c->model('DB::CustomObjectType')->search_rs(
        {
            %where
        },
        { join => 'custom_object_type_version_id' }
    );

    my @json;
    while (my $type = $rs->next) {
        my $category = $type->catalog_folder_id;

        push(
            @json,
            {
                'naam'       => $type->custom_object_type_version_id->name,
                'invoertype' => $c->stash->{bib_type},
                'categorie'  => $category ? $category->naam : "-",
                'id'         => $type->id,
                'uuid'       => $type->get_column('uuid'),
            }
        );
    }
    $c->stash->{json} = \@json;
    $c->detach('Zaaksysteem::View::JSONlegacy');
    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 search

TODO: Fix the POD

=cut

