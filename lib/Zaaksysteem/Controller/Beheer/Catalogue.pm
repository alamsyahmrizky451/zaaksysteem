package Zaaksysteem::Controller::Beheer::Catalogue;

use Moose;
use namespace::autoclean;

use Hash::Merge::Simple qw( clone_merge );

BEGIN { extends 'Zaaksysteem::Controller' }

Params::Profile->register_profile(
    method  => '_get_crumb_path',
    profile => {
        required => [ qw/bibliotheek_categorie_id c/ ]
    }
);

sub _get_crumb_path {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _get_crumb_path" unless $dv->success;

    my $c                        = $params->{c};
    my $bibliotheek_categorie_id = $params->{bibliotheek_categorie_id};

    return '' if not defined $bibliotheek_categorie_id;

    my $category = $c->model('DB::BibliotheekCategorie')->find($bibliotheek_categorie_id);
    my $pid = $category->get_column('pid');

    $c->stash->{crumbs} ||= [];
    unshift (@{ $c->stash->{crumbs}}, {id => $category->id, name => $category->naam});

    if($pid) {
        $self->_get_crumb_path({
            c => $c,
            bibliotheek_categorie_id => $pid,
        });
    }
}



Params::Profile->register_profile(
    method  => '_list_child_categories',
    profile => {
        required => [ qw/bibliotheek_categorie_id c tablename/ ]
    }
);

sub _list_child_categories {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _list_child_categories" unless $dv->success;

    my $bibliotheek_categorie_id = $params->{bibliotheek_categorie_id};
    my $c = $params->{c};
    my $tablename = $params->{tablename};

    my $child_categories = $c->model('DB::BibliotheekCategorie')->search({
        pid => $bibliotheek_categorie_id
    });

    my $category_field = $tablename eq 'DB::BibliotheekCategorie' ? 'pid' :
        'bibliotheek_categorie_id';

    my $element_count = $c->model($tablename)->search({
        $category_field => $bibliotheek_categorie_id
    })->count();

    $c->stash->{child_category_ids} ||= [];
    if($bibliotheek_categorie_id) {
        push @{$c->stash->{child_category_ids}}, ''. $bibliotheek_categorie_id;
    }
    my %id_lookup = map { $_ => 1 } @{$c->stash->{child_category_ids}};

    while(my $row = $child_categories->next()) {
        # protect against cyclical child/parent relationships
        next if exists $id_lookup{$row->id};

        push @{$c->stash->{child_category_ids}}, '' . $row->id;  # stringify using ''
        $element_count += $self->_list_child_categories({
            c => $c,
            bibliotheek_categorie_id => $row->id,
            tablename => $tablename
        });
    }

    $c->stash->{element_count} ||= {};
    $c->stash->{element_count}->{$bibliotheek_categorie_id||'root'} = $element_count;

    return $element_count;
}


sub _filter {
    my ($self, $textfilter, @items) = @_;

    my @results = ();
    foreach my $item (@items) {
        if($item->{naam} =~ m|$textfilter|) {
            push @results, $item;
        }
    }
    return @results;
}


sub list : Chained('/') : PathPart('beheer/catalogus') : Args(1) {
    my ($self, $c, $bibliotheek_categorie_id) = @_;

    $c->stash->{bibliotheek_categorie_id} = $bibliotheek_categorie_id;

    my $parent_categorie = $c->model('DB::BibliotheekCategorie')->
        find($bibliotheek_categorie_id);

    if($parent_categorie && $parent_categorie->pid) {
        $c->stash->{parent_categorie_id} = $parent_categorie->pid->id;
    }

    $c->forward('base');
}


sub base : Chained('/') : PathPart('beheer/catalogus') : Args(0) {
    my ($self, $c) = @_;

    my $params = $c->req->params();
    my $page       = $params->{'page'} || 1;
    my $textfilter = $params->{'textfilter'};


    my $bibliotheek_categorie_id = $c->stash->{bibliotheek_categorie_id};

    my $tables = {
        BibliotheekCategorie => {
            pid => $bibliotheek_categorie_id
        },
        BibliotheekKenmerken => {
            bibliotheek_categorie_id    => $bibliotheek_categorie_id,
            deleted                     => undef,
        },
        BibliotheekSjablonen => {
            bibliotheek_categorie_id    => $bibliotheek_categorie_id,
            deleted                     => undef,
        },
        Zaaktype => {
            bibliotheek_categorie_id => $bibliotheek_categorie_id,
            deleted => undef,
        },
        BibliotheekNotificaties => {
            bibliotheek_categorie_id    => $bibliotheek_categorie_id,
            deleted                     => undef,
        },
    };

    my $resultsets = {};
    foreach my $tablename (keys %$tables) {
        my $query = $tables->{$tablename};
        if($textfilter) {
            $query->{'lower(search_term)'} = {
                like    => '%' . lc($textfilter) . '%'
            };

            my $category_field = $tablename eq 'BibliotheekCategorie' ? 'pid' :
                'bibliotheek_categorie_id';

            delete $query->{$category_field};

            if($bibliotheek_categorie_id) {
                my $total_cats = $self->_list_child_categories({
                    c           => $c,
                    tablename   => 'DB::'.$tablename,
                    bibliotheek_categorie_id => $bibliotheek_categorie_id,
                });
                $query->{$category_field} = {
                    '-in' => $c->stash->{'child_category_ids'}
                };
            }
        }

        $resultsets->{$tablename} = $c->model('DB::'.$tablename)->search($query, {
            select => ['id', 'object_type'],
            order_by => ['naam'],
        });
        $resultsets->{$tablename}->result_class(
            'DBIx::Class::ResultClass::HashRefInflator');
    }

    my $resultset = $resultsets->{Zaaktype}->union_all([
        $resultsets->{BibliotheekCategorie},
        $resultsets->{BibliotheekKenmerken},
        $resultsets->{BibliotheekSjablonen},
    ])->search(undef, {
        page => $page
    });


    $c->stash->{results} = $resultset;

    my $bla = $c->model('DB::BibliotheekCategorie')->search({
        pid => $bibliotheek_categorie_id,
    },
    {
        page => $page,
    });

    if($bibliotheek_categorie_id) {
        $self->_get_crumb_path({
            c => $c,
            bibliotheek_categorie_id => $bibliotheek_categorie_id,
        });
    }

    $c->stash->{template} = 'beheer/catalogue/catalogue.tt';
}


__PACKAGE__->meta->make_immutable;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 list

TODO: Fix the POD

=cut

