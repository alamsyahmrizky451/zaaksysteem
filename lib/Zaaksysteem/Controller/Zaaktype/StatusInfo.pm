package Zaaksysteem::Controller::Zaaktype::StatusInfo;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Zaaksysteem::Controller::Zaaktype::StatusInfo in Zaaktype::StatusInfo.');
}

sub edit : Chained('/zaaktype/base'): PathPart('statusinfo/edit'): Args(0) {
    my ($self, $c) = @_;

    if (%{ $c->req->params }) {
        $c->response->redirect($c->uri_for('/zaaktype/statusinfo/edit'));
    }

    $c->stash->{template} = 'zaaktype/statusinfo/edit.tt';

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 edit

TODO: Fix the POD

=head2 index

TODO: Fix the POD

=cut

