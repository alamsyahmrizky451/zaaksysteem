package Zaaksysteem::Controller::Plugins::Digid;
use Moose;

use BTTW::Tools;
use Carp qw(croak);
use JSON;
use List::Util qw(none);
use MIME::Base64;
use Moose::Util qw/apply_all_roles does_role/;
use Zaaksysteem::Types qw(BSN);

BEGIN { extends 'Zaaksysteem::Controller' }

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_EIDAS
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID

    VALIDATION_CONTACT_DATA
    VALIDATION_EXTERNAL_CONTACT_DATA

    :SUBJECT_TYPES
/;

sub login : Chained('/') : PathPart('auth/digid'): Args() {
    my ($self, $c, $do_auth) = @_;

    if ($c->user_exists) {
        $c->delete_session;
    }

    ### In case of an XHR, and we get here...define we are not logged in
    if ($c->req->is_xhr) {
        $c->res->status('401');
    }

    $c->stash->{ success_endpoint } = $c->req->param('success_endpoint');
    $c->stash->{ failure_endpoint } = $c->req->uri;

    $c->session->{_saml_endpoints}  = {
          success => $c->stash->{success_endpoint},
          failure => $c->stash->{failure_endpoint},
    };

    if ($c->session->{_saml_error}) {
        $c->stash->{saml_error} = $c->session->{_saml_error};
    }

    $c->stash->{ idps } = [ $c->model('DB::Interface')->search_module('samlidp', '$.login_type_citizen') ];

    $c->create_zs_login_token();
    $c->stash->{template} = 'plugins/digid/login.tt';
    $c->stash->{ page_title } = 'Inloggen met DigiD';
}

sub logout : Chained('/') : PathPart('auth/digid/logout'): Args() {
    my ($self, $c) = @_;

    ### Copy information from session into stash
    $c->stash->{digid_error} = $c->session->{digid_error} if $c->session->{digid_error};

    $c->delete_session();

    $c->stash->{template}   = 'plugins/digid/login.tt';
    $c->stash->{logged_out} = 1;
    $c->detach;
}


sub _zaak_create_secure_digid : Private {
    my ($self, $c) = @_;

    my $saml_state = $c->session->{ _saml } || {};

    my $params = $c->req->params();
    my $authentication_method = $params->{authenticatie_methode} //
        $c->session->{_zaak_create}{extern}{verified} // '';

    return if !$self->_check_verified($authentication_method);

    if(!$saml_state->{ success }) {
        $c->log->debug("SAML not (yet) successful, redirect to login page");
        my %arguments = (
            authenticatie_methode => $authentication_method,
            ztc_aanvrager_type    => 'natuurlijk_persoon',
            sessreset             => $params->{sessreset} ? 1 : 0,
        );

        my $zaaktype = $params->{zaaktype_id};
        if ($zaaktype =~ /^[0-9]+$/) {
            $arguments{zaaktype_id} = $zaaktype;
        }

        $c->res->redirect(
            $c->uri_for(
                '/auth/digid',
                {
                    success_endpoint => $c->uri_for(
                        '/zaak/create/webformulier/', \%arguments,
                    ),
                    failure_endpoint =>
                        $c->session->{_saml_endpoints}{failure},
                }
            )
        );

        delete($c->session->{_zaak_create}{extern});
        $c->detach;
    }

    my %zaak_create = (
        aanvrager_type => 'natuurlijk_persoon',
        verified       => $authentication_method,
        id             => $authentication_method eq 'digid'
            ? $saml_state->{uid}
            : $saml_state->{nameid},
    );

    $c->session->{_zaak_create}{extern} = \%zaak_create;

    $c->log->info("SAML login successful, create case as NP");

    $c->stash->{aanvrager_type} = 'natuurlijk_persoon';
    $c->forward('_zaak_create_aanvrager');
}

sub _zaak_create_aanvrager : Private {
    my ($self, $c) = @_;


    if (!$c->req->params->{aanvrager_update}) {
        return;
    }

    my $zaaktype = $c->model('DB::Zaaktype')->find(
        $c->session->{_zaak_create}{zaaktype_id}
    );

    my $zaaktype_node = $zaaktype->zaaktype_node_id;

    my $external_validation_profile = $c->user_exists
            ? VALIDATION_CONTACT_DATA
            : VALIDATION_EXTERNAL_CONTACT_DATA->($zaaktype_node);

    ### Only validate contact, which are all optional
    my $profile;
    if ($c->req->params->{contact_edit}) {
        $profile = $external_validation_profile;
    } else {
        ### Get profile from Model
        $profile         = $c->get_profile(
            'method'    => 'create',
            'caller'    => 'Zaaksysteem::Controller::Betrokkene'
        ) or die('Terrible die here');

        ### MERGE
        my $contact_profile = $external_validation_profile;
        while (my ($key, $data) = each %{ $contact_profile }) {
            unless ($profile->{$key}) {
                $profile->{$key} = $data;
                next;
            }

            if (UNIVERSAL::isa($data, 'ARRAY')) {
                push(@{ $profile->{$key} }, @{ $data });
                next;
            }

            if (UNIVERSAL::isa($data, 'HASH')) {
                while (my ($datakey, $dataval) = each %{ $data }) {
                    $profile->{$key}->{$datakey} = $dataval;
                }
                next;
            }
        }
    }

    Params::Profile->register_profile(
        method => '_zaak_create_aanvrager',
        profile => $profile,
    );

    if ($c->req->is_xhr) {
        $c->zvalidate;
        my $msgs = $c->stash->{json}{msgs} || {};
        my $params = $c->req->params;

        # Hack in place for double creation of BSN because I'm not sure how to
        # hack this logic into DFV
        if ($params->{create} && $params->{'np-burgerservicenummer'}) {
            my $bsn = $params->{'np-burgerservicenummer'};
            my $ok = BSN->check($bsn);
            if ($ok) {
                my $entry = $c->model('DB::NatuurlijkPersoon')->find_by_bsn(
                    $bsn,
                    {
                        authenticated => $params->{'np-authenticated'}
                            // 0
                    }
                );
                _bsn_incorrect($c,
                    "Dit burgerservicenummer bestaat al in het systeem"
                ) if $entry;
            }
            else {
                _bsn_incorrect($c, "Dit burgerservicenummer is incorrect");
            }
        }
        if ($msgs->{"npc-email"}) {
            $msgs->{"npc-email"}
                = 'Er is geen geldig e-mailadres ingevuld (voorbeeld: naam@example.com).';
        }
        if ($msgs->{"npc-telefoonnummer"}) {
            $msgs->{"npc-telefoonnummer"}
                = "Er is geen geldig telefoonnummer ingevuld (10 t/m 15 cijfers en + is toegestaan)";
        }
        if ($msgs->{"npc-mobiel"}) {
            $msgs->{"npc-mobiel"}
                = "Er is geen geldig telefoonnummer ingevuld (10 t/m 15 cijfers en + is toegestaan)";
        }
        $c->detach('Zaaksysteem::View::JSONlegacy');
    }

    my $dv      = $c->zvalidate;
    return unless ref($dv);

    return unless $dv->success;

    ### Post
    if ($c->req->params->{aanvrager_edit}) {
        $c->session->{_zaak_create}->{aanvrager_update} = $dv->valid;
    } elsif ($c->req->params->{contact_edit}) {
        for (qw/npc-email npc-telefoonnummer npc-mobiel/) {
            if (defined($c->req->params->{ $_ })) {
                $c->session->{_zaak_create}->{ $_ } =
                    $c->req->params->{ $_ };
            }
        }
    }

    if (defined $c->req->params->{'npc-preferred-contact-channel'}) {
        $c->session->{_zaak_create}->{aanvrager_update}{preferred_contact_channel}
            = $c->req->params->{'npc-preferred-contact-channel'};
    }
}

sub _bsn_incorrect {
    my ($c, $error) = @_;
    push(@{ $c->stash->{json}{invalid} }, 'np-burgerservicenummer');
    $c->stash->{json}{success} = 0;
    $c->stash->{json}{valid}
        = [grep { $_ ne 'np-burgerservicenummer' }
            @{ $c->stash->{json}{valid} }];
    $c->stash->{json}{msgs}{'np-burgerservicenummer'} = $error;
}

sub _zaak_create_load_externe_data : Private {
    my ($self, $c) = @_;


    my $verified = $c->session->{_zaak_create}{extern}{verified};
    return if !$self->_check_verified($verified);

    if (   !$c->req->params->{aanvrager_update}
        || !$c->session->{_zaak_create}{aanvrager_update})
    {
        $c->log->info(
            q{Not creating betrokkene: No 'aanvrager_update'
            parameter in session or request parameters}
        );
        return;
    }

    my $id = $c->model('Betrokkene')->create(
        'natuurlijk_persoon',
        {
            %{ $c->session->{_zaak_create}{aanvrager_update} },
            'np-authenticated'   => 0,
            'np-authenticatedby' => $verified eq 'digid'
                ? ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
                : ZAAKSYSTEEM_GM_AUTHENTICATEDBY_EIDAS
        }
    );

    if ($verified eq 'eidas' && $c->session->{_saml_state}{idp_id}) {
        my $betrokkene = $c->model('Betrokkene')->get(
            {
                type => 'natuurlijk_persoon'
            },
            $id
        );

        # eIDAS - create user_entity + subject rows.
        # We ignore the returned user_entity, because it's only really necessary
        # for a second login to be able to find the correct subject again.
        $c->model('DB::Subject')->create_from_eidas(
            idp_id       => $c->session->{_saml_state}{idp_id},
            uuid         => $betrokkene->uuid,
            subject_type => SUBJECT_TYPE_PERSON,
            username     => $c->session->{_saml}{nameid},
        );
    }

    $c->session->{_zaak_create}{ztc_aanvrager_id} = 'betrokkene-natuurlijk_persoon-' .  $id;
}

sub _check_verified {
    my ($self, $verified) = @_;
    if (none { $verified eq $_ } qw(digid eidas)) {
        return 0;
    }
    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 VALIDATION_CONTACT_DATA

TODO: Fix the POD

=cut

=head2 VALIDATION_EXTERNAL_CONTACT_DATA

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID

TODO: Fix the POD

=cut

=head2 login

TODO: Fix the POD

=cut

=head2 logout

TODO: Fix the POD

=cut

