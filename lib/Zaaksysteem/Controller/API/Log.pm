package Zaaksysteem::Controller::API::Log;
use Moose;
use namespace::autoclean;

use BTTW::Tools;
use Moose::Util::TypeConstraints qw(enum);
use Zaaksysteem::Types qw(NonEmptyStr);

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head2 post_log_message

Send a log message supplied by frontend to the log system.

Required parameters:

=over

=item * level

Log level. One of 'debug', 'info', 'warn', 'error', 'fatal'.

=item * message

Non-empty string, the message to log.

=item * context

Key/value pairs to send to the logging backend, for easy searching or
grouping of log search results (as part of the "mapped diagnostic context").

Example uses for this are things like the source file name, line number,
function name, etc.

=back

=cut

define_profile post_log_message => (
    required => {
        level => enum([qw(debug info warn error fatal)]),
        message => NonEmptyStr,
        context => 'HashRef'
    }
);

sub post_log_message : Chained('/') : PathPart('api/log/post_log_message') : DB('RO') {
    my ($self, $c) = @_;
    $c->assert_post();

    unless ($c->user_exists || $c->session->{pip}) {
        throw(
            'log/not_authorized',
            'You are not authorized to use this API',
            {http_code => 401}
        );
    }

    my $opts = assert_profile($c->req->params)->valid;

    my $level = $opts->{level};
    my $message = $opts->{message};
    my $context = $opts->{context};

    if (keys %$context > 10 || length($message) > 255) {
        throw(
            "log/message_too_long",
            "Log message too long or too much context",
        );
    }

    Log::Log4perl::MDC->put(
        "frontend_context" => $context,
    );
    $self->log->$level($message);

    $c->stash->{zapi} = ["Message logged"];
} 

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

