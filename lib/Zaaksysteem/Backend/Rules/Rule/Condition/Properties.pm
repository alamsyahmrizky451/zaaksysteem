package Zaaksysteem::Backend::Rules::Rule::Condition::Properties;

use Moose::Role;

use List::Util qw[any first];

use BTTW::Tools;
use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;
use Zaaksysteem::Attributes;

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Condition::Properties - Handles customer properties of casetype

=head1 SYNOPSIS

See L<Zaaksysteem::Backend::Rules::Rule::Condition>

=head1 DESCRIPTION

Condition role in charge of handling conditions based on current zaaktype values. Like the values
for wkpb and lex_silencio_positivo.

Make sure you can define an action based on this criteria.

=head1 CONSTRUCTION

On construction, by adding functionality to C<BUILD>, we translate the old style attributes
to the new style. E.g. lex_silencio_positivo to case.casetype.lex_silencio_positivo.

=cut

after BUILD => sub {
    my $self = shift;

    my $attribute = $self->attribute;

    if ($attribute eq 'beroep_mogelijk') {
        $attribute = 'bezwaar_en_beroep_mogelijk';
    }

    return unless any {
        $_ eq $self->attribute
    } @{ ZAAKSYSTEEM_CONSTANTS->{ CASETYPE_RULE_PROPERTIES } };

    my $attr = first {
        $_->{ bwcompat_name } eq $attribute
    } grep {
        $_->{ bwcompat_name }
    } ZAAKSYSTEEM_SYSTEM_ATTRIBUTES();

    $self->attribute($attr->{ name });
};

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules::Rule::Condition> L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

