package Zaaksysteem::Object::ValueType::Text;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object::ValueType::String';

=head1 NAME

Zaaksysteem::Object::ValueType::Text - Text values for the object
infrastructure

=head1 DESCRIPTION

This package extends from L<Zaaksysteem::Object::ValueType::String>, and
merely changes the value type name to C<text>.

The purpose of this value is higher-level than the current value type
semantics, and indicates something about the purpose of values of this type
(namely that C<text> values should be expected to contain multiple lines, or
even several paragraphs of text, while C<string> values are usually single
words, slugs, or short description sentences).

=head1 METHODS

=head2 name

Overrides L<Zaaksysteem::Object::ValueType::String/name> to return C<text>.

=cut

override name => sub {
    return 'text';
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
