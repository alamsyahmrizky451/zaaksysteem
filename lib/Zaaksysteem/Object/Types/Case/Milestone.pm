package Zaaksysteem::Object::Types::Case::Milestone;

use Moose;

use Zaaksysteem::Types qw[
    NonEmptyStr
    SequenceNumber
];

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Case::Milestone - Milestone 'progress indicator'
case objects

=head1 DESCRIPTION

A milestone abstracts the notion of progress through a case workflow.

=head1 ATTRIBUTES

=head2 milestone_label

Label of the currently attained milestone.

=cut

has milestone_label => (
    is => 'rw',
    isa => NonEmptyStr,
    label => 'Mijlpaal',
    traits => [qw[OA]],
);

=head2 milestone_sequence_number

Sequence number of the currently attained milestone.

=cut

has milestone_sequence_number => (
    is => 'rw',
    isa => SequenceNumber,
    label => 'Mijlpaalvolgnummer',
    traits => [qw[OA]]
);

=head2 phase_label

Label of the current phase.

=cut

has phase_label => (
    is => 'rw',
    isa => NonEmptyStr,
    label => 'Fase',
    traits => [qw[OA]]
);

=head2 phase_sequence_number

Sequence number of the current phase.

=cut

has phase_sequence_number => (
    is => 'rw',
    isa => SequenceNumber,
    label => 'Fasevolgnummer',
    traits => [qw[OA]]
);

=head2 last_sequence_number

Sequence number of the last phase.

=cut

has last_sequence_number => (
    is => 'rw',
    isa => SequenceNumber,
    label => 'Laaste fasevolgnummer',
    traits => [qw[OA]]
);

=head1 METHODS

=head2 type

Overrides L<Zaaksysteem::Object/type>, always returns C<case/route>.

=cut

override type => sub { 'case/milestone' };

=head2 TO_STRING

Overrides L<Zaaksysteem::Object/TO_STRING> and returns the current phase
label.

=cut

override TO_STRING => sub { shift->phase_label };

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
