package Zaaksysteem::Object::Types::Meta;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

use Zaaksysteem::Types qw(NonEmptyStr);
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Types::Meta - Built-in object type implementing
a class for Meta objects

=head1 DESCRIPTION

An object class for Zaaksysteem metadata. Think version numbering, etc.


=head1 ATTRIBUTES

=head2 label

The name of the parameter

=cut

has label => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Name of the configuration item',
    required => 1,
);

=head2 value

The value of the configuration item

=cut

has value => (
    is       => 'rw',
    isa      => 'Defined',
    traits   => [qw(OA)],
    label    => 'Value',
    required => 0,
);


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
