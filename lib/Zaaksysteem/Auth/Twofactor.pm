package Zaaksysteem::Auth::Twofactor;
use Moose;

with 'MooseX::Log::Log4perl';

use BTTW::Tools;

has [qw(c authdbic config session params)] => (
    is => 'rw',
);

has [qw/success cancel error login success_endpoint/] => (
    is => 'rw'
);

has 'logged_in_id' => (
    is => 'rw',
);

sub BUILD {
    my $self = shift;

    ### Check for session
    if (exists($self->session->{_twofactor})) {
        $self->_load_session;
    }
}

sub _load_session {
    my ($self) = @_;

    return unless exists($self->session->{_twofactor}{login});

    $self->success(1);
    $self->success_endpoint(
        $self->session->{_twofactor}{success_endpoint}
    );
    $self->login($self->session->{_twofactor}{login});
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
