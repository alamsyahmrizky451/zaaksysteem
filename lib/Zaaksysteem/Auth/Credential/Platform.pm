package Zaaksysteem::Auth::Credential::Platform;

use Moose;

=head1 NAME

Zaaksysteem::Auth::Credential::Platform - Authenticate platform 'users'

=head1 DESCRIPTION

This credential check implementation applies to users of the platform key,
and is exclusively used within the Zaaksysteem platform.

=head1 SYNOPSIS

    sub my_action {
        my ($self, $c) = @_;

        $c->authenticate({ key => 'my platform key' }, 'platform');
    }

=cut

use BTTW::Tools;

=head1 METHODS

=cut

around BUILDARGS => sub {
    my ($orig, $class, $config, $app, $realm) = @_;

    # We don't need any of the config here, we derive our context on
    # authentication via the supplied $realm parameter

    return $class->$orig;
};

=head2 authenticate

Implements interface for L<Catalyst::Plugin::Authentication>. Authenticates
the 'user' using the platform key supplied.

=cut

sub authenticate {
    my ($self, $c, $realm, $authinfo) = @_;

    my $config = assert_profile($realm->config, profile => {
        required => {
            interface => 'Str',
            username => 'Str'
        }
    })->valid;

    my $args = assert_profile($authinfo, profile => {
        required => {
            key => 'Str'
        },
        optional => {
            username => 'Str',
            interface => 'Str',
        }
    })->valid;

    unless ($args->{ key } eq $c->config->{ zs_platform_key }) {
        throw(
            'auth/platform/invalid_key',
            'Provided platform authentication key is invalid'
        );
    }

    my $interface = $c->model('DB::Interface')->find_by_module_name(
        $args->{ interface } || $config->{ interface }
    );

    unless (defined $interface) {
        throw('auth/platform/interface_not_found', sprintf(
            'Unable to find required interface by module name "%s"',
            $config->{ interface }
        ));
    }

    my $user = $realm->find_user($c, {
        username => $args->{ username } || $config->{ username },
        source => $interface->id
    });

    unless (defined $user) {
        throw('auth/platform/user_not_found', sprintf(
            'Configured platform user could not be found by name "%s"',
            $config->{ username }
        ));
    }

    $user->is_external_api(1);

    # BANISH TO THE DEPTHS OF GIT HISTORY
    # This workaround is in place while we design a proper auth codepath for
    # all cases.
    $c->stash->{ platform_access } = 1;

    return $user;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
