package Zaaksysteem::BR::Subject::Iterator::Person;

use Moose::Role;

=head1 NAME

Zaaksysteem::BR::Subject::Iterator::Person - This is a iterator extension to cache addresses for person subjects

=head1 DESCRIPTION

This Iterator extension will provide the iterator with the logic to cache C<Adres> tables in search
results. This way, we can reduce the amount of queries substantionaly.

=head1 ATTRIBUTES

=head2 _cached_addresses

Private store of cached addresses

=cut

has _cached_addresses => (
    is      => 'rw',
    default => 1,
);

=head1 METHODS

=head2 _can_cache_addresses

Returns true when caching of addresses for this iterator is allowed/possible

=cut

sub _can_cache_addresses {
    my $self    = shift;

    if ($self->_cache_is_setup && $self->rs->result_source->name eq 'natuurlijk_persoon') {
        return 1;
    }

    return
}

=head1 AROUND METHODS

=head2 _cache_related_rows

Caches the related rows, C<Adres> in this case, into the iterator

=cut


around '_cache_related_rows' => sub {
    my $method  = shift;
    my $self    = shift;

    my $ok      = $self->$method(@_);

    return $ok if (!$self->_can_cache_addresses || $self->_in_cache_loop->{person});

    my $np_query    = $self->rs->search()->get_column('natuurlijk_persoon.id')->as_query;

    ### Get all addresses
    my @addresses   = $self->rs->result_source->schema->resultset('Adres')->search({
        'natuurlijk_persoon_id'     => { in => $np_query },
        'deleted_on'                => undef,
    })->all;

    my %cached_adr;
    for my $address (@addresses) {
        my $np_id               = $address->get_column('natuurlijk_persoon_id');
        $cached_adr{ $np_id }   ||= [];

        push(@{ $cached_adr{ $np_id } }, $address);
    }

    $self->_in_cache_loop->{person} = 1;
    $self->_cached_addresses(\%cached_adr);

    return $ok;
};

=head2 _apply_cache_on_row

Applies the cached data, from C<Adres> in this case, into the returned row

=cut

around '_apply_cache_on_row' => sub {
    my $method  = shift;
    my $self    = shift;
    my ($row)   = @_;

    my $ok      = $self->$method(@_);

    ### Only when subject_type = person
    return $ok if (!$self->_can_cache_addresses);

    my $np_id       = $row->get_column('id');

    my @addresses;
    push(@addresses, $row->adres_id) if $row->adres_id;
    push(@addresses, @{ $self->_cached_addresses->{$np_id} }) if $self->_cached_addresses->{$np_id};

    $row->cached_addresses(
        \@addresses
    );

    return 1;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

