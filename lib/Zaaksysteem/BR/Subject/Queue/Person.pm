package Zaaksysteem::BR::Subject::Queue::Person;

use Moose::Role;

=head1 NAME

Zaaksysteem::BR::Subject::Queue::Person - Triggers and queue items related to persons

=head1 DESCRIPTION

Applies logic for zaaksysteem to send queue items for various person changes.

=head1 METHODS


=head2 run_post_triggers

    $entity->run_post_triggers(
        $row,           # Databaserow
        \%values        # Optional: list of values in update or create
    );

Sends the approriate queue items for checking related items for this subject. For now, it only sends
a queue item when "datum_overlijden" is set. To make sure we mark cases "as deceased".

=cut

sub run_post_triggers {
    my $self        = shift;
    my $row         = shift;

    my $qrs = $row->result_source->schema->resultset('Queue');

    if ($row->datum_overlijden) {
        my $item = $qrs->create_item(
            'update_cases_of_deceased',
            {
                label    => 'Update cases of deceased persons',
                metadata => {
                    target      => 'backend',
                    disable_acl => 1,
                },
                data => { natuurlijk_persoon_id => $row->id, }
            }
        );

        $qrs->queue_item($item);
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
