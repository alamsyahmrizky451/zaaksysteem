package Zaaksysteem::Zorginstituut::Roles::Provider;
use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Zorginstituut::Provider - A provider role definition kinda thing

=head1 DESCRIPTION

Defines what a provider must do/implement for it to function as intended.

=head1 SYNOPSIS

    package Foo;
    with 'Zaaksysteem::Zorginstituut::Roles::Provider';

    sub shortname { 'foo' }
    sub label     { 'foo' }
    sub shortname { 'foo' }

    sub configuration_items {
        # list of Zaaksysteem::ZAPI::Form::Field objects
        return @configuration_items;
    }

=cut

use BTTW::Tools qw(sig);
use Zaaksysteem::ZAPI::Form::Field;

=head1 ATTRIBUTES

=head2 di01_endpoint

Endpoint for di01 messages

=cut

has di01_endpoint => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head1 METHODS

Consumers of this role must implement the following methods

=over

=item shortname

Returns a internal name for the given provider module

=item label

Returns a human readable label for the given provider module

=item configuration_items

Returns a list of configuration items (L<Zaaksysteem::ZAPI::Form::Field>
instances) that are necessary to configure the appointment interface.

=back

=cut

requires qw(
    shortname
    label
    configuration_items
);

sig shortname => '=> Str';

sig label => '=> Str';

sig configuration_items => '=> @Zaaksysteem::ZAPI::Form::Field';

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
use Moose;
