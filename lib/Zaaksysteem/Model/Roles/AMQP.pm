package Zaaksysteem::Model::Roles::AMQP;
use Moose::Role;
use namespace::autoclean;

use Net::AMQP::RabbitMQ;
use Zaaksysteem::StatsD;

=head1 NAME

Zaaksysteem::Model::Roles::AMQP - Role for sharing a single AMQP connection
between multiple models.

=head1 SYNOPSIS

    package My::Model::Something;
    use Moose;
    with 'Zaaksysteem::Model::Roles::AMQP';

    sub x {
        my $mq = get_mq_connection($c);
    }

=cut

=head1 METHODS

=head2 get_mq_connection

Create a new connection to the configured AMQP server, or return the existing one
if a connection has been made previously.

Returns a L<Net::AMQP::RabbitMQ> instance.

=cut

my $mq_connection;

sub get_mq_connection {
    my $c = shift;

    if (defined $mq_connection && $mq_connection->is_connected()) {
        return $mq_connection;
    }

    $mq_connection = Net::AMQP::RabbitMQ->new();

    my $channel = $c->config->{'Model::Queue'}{channel} // 1;

    my $rabbitmq_host = $c->config->{'Model::Queue'}{host};
    my $rabbitmq_opts = $c->config->{'Model::Queue'}{options} // {};

    if ($ENV{ ZS_SILO_NAME }) {
        $rabbitmq_opts->{ vhost } = $ENV{ ZS_SILO_NAME };
    }

    $mq_connection->connect($rabbitmq_host, $rabbitmq_opts);
    $mq_connection->channel_open($channel);

    return $mq_connection;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
