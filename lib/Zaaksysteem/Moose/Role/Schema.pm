package Zaaksysteem::Moose::Role::Schema;
use Moose::Role;

=head1 NAME

Zaaksysteem::Abstract::Base - An abstract base class for Zaaksysteem modules

=head1 DESCRIPTION

The abstract base class implements various things we use in most Zaaksysteem
modules and reduces boilerplate.

=head1 SYNOPSIS

    package Foo;
    use Zaaksysteem::Moose;

    with 'Zaaksysteem::Moose::Role::Schema';

    # you now have
    has schema => (
        is       => 'ro',
        isa      => 'Zaaksysteem::Schema',
        required => 1,
    );

    # You can build a resultset
    has some_rs => (
        is => 'ro',
        isa => 'SomeRs',
        lazy => 1,
        builder => '_build_some_rs'
    );

    sub _build_some_rs {
        my $self = shift;

        # A standard RS
        return $self->build_resultset('Foo');

        # add default search params
        return $self->build_resultset(
            'Foo',
            { search   => 'foo' },
            { prefetch => 'bar' },
        );
    }

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

sub build_resultset {
    my ($self, $name, @rest) = @_;
    my $rs = $self->schema->resultset($name);
    return $rs->search_rs(@rest) if @rest;
    return $rs;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
