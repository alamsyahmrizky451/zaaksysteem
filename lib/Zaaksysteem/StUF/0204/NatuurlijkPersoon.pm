package Zaaksysteem::StUF::0204::NatuurlijkPersoon;

use Moose::Role;
use BTTW::Tools;
use JSON::Path;

use Zaaksysteem::Profiles qw/PROFILE_NATUURLIJK_PERSOON/;

use Zaaksysteem::StUF::0204::Element;

with qw/
    Zaaksysteem::StUF::GeneralInstance
    Zaaksysteem::StUF::0204::General
/;

=head1 NAME

Zaaksysteem::StUF::0301::NatuurlijkPersoon - Handling of Natuurlijk Personen below the stuf 0204 definition

=head1 SYNOPSIS


=head1 DESCRIPTION

StUF 0204 related calls for handling of Natuurlijk Personen, an improvement of Zaaksysteem::StUF.

Tested in: TestFor::General::StUF::0204::NatuurlijkPersoon

=head1 ATTRIBUTES

See L<Zaaksysteem::XML::Compile::Instance> for a description of Required attributes

=head1 CONSTANTS

=cut

=head2 NATUURLIJK_PERSOON_DEFINITION

Contains the element-instruction for the PRS StUF entry

=cut

use constant NATUURLIJK_PERSOON_DEFINITION => [
    {
        path    => '$.a-nummer',
        name    => 'a_nummer',
        partner => 'partner_a_nummer',
    },
    {
        path    => '$.bsn-nummer',
        name    => 'burgerservicenummer',
        partner => 'partner_burgerservicenummer',
    },
    {
        path    => '$.voornamen',
        name    => 'voornamen',
    },
    {
        path    => '$.voorletters',
        name    => 'voorletters',
    },
    {
        path    => '$.voorvoegselGeslachtsnaam',
        name    => 'voorvoegsel',
        partner => 'partner_voorvoegsel',
    },
    {
        path    => '$.geslachtsnaam',
        name    => 'geslachtsnaam',
        partner => 'partner_geslachtsnaam',
    },
    {
        path    => '$.geboortedatum',
        name    => 'geboortedatum',
    },
    {
        path    => '$.datumOverlijden',
        name    => 'datum_overlijden',
    },
    {
        path    => '$.indicatieGeheim',
        name    => 'indicatie_geheim',
    },
    {
        path    => '$.burgerlijkeStaat',
        name    => 'burgerlijke_staat',
    },
    {
        path    => '$.aanduidingNaamgebruik',
        name    => 'aanduiding_naamgebruik',
    },
    {
        path    => '$.geslachtsaanduiding',
        name    => 'geslachtsaanduiding',
    },
    {
        path    => '$.indicatieGeheim',
        name    => 'indicatie_geheim',
    },
    {
        path    => '$.codeGemeenteVanInschrijving',
        name    => 'gemeente_code_van_inschrijving',
    },
];

=head2 NATUURLIJK_PERSOON_ADR_DEFINITION

Contains the element-instruction for the PRS StUF, ADR entry

=cut

use constant NATUURLIJK_PERSOON_ADR_DEFINITION => [
    {
        path    => '$.postcode',
        name    => 'postcode',
    },
    {
        path    => '$.woonplaatsnaam',
        name    => 'woonplaats',
    },
    {
        path    => '$.straatnaam',
        name    => 'straatnaam',
    },
    {
        path    => '$.huisnummer',
        name    => 'huisnummer',
    },
    {
        path    => '$.huisletter',
        name    => 'huisletter',
    },
    {
        path    => '$.huisnummertoevoeging',
        name    => 'huisnummertoevoeging',
    },
    {
        path    => '$.gemeentecode',
        name    => 'gemeente_code',
    },
    {
        path    => '$.landcode',
        name    => 'landcode',
    },
];


=head1 METHODS

Inherits from L<Zaaksysteem::StUF::GeneralInstance> and L<Zaaksysteem::StUF::0204::General>, see
their manpage for more methods and attributes

    Zaaksysteem::StUF::0204::NatuurlijkPersoon
        with
            Zaaksysteem::StUF::GeneralInstance
            Zaaksysteem::StUF::0204::General

=head2 get_natuurlijkpersoon

Arguments: $STRING_XML

Return value: $NP_HASH

=begin perl

    $stuf0204->get_natuurlijkpersoon()

    ### Returns:
    VAR1 = {
        mutation_type   => 'update|create|delete',
        old             => {


        },
        new             => {

        }
    }

=end perl

TODO: Partner + Address information

=cut

define_profile 'get_natuurlijkpersoon' => (
    required    => [qw/options/],
    optional    => [qw/data multiple/],
);

sub get_natuurlijkpersoon {
    my $self                = shift;
    my $options             = assert_profile($_[0] || {})->valid;

    my $perldata;
    if ($options->{data}) {
        $perldata           = $options->{data}
    } else {
        my ($element, $toomany) = $self->retrieve_xml_element(
            {
                %{ $_[0] },
                method     => 'kennisgevingsbericht'
            }
        );

        throw(
            'stuf/0204/natuurlijk_persoon/batch_import',
            'Multiple elements returned, probably batch import: cannot continue'
        ) if $toomany;

        $perldata           = $self->kennisgevingsbericht('READER', $element);
    }


    throw(
        'stuf/0204/natuurlijk_persoon',
        'Incorrect StUF message, cannot find $stuf->{body}->{PRS}'
    ) unless ($perldata && $perldata->{stuurgegevens}->{entiteittype} eq 'PRS');

    ### In multiple mode, we do not have a "multiple"
    if ($options->{multiple}) {
        my @rv;

        ### 1) Check correct entity
        if ($perldata->{body} && $perldata->{body}->{PRS}) {
            for my $prs (@{ $perldata->{body}->{PRS} }) {
                push(@rv,
                    $self->_get_params_for_natuurlijkpersoon(
                        {
                            definition  => NATUURLIJK_PERSOON_DEFINITION,
                            hash        => $prs
                        },
                        $options->{options}
                    )
                );
            }
        }

        return \@rv;
    } else {
        my $mutation_type = $self->_get_mutation_type_from_stuurgegevens($perldata->{stuurgegevens});

        my ($new, $old);
        if (
            $mutation_type eq 'update'
        ) {
            $new     = $self->_get_params_for_natuurlijkpersoon(
                {
                    definition  => NATUURLIJK_PERSOON_DEFINITION,
                    hash        => $perldata->{body}->{PRS}->[1]
                },
                $options->{options}
            );

            $old     = $self->_get_params_for_natuurlijkpersoon(
                {
                    definition  => NATUURLIJK_PERSOON_DEFINITION,
                    hash        => $perldata->{body}->{PRS}->[0]
                },
                $options->{options}
            );
        } elsif (
            $mutation_type eq 'create' || $mutation_type eq 'delete'
        ) {
            $new = $self->_get_params_for_natuurlijkpersoon(
                {
                    definition  => NATUURLIJK_PERSOON_DEFINITION,
                    hash        => $perldata->{body}->{PRS}->[0]
                },
                $options->{options}
            );

            $old = {};
        }

        return {
            'mutation_type'     => $mutation_type,
            'old'               => $old,
            'new'               => $new
        };
    }
}

=head2 set_natuurlijkpersoon

Arguments: \%PARAMS, \%OPTIONS

Return value: $STRING_XML

    my $xml             = $self->xmlcompile->stuf0204->set_natuurlijkpersoon(
        {
            a_nummer                => '1234567890',
            burgerservicenummer     => '987654321',
            voornamen               => 'Tinus',
            voorletters             => 'TV',
            voorvoegsel             => 'vander',
            geslachtsnaam           => 'Testpersoon',
            geboortedatum           => '19830609',
            datum_overlijden        => '19830610',
            indicatie_geheim        => undef,
            burgerlijke_staat        => undef,
            aanduiding_naamgebruik  => 'P',
        },
        {
            sender                  => 'ZSNL',
            receiver                => 'CMODIS',
            reference               => 'ZS0000229154',
            datetime                => '2014030209011458',
            mutation_type           => 'create',
            follow_subscription     => 1,
        },
    );

Generates the XML for a "kennisgevingsbericht".

For B<Options>, see L<< Zaaksysteem::StUF::0204::General#generate_stuurgegevens >>

B<Params>

The following params can be used for generating a kennisgevingsbericht

=over 4

=item a_nummer

=item burgerservicenummer

=item voornamen

=item voorletters

=item voorvoegsel

=item geslachtsnaam

=item geboortedatum

=item datum_overlijden

=item indicatie_geheim

=item burgerlijke_staat

=item aanduiding_naamgebruik

=back

=cut

define_profile 'set_natuurlijkpersoon' => (
    required => [qw/
    /],
    optional    => [
        @{ PROFILE_NATUURLIJK_PERSOON()->{required} },
        @{ PROFILE_NATUURLIJK_PERSOON()->{optional} },
        qw/
            external_id
            subscription_id
        /
    ],
    require_some        => {
        'external_id_or_bsn'    => [1, qw/burgerservicenummer external_id/],
    },

    ### Below constraints are from official StUF XSD
    constraint_methods  => {
        %{ PROFILE_NATUURLIJK_PERSOON()->{constraint_methods} },
    }
);

sub set_natuurlijkpersoon {
    my $self                = shift;
    my $params              = assert_profile(shift || {})->valid;
    my ($options)           = @_;

    my $prs = $self->_load_body_prs($params);

    $prs    = $self->_load_identification($params, $prs, $options);

    my $perldata            = {
        'body'              => {
            'PRS'   => [ $prs ],
        },
        'stuurgegevens'     => $self->generate_stuurgegevens(
            {
                %{ $options },
                messagetype     => 'Lk01',
                mutation_type   => 'create',
                entitytype      => 'PRS',
            }
        ),
    };

    return $self->handle_message(
        'kennisgevingsbericht',
        $perldata,
        {
            %{ $options },
            translate   => 'WRITER',
        }
    );
}

=head2 search_natuurlijkpersoon

Arguments: \%PARAMS, \%OPTIONS

Return value: $STRING_XML

    my $xml             = $self->xmlcompile->stuf0204->search_natuurlijkpersoon(
        {
            a_nummer                => '1234567890',
            burgerservicenummer     => '987654321',
            voornamen               => 'Tinus',
            voorletters             => 'TV',
            voorvoegsel             => 'vander',
            geslachtsnaam           => 'Testpersoon',
            geboortedatum           => '19830609',
        },
        {
            sender                  => 'ZSNL',
            receiver                => 'CMODIS',
            reference               => 'ZS0000229154',
            datetime                => '2014030209011458',
            mutation_type           => 'create',
            follow_subscription     => 1,
        },
    );

Generates the XML for a "vraagBericht". This way we could place a search on a
StUF servicebus.

For B<Options>, see L<< Zaaksysteem::StUF::0204::General#generate_stuurgegevens >>

B<Params>

The following params can be used for generating a kennisgevingsbericht

=over 4

=item a_nummer

=item burgerservicenummer

=item voornamen

=item voorletters

=item voorvoegsel

=item geslachtsnaam

=item geboortedatum

=back

=cut

define_profile 'search_natuurlijkpersoon' => (
    required => [],
    optional    => [
        @{ PROFILE_NATUURLIJK_PERSOON()->{required} },
        @{ PROFILE_NATUURLIJK_PERSOON()->{optional} },
        'gemeente_code_van_inschrijving'
    ],
    # require_some => {
    #     'bsn_or_geslachtsnaam'  => [1, qw/burgerservicenummer geslachtsnaam/],
    # },

    ### Below constraints are from official StUF XSD
    constraint_methods  => PROFILE_NATUURLIJK_PERSOON()->{constraint_methods},
);

sub search_natuurlijkpersoon {
    my $self                = shift;
    my $params              = assert_profile(shift || {})->valid;
    my ($options)           = @_;

    my $prs                 = $self->_load_body_prs($params);

    $prs->{PRSADRVBL}       = $self->_load_body_adr($params);

    my $perldata            = {
        'body'              => {
            'PRS'   => [
                $prs,                               # Startquestion
                $prs,                               # Endquestion
                $self->_generate_search_question,   # Expected response
            ],
        },
        'stuurgegevens'     => $self->generate_stuurgegevens(
            {
                %{ $options },
                messagetype     => 'Lv01',
                entitytype      => 'PRS',
            }
        ),
    };

    ### Dispatching, so we get an answer. Process the answer
    if ($options->{dispatch}) {
        my ($rv, $trace) = $self->handle_message(
            'vraagbericht',
            $perldata,
            {
                %{ $options },
                translate   => 'WRITER',
            }
        );

        throw(
            'stuf/0204/natuurlijkpersoon/soap_error',
            'Error calling the SOAP dispatcher: ' . $trace->error
            .' / Response: ' . $trace->response->content,
            $trace
        ) unless $rv;

        unless ($rv->{synchroonAntwoord}) {
            throw(
                'stuf/0204/natuurlijkpersoon/invalid_response',
                'Error calling the SOAP dispatcher, invalid response: ' . $trace->error
                ." / Response: \n" . $trace->response->content,
                $trace
            );
        }

        return ($self->get_natuurlijkpersoon(
            {
                multiple    => 1,
                data        => $rv->{synchroonAntwoord},
                options     => $options
            }
        ), $trace);
    ### Else: just show us the XML
    } else {
        return $self->handle_message(
            'vraagbericht',
            $perldata,
            {
                %{ $options },
                translate   => 'WRITER',
            }
        );
    }

    #$self->vraagbericht('WRITER', $perldata);
}

=head1 PRIVATE METHODS

=head2 _generate_search_question

Arguments: none

    my $rv = $self->_generate_search_question

    print $rv;

Generates a C<PRS> block containing all elements we would like to receive from the opponent
partner. It is StUF for telling them which elements to send back in the response.

=cut

sub _generate_search_question {
    my $self            = shift;

    my $prs             = $self->_load_body_prs({}, {as_nil => 1});

    $prs->{PRSADRVBL}   = $self->_load_body_adr({}, {as_nil => 1});
    $prs->{PRSADRCOR}   = $self->_load_body_adr({}, {as_nil => 1});

    $prs->{PRSPRSHUW}   = {
        soortEntiteit => 'R',
        datumSluiting => Zaaksysteem::StUF::0204::Element->new(
            novalue => 'geenWaarde',
            name    => 'BG:datumSluiting',
        ),
        datumOntbinding => Zaaksysteem::StUF::0204::Element->new(
            novalue => 'geenWaarde',
            name    => 'BG:datumOntbinding',
        ),
        PRS => $self->_load_body_prs(
            {},
            {
                as_nil  => 1,
                partner => 1
            }
        )
    };

    # $prs->{PRSPRSHUW}   = $self->_load_body_prs({}, {partner => 1});

    return $prs;
}

=head2 _load_identification

Arguments: \%params, %prs, \%options

    $self->_load_identification(\%params, \%prs, \%options)


Loads the identification credentials in the StUF message, which depends on
the key_type in C<%options>.

Depending on the key_type, either the C<sleutelGegevensbeheer> field or the
C<sleutelOntvangend> will be filled with the key of the other party.

C<sleutelVerzendend> will be filled with our local subscription ID.

B<Valid %params>

=over 4

=item subscription_id [required]

The ID of the L<Zaaksysteem::DB::ObjectSubscription> table containing our
subscription

=item external_id [required]

The external ID of the other party, pointing to our local record.

=back

B<Valid %options>

=over 4

=item key_type [required]

Defines the type of key we use for this message.

B<gegevens>

Sets the C<sleutelGegevensbeheer> value

B<ontvangend>

Sets the C<sleutelOntvangend> value

=back

=cut

define_profile '_load_identification' => (
    required            => [qw/key_type subscription_id/],
    optional            => [qw/
        external_id
    /],
    constraint_methods  => {
        key_type        => qr/^(?:gegevens|ontvangend)$/,
        external_id     => qr/^\w+$/,
        subscription_id => qr/^\d+$/,
    },
    dependencies        => {
        require_some    => [1, qw/external_id via_gbav/],
    }
);

sub _load_identification {
    my $self            = shift;
    my $params          = shift;
    my $prs             = shift;
    my $options         = shift || {};

    assert_profile(
        {
            %$params,
            key_type    => $options->{key_type},
        }
    );

    my $keytype;
    if ($options->{key_type} eq 'gegevens') {
        $keytype = 'sleutelGegevensbeheer';
    } elsif ($options->{key_type} eq 'ontvangend') {
        $keytype = 'sleutelOntvangend';
    }

    if ($params->{external_id}) {
        $prs->{$keytype}   = $params->{external_id};
    }

    if ($params->{subscription_id}) {
        $prs->{sleutelVerzendend}   = $params->{subscription_id};
    }

    return $prs;
}


=head2 _get_params_for_natuurlijkpersoon

Arguments: \%params

Return value: \%RESULT_HASH

    $self->_get_params_for_natuurlijkpersoon(
        {
            definition  => NATUURLIJK_PERSOON_DEFINITION,
            hash        => $perldata->{body}->{PRS}->[0]
        }
    );

Given a C<< $STUF->{PRS} >> hash, collects all the necessary data in a structure
zaaksysteem.nl understands.

=cut

sub _get_params_for_natuurlijkpersoon {
    my $self                = shift;
    my ($params, $options)  = @_;

    my $np                  = $self->get_params_by_definition(@_);

    $np                     = $self->_get_params_for_natuurlijkpersoon_address($np, @_);

    $np                     = $self->_get_params_for_partner($np, @_);

    $np                     = $self->_get_params_for_identification($np, @_);

    $np                     = $self->_get_params_for_scenarios($np, @_);

    return $np;
}


=head2 _get_params_for_scenarios

Arguments: \%RESULT_HASH, \%STUF_HASH

Return value: \%RESULT_HASH

Will make sure parameters are set for different scenario's, think of:

    briefadres
    inonderzoek
    geheim

=cut

sub _get_params_for_scenarios {
    my $self                = shift;
    my ($np, $params)       = @_;

    my $hash                = $params->{hash};

    # warn(Data::Dumper::Dumper($hash));

    eval {
        local $JSON::Path::Safe = 0;

        my $rawonderzoek = JSON::Path->new(
            '$.extraElementen.seq_extraElement[*][?($_->{naam} eq "aanduidingGegevensInOnderzoek")]._'
        )->value($params->{hash});

        $np->{onderzoek_persoon} = 1 if $rawonderzoek;

        $rawonderzoek = JSON::Path->new(
            '$.extraElementen.seq_extraElement[*][?($_->{naam} eq "aanduidingGegevensInOnderzoekOverlijden")]._'
        )->value($params->{hash});

        $np->{onderzoek_overlijden} = 1 if $rawonderzoek;

        my $address;
        if ($params->{hash}->{PRSADRVBL}) {
            $address = ( ref($params->{hash}->{PRSADRVBL}) eq 'ARRAY'
                ? $params->{hash}->{PRSADRVBL}->[0]->{ADR}
                : $params->{hash}->{PRSADRVBL}->{ADR}
            );
        }

        if (!$address && $params->{hash}->{PRSADRCOR}) {
            $address = (ref($params->{hash}->{PRSADRCOR}) eq 'ARRAY'
                ? $params->{hash}->{PRSADRCOR}->[0]->{ADR}
                : $params->{hash}->{PRSADRCOR}->{ADR}
            );
        }

        if ($address) {
            $rawonderzoek = JSON::Path->new(
                '$.extraElementen.seq_extraElement[*][?($_->{naam} eq "aanduidingGegevensInOnderzoek")]._'
            )->value($address);

            $np->{onderzoek_verblijfplaats} = 1 if $rawonderzoek;
        }

        my $huwelijk;
        if ($params->{hash}->{PRSPRSHUW}) {
            $huwelijk = $params->{hash}->{PRSPRSHUW}->[0];
        }

        if ($huwelijk) {
            $rawonderzoek = JSON::Path->new(
                '$.extraElementen.seq_extraElement[*][?($_->{naam} eq "aanduidingGegevensInOnderzoek")]._'
            )->value($huwelijk);

            $np->{onderzoek_huwelijk} = 1 if $rawonderzoek;
        }
    };

    if ($@) {
        warn('Error in onderzoek bepaling: ' . $@);
    }

    return $np;
}

=head2 _get_params_for_identification

Arguments: \%RESULT_HASH, \%STUF_HASH

Return value: \%RESULT_HASH

    my $rv = $self->_get_params_for_identification;

    print Dumper($rv);

    # {
    #     subscription_id     => 234234,
    #     external_id         => S8923023,
    # }

Returns the identification information from the StUF message, retrieved from
the key of C<key_type>.

=cut

define_profile '_get_params_for_identification' => (
    required            => [qw/key_type/],
    constraint_methods  => {
        key_type        => qr/^(?:gegevens|ontvangend)$/,
    }
);

sub _get_params_for_identification {
    my $self                        = shift;
    my ($np, $params, $options)     = @_;

    $options                        ||= {};

    my $hash                        = $params->{hash};

    assert_profile($options);

    my $remote_key;
    if ($options->{key_type} eq 'gegevens') {
        $remote_key = 'sleutelGegevensbeheer';
    } elsif ($options->{key_type} eq 'ontvangend') {
        $remote_key = 'sleutelVerzendend';
    }

    $np->{external_id}          = $hash->{$remote_key};
    $np->{subscription_id}      = $hash->{sleutelOntvangend};

    return $np;
}

=head2 _get_params_for_natuurlijkpersoon_address

Arguments: \%RESULT_HASH, \%STUF_HASH

Return value: \%RESULT_HASH

=cut

sub _get_params_for_natuurlijkpersoon_address {
    my $self                = shift;
    my ($np, $params)       = @_;

    my $hash                = $params->{hash};

    ### Get address data
    return $np unless (
        exists($hash->{PRSADRVBL}) ||
        exists($hash->{PRSADRCOR})
    );

    my $address;

    if (exists($hash->{PRSADRVBL})) {
        my $error;

        my @prsadrvbl;
        if (ref $hash->{PRSADRVBL} eq 'ARRAY') {
            @prsadrvbl = @{ $hash->{PRSADRVBL} };
        } else {
            @prsadrvbl = ($hash->{PRSADRVBL});
        }

        ($address, $error) = grep (
            { $self->_is_active_period($_->{ADR}) }
            @prsadrvbl
        );

        if ($error) {
            throw(
                'stuf/prs/get_params_for_natuurlijk_persoon_adres/multiple',
                'Got multiple address..that cannot be right'
            );
        }

        $np->{functie_adres} = 'W';

        $address = $address->{ADR};
    }

    if (!$address && exists($hash->{PRSADRCOR})) {
        my $error;

        my @prsadrcor;
        if (ref $hash->{PRSADRCOR} eq 'ARRAY') {
            @prsadrcor = @{ $hash->{PRSADRCOR} };
        } else {
            @prsadrcor = ($hash->{PRSADRCOR});
        }

        ($address, $error) = grep (
            { $self->_is_active_period($_->{ADR}) }
            @prsadrcor
        );

        if ($error) {
            throw(
                'stuf/prs/get_params_for_natuurlijk_persoon_adres/multiple',
                'Got multiple address..that cannot be right'
            );
        }

        $np->{functie_adres} = 'B';

        $address = $address->{ADR};
    }

    for my $entry (@{ NATUURLIJK_PERSOON_ADR_DEFINITION() }) {
        my $stufname = $entry->{path};
        $stufname =~ s/\$\.//;

        next unless exists($address->{$stufname});

        $np->{$entry->{name}}   = $self->plaintext_value($address->{$stufname});
    }

    return $np;
}

=head2 _get_params_for_partner

Arguments: \%NP_ENTRY, \%STUF_PARAMS

Return value: \%NP_ENTRY

    $self->_get_params_for_partner($np_entry, $stuf_params);

Will extract the partner data from the stuf params, and sets it into C<$np_entry>

=cut

sub _get_params_for_partner {
    my $self                = shift;
    my ($np, $params)       = @_;

    my $hash                = $params->{hash};

    my ($partner_voorvoegsel, $partner_geslachtsnaam);
    eval {
        local $JSON::Path::Safe = 0;

        my $partner_geslachtsnaam = JSON::Path->new(
            '$.extraElementen.seq_extraElement[*][?($_->{naam} eq "geslachtsnaamEchtgenoot")]._'
        )->value($hash);

        my $partner_voorvoegsel = JSON::Path->new(
            '$.extraElementen.seq_extraElement[*][?($_->{naam} eq "voorvoegselsGeslachtsnaamEchtgenoot")]._'
        )->value($hash);

        $np->{partner_geslachtsnaam}    = $partner_geslachtsnaam if $partner_geslachtsnaam;
        $np->{partner_voorvoegsel}      = $partner_voorvoegsel if $partner_voorvoegsel;
    };

    ### Get address data
    return $np if ($np->{partner_geslachtsnaam} || !exists($hash->{PRSPRSHUW}));

    my @raw_partners;
    if (ref $hash->{PRSPRSHUW} eq 'ARRAY') {
        @raw_partners = @{ $hash->{PRSPRSHUW} };
    } else {
        @raw_partners = ($hash->{PRSPRSHUW});
    }

    my $partner = $self->_get_active_partner(\@raw_partners);

    for my $entry (@{ NATUURLIJK_PERSOON_DEFINITION() }) {
        next unless $entry->{partner};

        my $stufname = $entry->{path};
        $stufname =~ s/\$\.//;

        next unless exists($partner->{PRS}->{$stufname});

        $np->{$entry->{partner}}   = $self->plaintext_value($partner->{PRS}->{$stufname});
    }

    return $np;
}

=head2 _load_body_prs

Arguments: \%NP_ENTRY, \%OPTIONS

Return value: \%STUF_PARAMS

    $self->_load_body_prs($np_entry, $stuf_params);

The magic of this module, reads the constant C<NATUURLIJK_PERSOON_DEFINITION>, and makes sure
every element of StUF gets filled with the data from the given \%NP_ENTRY.

Returns a hash containing StUF XML Elements.

B<Options>

=over 4

=item partner

Loads only the partner data

=item as_nil

Returns the element as a nil value, use this for creating an empty list of elements.

=back

=cut

sub _load_body_prs {
    my $self            = shift;
    my $params          = shift;
    my $options         = shift || {};

    my $prs             = {
        soortEntiteit => 'F',
    };
    for my $entry (@{ NATUURLIJK_PERSOON_DEFINITION() }) {
        if ($options->{partner}) {
            next unless $entry->{partner};
        }

        my $stufname = $entry->{path};
        $stufname =~ s/\$\.//;

        if ($options->{as_nil}) {
            $prs->{$stufname}   = Zaaksysteem::StUF::0204::Element->new(
                novalue => 'geenWaarde',
                name    => 'BG:' . $stufname,
            );

            next;
        }

        next unless exists($params->{$entry->{name}});

        $prs->{$stufname}   = $params->{$entry->{name}};
    }

    return $prs;

};

=head2 _load_body_adr

Arguments: \%NP_ENTRY, \%OPTIONS

Return value: \%STUF_PARAMS

    $self->_load_body_adr($np_entry, $stuf_params);

The magic of this module, reads the constant C<NATUURLIJK_PERSOON_ADR_DEFINITION>, and makes sure
every element of StUF gets filled with the data from the given \%NP_ENTRY.

Returns a hash containing StUF XML Elements.

B<Options>

=over 4

=item as_nil

Returns the element as a nil value, use this for creating an empty list of elements.

=back

=cut


sub _load_body_adr {
    my $self            = shift;
    my $params          = shift;
    my $options         = shift || {};

    my $ADR = {
        soortEntiteit => 'F',
    };
    for my $entry (@{ NATUURLIJK_PERSOON_ADR_DEFINITION() }) {
        my $stufname = $entry->{path};
        $stufname =~ s/\$\.//;

        if ($options->{as_nil}) {
            $ADR->{$stufname}   = Zaaksysteem::StUF::0204::Element->new(
                novalue => 'geenWaarde',
                name    => 'BG:' . $stufname,
            );

            next;
        }

        next unless exists($params->{$entry->{name}});

        $ADR->{$stufname}   = $params->{$entry->{name}};
    }

    return { soortEntiteit => 'R', => 'ADR' => $ADR };
};

=head2 _get_active_partner

Arguments: \%partners

    $self->_get_active_partner(\%params)

Retrieves the active partner from the C<PRSPRSHUW> element of StUF. Reads the
datumSluiting and datumOntbinding by finding the active one.

=cut

sub _get_active_partner {
    my $self                = shift;
    my $partners            = shift;

    my $latest;

    my ($oldstartdate, $oldenddate);
    for my $partner (@{ $partners }) {
        # warn('Partner: ' . Data::Dumper::Dumper($partner));
        if ($latest) {
            $oldstartdate   = $self->plaintext_value($latest->{datumSluiting});
            $oldenddate     = $self->plaintext_value($latest->{datumOntbinding});
        } else {
            $latest = $partner;
            next;
        }

        my $startdate       = $self->plaintext_value($partner->{datumSluiting});
        my $enddate         = $self->plaintext_value($partner->{datumOntbinding});

        if (
            !$enddate ||
            ($startdate && !$oldstartdate) ||
            ($startdate && $oldstartdate && $startdate > $oldstartdate)
        ) {
            $latest = $partner;
        }
    }

    return $latest;
}

=head2 _is_active_period

Arguments: \%STUF_HASH

Return value: 1||0

    my $raw_period      = {
      einddatum => {
        _ => 'NIL',
        exact => 1,
        indOnvolledigeDatum => 'V',
        noValue => 'geenWaarde'
      },
      ingangsdatum => {
        _ => bless( {
          _e => [
            0
          ],
          _es => '+',
          _m => [
            19511115
          ],
          sign => '+'
        }, 'Math::BigFloat' ),
        exact => 1,
        indOnvolledigeDatum => 'V'
      },
    };

    $active = $self->_is_active_period($raw_period);

Returns true when the period defined in the hash by keys C<einddatum> and C<ingangsdatum> are
true for the current time

=cut

sub _is_active_period {
    my $self            = shift;
    my $hash            = shift;

    my $startdate       = $self->plaintext_value($hash->{ingangsdatum});
    my $enddate         = $self->plaintext_value($hash->{einddatum});

    my $currentdate     = DateTime->now->strftime('%Y%m%d');

    ### No period at all given: means: no history
    return 1 if (!$startdate && !$enddate);

    if ($startdate && $startdate =~ /^\d+$/ && $startdate <= $currentdate) {
        if (!$enddate || $enddate !~ /^\d+$/ || $enddate > $currentdate) {
            return 1;
        }
    }

    return;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
