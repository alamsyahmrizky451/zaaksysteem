package Zaaksysteem::Types::Filestore;
use Moose;
use Moose::Util::TypeConstraints;


### Type constraints implemented, to make sure we can use the types when we use this class

subtype 'ArrayRefFilestore'
    => as 'ArrayRef[Zaaksysteem::Types::Filestore]';

coerce 'ArrayRefFilestore'
    => from 'ArrayRef'
        => via { return [map { Zaaksysteem::Types::Filestore->new(%{ $_ })} @{ $_ } ]; };

use constant ATTRIBUTES => [qw/uuid thumbnail_uuid filename size mimetype md5 original_name is_archivable/];

### To use in another module, e.g. Zaaksysteem::Zaken::Roles::ZTT...
our $ATTRIBUTES = ATTRIBUTES();


=head1 NAME

Zaaksysteem::Types::Filestore - Implements a filestore entry for ObjectAttributes

=head1 DESCRIPTION

These are objects containing every needed information from the Filestore JSON

=head1 SYNOPSIS

    ### Somewhere in class extending from L<Zaaksysteem::Object>
    package Test::OA::Filestore

    use Moose;

    has profile_picture => (
        is     => 'rw',
        isa    => 'ArrayRefFilestore',
        traits => [qw[OA]],
        label  => "A profile picture of hasselhoff",
        type   => 'file',
        coerce => 1
    );

    ### Somewhere else

    my $filestore_type  = {
        uuid            => $filestore->uuid,
        thumbnail_uuid  => $filestore->thumbnail_uuid,
        filename        => $filestore->original_name,
        size            => $filestore->size,
        mimetype        => $filestore->mimetype,
        md5             => $filestore->md5,
    };

    my $obj             = Test::OA::Filestore->new(
        profile_picture => [$filestore_type],
    );



    print $obj->profile_picture->[0]->filename

=head1 ATTRIBUTES

=head2 uuid
=head2 thumbnail_uuid
=head2 filename
=head2 size
=head2 mimetype
=head2 md5
=head2 is_archivable

=cut

has ATTRIBUTES() => (
    'is'        => 'rw',
);

=head2 TO_JSON

    [
      {
        'filename' => 'FilestoreTest.txt',
        'md5' => 'b771ff6f06666e4eb88aab9f2131a68f',
        'mimetype' => 'text/plain',
        'size' => 2387,
        'thumbnail_uuid' => undef,
        'uuid' => '895f680c-b0f9-4cd0-b37f-45b72cf86cd1',
        'is_archivable' => true,
      }
    ]

Transforms this object into a JSON hash:

=cut

sub TO_JSON {
    my $self = shift;

    return {
        uuid            => $self->uuid,
        thumbnail_uuid  => $self->thumbnail_uuid,
        filename        => $self->filename,
        original_name   => $self->original_name,
        size            => $self->size,
        mimetype        => $self->mimetype,
        md5             => $self->md5,
        is_archivable     => $self->is_archivable ? \1 : \0,
    };
}

# Because namespace::autoclean removes overloads.
no Moose;

__PACKAGE__->meta->make_immutable();

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
