package Zaaksysteem::Roles::ContactDetails;
use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Roles::ContactDetails - A role that updates open cases and their
contact details

=cut

=head2 update_contact_details_for_cases

Update contact details of open cases

=cut

sub update_contact_details_for_cases {
    my $self = shift;

    my $cases = $self->get_cases(
        type_zaken => [qw(new open stalled)],
        all_cases  => 1
    );

    while (my $case = $cases->next) {
        $case->ddd_update_system_attribute_group('requestor');
        $case->ddd_update_system_attribute_group('recipient');
        $case->touch();
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

