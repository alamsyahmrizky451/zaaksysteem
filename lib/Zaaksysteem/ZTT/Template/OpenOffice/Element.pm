package Zaaksysteem::ZTT::Template::OpenOffice::Element;

=head1 NAME

Zaaksysteem::ZTT::Template::OpenOffice::Element - Add OpenOffice Element methods

=cut

use Moose::Role;

use OpenOffice::OODoc::InsertDocument;
use OpenOffice::OODoc::HeadingStyles;
use Path::Tiny;

use BTTW::Tools;

use Zaaksysteem::ZTT::Converter;


=head1 METHODS

=head2 substitute_element_from_markup

Substitute an OODoc Element with rendered markup

Accepts the following named parameters:

=over

=item oodoc_element

An L<OpenOffice::OODoc::Element>

=item markup_text

The markup text, as plain text.

=item markup_type

The format of the C<markup_text>, defaults to C<html>. Many other formats could
be used, depending on how C<markup_to_oodoc> has been implemented for the
L<Zaaksysteem::ZTT::Converter> (See L<Zaaksysteem::ZTT::Converter::Pandoc> for
more info)

=item reference_file

An optional ODT reference file. When it is not provided, the current C<document>
is being used.

=back

=cut

define_profile substitute_element_from_markup => (
    required => {
        oodoc_element  => 'OpenOffice::OODoc::Element',
    },
    optional => {
        markup_text    => 'Str',
        markup_type    => 'Str',
        reference_file => 'Path::Tiny',
    },
    defaults => {
        markup_text    => '',
        markup_type    => 'html',
    },
);

sub substitute_element_from_markup {
    my $self   = shift;
    my $params = assert_profile( {@_} )->valid;

    my $oodoc_element_to_replace = $params->{oodoc_element};
    my $markup_text              = $params->{markup_text};
    my $markup_type              = $params->{markup_type};
    my $reference_file           = $params->{reference_file} || 
        $self->_add_styles_and_save_as_reference_file;

    my $converter = Zaaksysteem::ZTT::Converter->new;
    my $oodoc_from_document = $converter->from_markup_text_into_openoffice_oodoc(
        markup_text    => $markup_text,
        markup_type    => $markup_type,
        reference_file => $reference_file,
    );

    if (defined $oodoc_from_document) {
        $self->document->insertDocument(
            $oodoc_element_to_replace,
            'before',
            $oodoc_from_document,
        );
    } else {
        $self->log->error(sprintf(
            "Pandoc conversion failed to create a valid ODT. Markup text: '%s'",
            $markup_text // '<undef>'
        ));
    }

    # we're done with the element to replace
    $oodoc_element_to_replace->delete;

    return

}

sub _add_styles_and_save_as_reference_file {
    my $self = shift;

    # pandoc accepts a reference-odt file that will be used to generate
    # output conform the defined styles. If no reference-odt is given, it will
    # generate sensible defaults.
    # It will not define its own 'sensible defaults' when they are missing from
    # our reference-odt.
    # ... So let's add them beforehand, afterwards is too late and the styling
    # is already lost.
    $self->styles->establishHeadingStyle( $_ ) for 1 .. 6;

    # note, that if there is a referencefile provided, we expect it to be
    # a fully style-defined reference document, not a half baked thingy

    my $reference_file = Path::Tiny->tempfile;
    my $reference_path = $reference_file->canonpath;
    $self->document->save("$reference_path");

    return $reference_file
}
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
