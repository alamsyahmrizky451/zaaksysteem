package Zaaksysteem::ZTT::Constants;

use warnings;
use strict;
use utf8;

use Exporter qw[import];

use List::MoreUtils qw[any];
use Zaaksysteem::Constants qw();

our @EXPORT = qw[
    ZTT_DEFAULT_OPERATORS
    ZTT_DEFAULT_FUNCTIONS
    ZTT_DEFAULT_CONSTANTS
];

our @EXPORT_OK = qw[
    ZTT_DEFAULT_OPERATORS
    ZTT_DEFAULT_FUNCTIONS
    ZTT_DEFAULT_CONSTANTS
];

=head1 NAME

Zaaksysteem::ZTT::Constants - Constants for the ZTT infrastructure

=head1 CONSTANTS

=head2 ZTT_DEFAULT_OPERATORS

A collection of operators supported by ZTT.

=cut

use constant ZTT_DEFAULT_OPERATORS => {
    '+' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x + $y;
    },

    '-' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x - $y;
    },

    '*' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x * $y;
    },

    '/' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return undef if $y == 0;

        return $x / $y;
    },

    '==' => sub {
        my ($x, $y) = scalarify(shift, shift);

        return $x eq $y;
    },

    '!=' => sub {
        my ($x, $y) = scalarify(shift, shift);

        return $x ne $y;
    },

    '~=' => sub {
        my ($x, $y) = scalarify(shift, shift);

        return index(lc $x, lc $y) >= 0;
    },

    '>' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x > $y;
    },

    '<' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x < $y;
    },

    '>=' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x >= $y;
    },

    '<=' => sub {
        my $x = _scalarify(shift) || 0;
        my $y = _scalarify(shift) || 0;

        return $x <= $y;
    },

    'in' => sub {
        my $x = _scalarify(shift);
        my $y = shift;

        return undef unless ref $y eq 'ARRAY';
        return any { $_ eq $x } @{ $y };
    }
};

=head2 ZTT_DEFAULT_CONSTANTS

A named list of constants available in all ZTT contexts.

=cut

use constant ZTT_DEFAULT_CONSTANTS => {
    'pi' => 3.14159265359
};

=head2 ZTT_DEFAULT_FUNCTIONS

A named list of functions available in all ZTT contexts.

=cut

use constant ZTT_DEFAULT_FUNCTIONS => {
    constant => sub {
        my $name = shift;

        return undef unless defined $name;

        return ZTT_DEFAULT_CONSTANTS->{ lc $name };
    },

    array => sub {
        return [
            map { ref $_ eq 'ARRAY' ? @{ $_ } : $_ } @_
        ];
    },

    join => \&_ztt_join
};

=head1 HELPER FUNCTIONS

=head2 scalarify

Helper function that converts a ZTT value from any type to a scalar-like
type. Currently only converts array values to a string join of the elements.

=cut

sub scalarify {
    return map { _scalarify($_) } @_;
}

sub _scalarify {
    my $arg = shift;

    return '' unless defined $arg;
    return join(', ', scalarify(@{ $arg })) if ref $arg eq 'ARRAY';

    return "$arg";
}

=head2 _ztt_join

Implementation of the ZTT 'join()' function

Syntax:

    [[ join(magic_string, separator, last_separator) ]]

Where "magicstring" is a magic string, "separator" is a separator for all items
in the list, except the last, and "last_separator" is the last separator.

If no separators are specified, the empty string is used as a separator, concatenating
the values without anything between them.

If "last_separator" is not specified, it defaults to the value of "separator".

=cut

sub _ztt_join {
    my $value          = shift;
    my $separator      = shift // '';
    my $last_separator = shift // $separator;

    return ""            if not defined $value;
    return "$value"      if ref($value) ne 'ARRAY';
    return ""            if not @$value;
    return "$value->[0]" if @$value == 1;

    return join(
        $last_separator,
        join($separator, @$value[0 .. $#$value - 1]),
        $value->[-1],
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

