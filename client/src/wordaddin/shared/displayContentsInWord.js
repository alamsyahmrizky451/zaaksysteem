// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default (base64EncodedString) => {
	Word.run( (context) => { //eslint-disable-line

    let thisDocument = context.document;

    thisDocument.body.clear();

    let mySelection = thisDocument.getSelection();

    mySelection.insertFileFromBase64(base64EncodedString, 'replace');

    return context.sync().then(() => {});
  });
};
