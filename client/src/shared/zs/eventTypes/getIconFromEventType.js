// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import variables from '!raw-loader!!./../../styles/_variables.scss';
import parseSassVariables from '../../util/parseSassVariables';
import get from 'lodash/get';

export default (type) => {
  let icon,
    color,
    sassVariables = parseSassVariables(variables);

  switch (type) {
    case 'case/document/accept':
    case 'case/document/assign':
    case 'case/document/copy':
    case 'case/document/create':
    case 'case/document/label':
    case 'case/document/metadata/update':
    case 'case/document/publish':
    case 'case/document/reject':
    case 'case/document/remove':
    case 'case/document/rename':
    case 'case/document/replace':
    case 'case/document/restore':
    case 'case/document/revert':
    case 'case/document/trash':
    case 'case/document/unpublish':
      icon = 'file';
      color =
        type === 'case/document/reject' ||
        type === 'case/document/remove' ||
        type === 'case/document/trash'
          ? get(sassVariables, 'red')
          : get(sassVariables, 'green');
      break;

    case 'subject/contactmoment/create':
    case 'subject/contact_moment/create':
      icon = 'comment-processing';
      color = get(sassVariables, 'blue');
      break;

    case 'case/note/create':
    case 'contactmoment/note':
    case 'object/note':
    case 'subject/internalnote/create':
    case 'subject/note/create':
      icon = 'note-plus';
      color = get(sassVariables, 'yellow');
      break;

    case 'case/attribute/update':
    case 'case/update/piprequest':
    case 'case/update/field':
    case 'case/pip/updatefield':
      icon = 'pencil';
      color = get(sassVariables, 'orange');
      break;

    case 'case/relation/update':
    case 'case/accept':
      icon = 'account-check';
      color = get(sassVariables, 'blue');
      break;

    case 'case/subject/add':
      icon = 'account-plus';
      color = get(sassVariables, 'yellow');
      break;

    case 'case/update/relation':
    case 'object/relation/add':
    case 'object/relation/update':
    case 'object/relation/delete':
      icon = 'link';
      color = get(sassVariables, 'blue');
      break;

    case 'email/send':
      icon = 'email';
      color = get(sassVariables, 'light-blue');
      break;

    case 'case/create':
      icon = 'plus';
      color = get(sassVariables, 'blue');
      break;

    case 'case/close':
      icon = 'check';
      color = get(sassVariables, 'green');
      break;

    case 'case/reopen':
      icon = 'folder-upload';
      break;

    case 'case/update/milestone':
      icon = 'folder-move';
      break;

    case 'case/update/purge_date':
      icon = 'folder-remove';
      color = get(sassVariables, 'red');
      break;

    case 'case/subcase':
    case 'case/close_child':
      icon = 'folder-multiple';
      break;

    case 'case/checklist/item/update':
      icon = 'checkbox-marked';
      color = get(sassVariables, 'purple');
      break;

    case 'case/update/allocation':
      icon = 'account';
      color = get(sassVariables, 'purple');
      break;

    case 'case/object/update':
    case 'case/object/create':
    case 'object/update':
      icon = 'hexagon';
      color = get(sassVariables, 'light-blue');
      break;

    case 'case/object/delete':
    case 'object/queue/item_failed':
      icon = 'alert';
      color = get(sassVariables, 'red');
      break;

    case 'auth/alternative':
      icon = 'key-variant';
      color = get(sassVariables, 'purple');
      break;

    case 'case/update/case_type':
    case 'casetype/mutation':
      icon = 'format-list-bulleted-type';
      color = get(sassVariables, 'yellow');
      break;

    default:
      icon = 'hexagon';
      color = get(sassVariables, 'blue');
      break;
  }

  return {
    icon,
    color,
  };
};
