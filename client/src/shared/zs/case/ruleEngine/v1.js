// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import ruleEngine from '.';
import assign from 'lodash/fp/assign';
import mapKeys from 'lodash/mapKeys';
import mapValues from 'lodash/mapValues';
import omit from 'lodash/omit';
import propCheck from './../../../util/propCheck';

let convertName = (name) => name.replace(/^attribute\./, '');

export default (rules, properties, options = { debug: false }) => {
  let vals, result;

  propCheck.throw(
    propCheck.shape({
      rules: propCheck.array,
      properties: propCheck.shape({
        attributes: propCheck.object,
        channel_of_contact: propCheck.string,
        requestor: propCheck.shape({
          instance: propCheck.shape({
            subject_type: propCheck.string,
          }),
        }),
      }),
    }),
    {
      rules,
      properties,
    }
  );

  // Create an object consisting of the casetype attributes and the casetype properties
  vals = assign(
    mapKeys(properties.attributes, (value, key) => `attribute.${key}`),
    mapKeys(
      properties.casetypeProperties,
      (value, key) => `case.casetype.${key.replace('casetypeProperties', '')}`
    )
  );

  // Stick the case values such as channel of contact on.
  vals = assign(
    vals,
    mapKeys(
      omit(properties, 'attributes', 'requestor', 'casetypeProperties'),
      (value, key) => `case.${key}`
    )
  );

  vals = assign(vals, {
    'case.requestor.subject_type': {
      employee: 'medewerker',
      company: 'bedrijf',
      person: 'natuurlijk_persoon',
    }[properties.requestor.instance.subject_type],
    'case.requestor.preset_client': properties.requestor.instance.preset_client,
  });

  result = ruleEngine(rules, vals, options);

  return mapValues(result, (group, name) => {
    return name === 'pause_application'
      ? assign(group, {
          attribute_names: group.attribute_names.map(convertName),
        })
      : mapKeys(group, (value, key) => convertName(key));
  });
};
