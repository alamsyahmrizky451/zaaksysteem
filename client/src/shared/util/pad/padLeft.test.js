// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import padLeft from './padLeft';

function typeErrorFactory(callback) {
  const errors = [];
  const badTypes = [42, {}, [], () => {}, false, null, undefined];

  badTypes.forEach((value) => {
    try {
      callback(value);
    } catch (error) {
      if (error instanceof TypeError) {
        errors.push(error.message);
      }
    }
  });

  expect(errors.length).toBe(badTypes.length);

  badTypes
    .map((value) => typeof value)
    .forEach((type, index) => {
      expect(errors[index]).toBe(`expected ${type} to be a string`);
    });
}

/**
 * @test {padLeft}
 */
describe('The `padLeft` function', () => {
  test('throws a type error if the first argument is not a string', () => {
    typeErrorFactory((value) => padLeft(value, ''));
  });

  test('throws a type error if the second argument is not a string', () => {
    typeErrorFactory((value) => padLeft('', value));
  });

  test('returns the pattern if the subject is empty', () => {
    expect(padLeft('', '0')).toBe('0');
  });

  test('returns the subject if its length is >= the length of the pattern', () => {
    expect(padLeft('4', '0')).toBe('4');
    expect(padLeft('42', '0')).toBe('42');
  });

  test('pads the subject with the left hand side of the pattern', () => {
    expect(padLeft('4', '00')).toBe('04');
    expect(padLeft('42', '+++')).toBe('+42');
    expect(padLeft('777', '+-?!?')).toBe('+-777');
  });
});
