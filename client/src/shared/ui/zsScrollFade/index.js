// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import each from 'lodash/each';
import throttle from 'lodash/throttle';

export default angular.module('zsScrollFade', []).directive('zsScrollFade', [
  '$window',
  ($window) => {
    return {
      restrict: 'A',
      link: (scope, element /*, attrs*/) => {
        let overflow;

        let getOverflow = () => {
          let top,
            bottom,
            left,
            right,
            el = element[0];

          top = el.scrollTop > 0;
          left = el.scrollLeft > 0;

          bottom = el.scrollTop + el.offsetHeight < el.scrollHeight;
          right = el.scrollLeft + el.offsetWidth < el.scrollWidth;

          return {
            top,
            bottom,
            left,
            right,
            some: top || left || bottom || right,
          };
        };

        let setOverflowClasses = () => {
          each(overflow, (value, key) => {
            let className = `fade-overflow-${key}`;

            element[value ? 'addClass' : 'removeClass'](className);
          });
        };

        let checkChange = () => {
          let currentOverflow = getOverflow();

          if (!angular.equals(currentOverflow, overflow)) {
            overflow = currentOverflow;
            setOverflowClasses();
          }
        };

        let onResize = () => {
          checkChange();
        };

        let onScroll = () => {
          checkChange();
        };

        $window.addEventListener('resize', onResize);
        element.bind('scroll', throttle(onScroll, 150));

        scope.$watch(checkChange);

        scope.$on('$destroy', () => {
          $window.removeEventListener('resize', onResize);
        });
      },
    };
  },
]).name;
