// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import once from 'lodash/once';
import snackbarServiceModule from './../zsSnackbar/snackbarService';
import get from 'lodash/get';
import './styles.scss';

export default angular
  .module('zsPdfViewer', [snackbarServiceModule])
  .directive('zsPdfViewer', [
    '$rootScope',
    '$q',
    'snackbarService',
    ($rootScope, $q, snackbarService) => {
      let load = once(() => {
        return $q((resolve /*, reject*/) => {
          require([
            'pdfjs-dist/build/pdf.combined.js',
            'pdfjs-dist/web/pdf_viewer',
          ], (...js) => {
            $rootScope.$evalAsync(() => {
              resolve(js);
            });
          });
        });
      });

      return {
        restrict: 'E',
        template,
        scope: {
          url: '@',
        },
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function (scope, element) {
            let ctrl = this,
              loadingLib,
              loadingDoc,
              pdfDocument,
              PDFJS,
              viewer;

            let loadUrl = (url) => {
              loadingDoc = true;

              PDFJS.getDocument({
                url,
                // Firefox doesn't pass an accept header,
                // causing the service worker to intercept it
                // as a text/html request
                httpHeaders: {
                  Accept: '*/*',
                  'ZS-PDF-Viewer': '1',
                },
              }).then(
                (pdfDoc) => {
                  loadingDoc = false;

                  pdfDocument = pdfDoc;

                  return viewer.setDocument(pdfDoc).then(() => {
                    viewer.currentScaleValue = 'page-width';
                  });
                },
                (err) => {
                  console.error(err);

                  loadingDoc = false;

                  snackbarService.error(
                    'Het document kon niet worden weergegeven. Neem contact op met uw beheerder voor meer informatie.'
                  );
                }
              );
            };

            loadingLib = true;

            load()
              .then((rest) => {
                let container = element[0].querySelector('.pdf-viewer-canvas');

                [PDFJS] = rest;

                viewer = new PDFJS.PDFJS.PDFViewer({
                  container,
                });

                viewer.currentScaleValue = 'page-width';

                if (!scope.$$destroyed) {
                  scope.$watch(
                    () => ctrl.url,
                    () => {
                      loadUrl(ctrl.url);
                    }
                  );
                }
              })
              .catch((err) => {
                console.error(err);

                snackbarService.error(
                  'De PDF viewer kon niet worden geopend. Neem contact op met uw beheerder voor meer informatie.'
                );
              })
              .finally(() => {
                loadingLib = false;
              });

            ctrl.getTotalPages = () => {
              get(pdfDocument, 'numPages');
            };

            ctrl.getCurrentPage = () => {
              get(viewer, 'currentPage');
            };

            ctrl.isLoading = () => loadingLib || loadingDoc;
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
