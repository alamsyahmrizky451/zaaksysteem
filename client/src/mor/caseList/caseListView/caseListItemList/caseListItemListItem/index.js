// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';

import './styles.scss';

export default angular
  .module('Zaaksysteem.mor.caseListItemListItem', [])
  .directive('caseListItemListItem', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseItem: '&',
        },
      };
    },
  ]).name;
