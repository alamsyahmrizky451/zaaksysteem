let context = require.context('file-loader?name=[name].[ext]!./icons', true, /\.png$/),
	icons = context.keys().map(context)
		.map( filename => {

			let size = filename.match(/(\d{3})\.png$/)[1];

			return {
				src: filename,
				sizes: `${size}x${size}`,
				type: 'image/png'
			};
		});

export default {
	icons
};
