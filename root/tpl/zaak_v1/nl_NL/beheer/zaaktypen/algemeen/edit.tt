[% USE Scalar %]
[% USE Dumper %]

[% BLOCK gdpr_boolean_item %]
<tr>
    <td class="td10">
        <input type="checkbox"
               name="node.properties.gdpr.[% class %].[% name %]"
               [% IF params.node.properties.gdpr.$class.$name %]
               checked
               [% END %]
        >
    </td>
    <td class="td300">
        [% label %]
    </td>
<tr>
[% END %]

[% BLOCK gdpr_personal_info %]
<tr>
    <td class="td300">Soort persoonsgegevens</td>
    <td class="td300">
        [% SET class = 'personal' %]
        <table>
            [% PROCESS gdpr_boolean_item
                label = 'Basispersoonsgegevens'
                name  = 'basic_details'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Burgerservicenummer'
                name  = 'personal_id_number'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Inkomensgegevens'
                name  = 'income'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Ras of etnische afkomst'
                name  = 'race_or_ethniticy'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Politieke opvattingen'
                name  = 'political_views'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Religieuze of levensbeschouwende overtuiging'
                name  = 'religion'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Lidmaatschap van een vakbond'
                name  = 'membership_union'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Genetische of biometrische gegevens'
                name  = 'genetic_or_biometric_data'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Gezondheid'
                name  = 'health'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Seksuele identiteit en orientatie'
                name  = 'sexual_identity'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Strafrechtelijke veroordelingen en strafbare feiten'
                name  = 'criminal_record'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Gegevens m.b.t. kind(eren)'
                name  = 'offspring'
            %]
        </table>
    </td>
    <td>
    [% PROCESS widgets/general/tooltip.tt
        message = tooltip %]
    </td>
</tr>
[% END %]

[% BLOCK gdpr_source_personal_info %]
<tr>
    <td class="td300">Bron persoonsgegevens</td>

    <td class="td300">
        [% SET class = 'source_personal' %]
        <table>
            [% PROCESS gdpr_boolean_item
                label = 'Registratie'
                name  = 'registration'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Afzender'
                name  = 'sender'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Ketenpartner'
                name  = 'partner'
            %]
            [% PROCESS gdpr_boolean_item
                label = 'Openbare bron'
                name  = 'public_source'
            %]
        </table>
    </td>
    <td>
    [% PROCESS widgets/general/tooltip.tt
        message = tooltip %]
    </td>
</tr>
[% END %]

[% BLOCK generic_field %]
    <tr>
        <td>[% label %]</td>
        <td>
            <input
                type="text"
                name="node.properties.[% fieldname %]"
                class="input_large"
                value="[% params.node.properties.$fieldname %]"
            />
        </td>
        <td>
            [% PROCESS widgets/general/validator.tt  message = validator_message %]
            [% PROCESS widgets/general/tooltip.tt    message = tooltip_message %]
        </td>
    </tr>
[% END %]


[% BLOCK url_field %]
    <tr>
        <td>[% label %]</td>
        <td class="zaaktypebeheer-algemeen-grondslag">
            <textarea name="[% fieldname %]">[% value %]</textarea>
        </td>
        <td>
            [% PROCESS widgets/general/validator.tt  message = validator_message %]
            [% PROCESS widgets/general/tooltip.tt    message = tooltip_message %]
        </td>
    </tr>
[% END %]


[% BLOCK textarea_field %]
    <tr>
        <td>[% label %]</td>
        <td>
            <textarea
                name="[% fieldname %]"
                class="input_large"
                
            />[% value %]</textarea>
        </td>
        <td>
            [% PROCESS widgets/general/validator.tt  message = validator_message %]
            [% PROCESS widgets/general/tooltip.tt    message = tooltip_message %]
        </td>
    </tr>
[% END %]

[% BLOCK gdpr_dropdown_field %]
    <tr>
        <td>[% label %]</td>
        <td>
            <select name="node.properties.gdpr.[% fieldname %]" class="replace-select">
                 [% FOREACH option IN dropdown_options %]
                    <option
                        value="[% option %]"
                        [% (
                            params.node.properties.gdpr.$fieldname
                                == option
                                    ? 'selected="selected"'
                                    : ''
                    ) %]>[% option %]</option>
                [% END %]
            </select>
        </td>
        <td>
            [% PROCESS widgets/general/validator.tt  message = validator_message %]
            [% PROCESS widgets/general/tooltip.tt    message = tooltip_message %]
        </td>
    </tr>
[% END %]

[% BLOCK generic_dropdown_field %]
    <tr>
        <td>[% label %]</td>
        <td>
            <select name="node.properties.[% fieldname %]" class="replace-select">
                 [% FOREACH option IN dropdown_options %]
                    <option
                        value="[% option %]"
                        [% (
                            params.node.properties.$fieldname
                                == option
                                    ? 'selected="selected"'
                                    : ''
                    ) %]>[% option %]</option>
                [% END %]
            </select>
        </td>
        <td>
            [% PROCESS widgets/general/validator.tt  message = validator_message %]
            [% PROCESS widgets/general/tooltip.tt    message = tooltip_message %]
        </td>
    </tr>
[% END %]

[% BLOCK generic_boolean_field %]
    [% PROCESS generic_dropdown_field
        dropdown_options = ZCONSTANTS.casetype_boolean_property_options.keys.sort.reverse
    %]
[% END %]


<div id="fresh-form" class="ztb">
<form method="POST" action="[% formaction %]" class="zvalidate use_submit ezra_zaaktypebeheer" enctype="multipart/form-data">
    <input type="hidden" name="zaaktype_update" value="1" />
<div class="block clearfix">
    <div class="block-header ztb-header clearfix">
        [% IF(zaaktype_id) %]
            <div class="block-header-title">[% params.node.titel | html_entity %] -  
            [% ELSE %]
                Zaaktype aanmaken
            [% END %]
                <span>Algemene attributen</span>
            </div>
    </div>
    <div class="block-content">
        <h3 class="title">Basisattributen</h3>
        <table class="marginbottom form fixed-layout">
            <tbody>
            <tr>
                <td class="td300">Categorie</td>
                <td class="td300">
                    [% PROCESS widgets/general/categorie.tt 
                    fieldname = 'zaaktype.bibliotheek_categorie_id' %]
                </td>
                <td class="td300">
                    [% PROCESS widgets/general/validator.tt  message = 'Categorie is verplicht' %]
                    [% PROCESS widgets/general/tooltip.tt    message = 'Selecteer hier de juiste categorie.' %]
                </td>
            </tr>
            
            
            
            <tr>
                <td>Naam zaaktype</td>
                <td>
                    <input
                        type="text"
                        name="node.titel"
                        class="input_large"
                        value="[% params.node.titel %]"
                    />
                </td>
                <td>
                    [% PROCESS widgets/general/validator.tt  message = 'De naam van dit zaaktype is verplicht' %]
                    [% PROCESS widgets/general/tooltip.tt    message = 'Typ hier de naam van het zaaktype.' %]
                </td>
            </tr>
                            
            
            
            <tr>
                <td>Identificatie</td>
                <td>
                    <input
                        type="text"
                        name="node.code"
                        class="input_large"
                        value="[% params.node.code %]"
                    />
                </td>
            <td>
                    <div class="validator rounded"><div class="validate-tip"></div><div class="validate-content rounded-right"><span></span></div></div>
                    [% PROCESS widgets/general/tooltip.tt   message = 'Vul hier de unieke code van het zaaktype in. Let op, voor sommige zaaktypen is er een landelijke code beschikbaar (zie de catalogus)' %]                    
                </td>
            </tr>
            <tr data-ng-controller="nl.mintlab.admin.casetype.MotherCaseTypeController" data-ng-init="isCaseTypeMother=[% params.node.properties.is_casetype_mother ? 'true' : 'false' %];caseTypeId='[% zaaktype_id %]';">
                <td><label class="titel">Moederzaaktype</label></td>
                <td>
                    <input type="checkbox" name="isCaseTypeMother" data-ng-model="isCaseTypeMother" data-ng-change="toggleCaseType()"/>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>Speciaal webformulier</td>
                <td>
                    <select name="definitie.custom_webform" class="replace-select">
                            <option
                                value=""
                                [% (
                                    params.definitie.custom_webform
                                        == ''
                                            ? 'selected="selected"'
                                            : '')
                                %]
                            > Geen</option>
                        [% FOREACH webformulier IN ZOPTIONS.CUSTOM_WEBFORMS.keys %]
                            [% webformulier_data = ZOPTIONS.CUSTOM_WEBFORMS.$webformulier %]
                            <option
                                value="[% webformulier %]"
                                [% (
                                    params.definitie.custom_webform
                                        == webformulier
                                            ? 'selected="selected"'
                                            : '')
                                %]
                            > [% webformulier_data.label %]</option>
                        [% END %]
                    </select>
                </td>
                <td>
            <tr>
                <td>Procesbeschrijving</td>
                <td>
                    <input
                        type="text"
                        name="definitie.procesbeschrijving"
                        class="input_large"
                        value="[% params.definitie.procesbeschrijving %]"
                    />
                </td>
                <td>
                    <div class="validator rounded">
                        <div class="validate-tip"></div>
                        <div class="validate-content rounded-right">
                            <span></span> De url van dit zaaktype is verplicht.
                        </div>
                    </div>
                    <div class="tooltip-test-wrap">
                        <div class="tooltip-test rounded">De url van de procesomschrijving van dit zaaktype, bijvoorbeeld http://www.google.nl</div>
                        <div class="tooltip-test-tip"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Handeling initiator</td>
                <td>
                    <select name="definitie.handelingsinitiator" class="replace-select">
                        [% FOREACH hinit IN ZOPTIONS.HANDELINGSINITIATORS.sort %]
                            <option
                                value="[% hinit %]"
                                [% (
                                    params.definitie.handelingsinitiator
                                        == hinit
                                            ? 'selected="selected"'
                                            : ''
                            ) %]>[% c.loc(hinit) %]</option>
                        [% END %]
                    </select>
                </td>
                <td>
                    <div class="validator rounded">
                        <div class="validate-tip"></div>
                        <div class="validate-content rounded-right">
                            <span></span> Handelingsinitiator is verplicht
                        </div>
                    </div>
                    <div class="tooltip-test-wrap">
                        <div class="tooltip-test rounded">De handelingsinititator geeft aan via welke handeling de zaak wordt aangevraagd.</div>
                        <div class="tooltip-test-tip"></div>
                    </div>
                </td>
            </tr>

            [% PROCESS generic_field
                label = 'Aanleiding'
                fieldname = 'aanleiding'
                validator_message = 'Aanleiding'
                tooltip_message = 'Aanleiding'
            %]

            [% PROCESS generic_field
                label = 'Doel'
                fieldname = 'doel'
                validator_message = 'Doel'
                tooltip_message = 'Doel'
            %]

            [% PROCESS generic_field
                label = 'Archiefclassificatiecode'
                fieldname = 'archiefclassicatiecode'
                validator_message = 'Archiefclassificatiecode'
                tooltip_message = 'Archiefclassificatiecode'
            %]

            [% PROCESS generic_dropdown_field
                label = 'Vertrouwelijkheidsaanduiding'
                dropdown_options = ZCONSTANTS.vertrouwelijkheidsaanduiding_options.keys.sort
                fieldname = 'vertrouwelijkheidsaanduiding'
                validator_message = 'Vertrouwelijkheidsaanduiding'
                tooltip_message = 'Vertrouwelijkheidsaanduiding'
            %]

            [% PROCESS generic_field
                label = 'Verantwoordelijke'
                fieldname = 'verantwoordelijke'
                validator_message = 'Verantwoordelijke'
                tooltip_message = 'Verantwoordelijke'
            %]

            [% PROCESS generic_field
                label = 'Verantwoordingsrelatie'
                fieldname = 'verantwoordingsrelatie'
                validator_message = 'Verantwoordingsrelatie'
                tooltip_message = 'Verantwoordingsrelatie'
            %]

            [% PROCESS generic_boolean_field
                label = 'Bezwaar en beroep mogelijk'
                fieldname = 'beroep_mogelijk'
                validator_message = 'Bezwaar en beroep mogelijk'
                tooltip_message = 'Bezwaar en beroep mogelijk'
            %]

            [% PROCESS generic_boolean_field
                label = 'Publicatie'
                fieldname = 'publicatie'
                validator_message = 'Publicatie'
                tooltip_message = 'Publicatie'
            %]

            [% PROCESS textarea_field
                label = 'Publicatietekst'
                fieldname = 'node.properties.publicatietekst'
                value = params.node.properties.publicatietekst
                validator_message = 'Publicatietekst'
                tooltip_message = 'Publicatietekst'
            %]

            [% PROCESS generic_boolean_field
                label = 'BAG'
                fieldname = 'bag'
                validator_message = 'BAG'
                tooltip_message = 'BAG'
            %]

            [% PROCESS generic_boolean_field
                label = 'Lex silencio positivo'
                fieldname = 'lex_silencio_positivo'
                validator_message = 'Lex silencio positivo'
                tooltip_message = 'Lex silencio positivo'
            %]

            [% PROCESS generic_boolean_field
                label = 'Opschorten mogelijk'
                fieldname = 'opschorten_mogelijk'
                validator_message = 'Opschorten mogelijk'
                tooltip_message = 'Opschorten mogelijk'
            %]

            [% PROCESS generic_boolean_field
                label = 'Verlenging mogelijk'
                fieldname = 'verlenging_mogelijk'
                validator_message = 'Verlenging mogelijk'
                tooltip_message = 'Verlenging mogelijk'
            %]

            [% PROCESS generic_field
                label = 'Verlengingstermijn in dagen'
                fieldname = 'verlengingstermijn'
                validator_message = 'Verlengingstermijn in dagen'
                tooltip_message = 'Verlengingstermijn in dagen'
            %]

            [% PROCESS generic_field
                label = 'Verdagingstermijn in dagen'
                fieldname = 'verdagingstermijn'
                validator_message = 'Verdagingstermijn in dagen'
                tooltip_message = 'Verdagingstermijn in dagen'
            %]

            [% PROCESS generic_boolean_field
                label = 'Wet dwangsom'
                fieldname = 'wet_dwangsom'
                validator_message = 'Wet dwangsom'
                tooltip_message = 'Wet dwangsom'
            %]

            [% PROCESS generic_boolean_field
                label = 'WKPB'
                fieldname = 'wkpb'
                validator_message = 'WKPB'
                tooltip_message = 'WKPB'
            %]

            [% PROCESS generic_field
                label = 'e-Formulier'
                fieldname = 'e_formulier'
                validator_message = 'e-Formulier'
                tooltip_message = 'e-Formulier'
            %]
            
            <tr>
                <td>Extra informatie (magic strings)</td>
                <td>
                    <input
                    name="definitie.extra_informatie"
                    value="[% params.definitie.extra_informatie %]"
                    type="text" class="input_large" /></td>
                <td>
                    <div class="validator rounded">
                        <div class="validate-tip"></div>
                        <div class="validate-content rounded-right">
                            <span></span>
                        </div>
                    </div>
                    <div class="tooltip-test-wrap">
                        <div class="tooltip-test rounded">
                            Maak hier eventueel gebruik van extra informatie van een zaak door gebruik te maken van een magic string.
                        </div>
                        <div class="tooltip-test-tip"></div>
                    </div>
                </td>
            </tr>

            <tr>
                <td>Extra informatie Extern (magic strings)</td>
                <td>
                    <input
                    name="definitie.extra_informatie_extern"
                    value="[% params.definitie.extra_informatie_extern %]"
                    type="text" class="input_large" /></td>
                <td>
                    <div class="validator rounded">
                        <div class="validate-tip"></div>
                        <div class="validate-content rounded-right">
                            <span></span>
                        </div>
                    </div>
                    <div class="tooltip-test-wrap">
                        <div class="tooltip-test rounded">
                            Maak hier eventueel gebruik van extra informatie van een zaak door gebruik te maken van een magic string.
                        </div>
                        <div class="tooltip-test-tip"></div>
                    </div>
                </td>
            </tr>

            <tr>
                <td>Trefwoorden</td>
                <td>
                    <input
                        type="text"
                        name="node.zaaktype_trefwoorden"
                        class="input_large"
                        value="[% params.node.zaaktype_trefwoorden %]"
                    />
                </td>
                <td>
                    <div class="validator rounded">
                        <div class="validate-tip"></div>
                        <div class="validate-content rounded-right">
                            <span></span> De url van dit zaaktype is verplicht.
                        </div>
                    </div>
                    <div class="tooltip-test-wrap">
                        <div class="tooltip-test rounded">Geef trefwoorden op zodat je er later op kunt zoeken om bij dit zaaktype uit kan komen</div>
                        <div class="tooltip-test-tip"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Omschrijving of toelichting</td>
                <td>
                    <input
                        type="text"
                        name="node.zaaktype_omschrijving"
                        class="input_large"
                        value="[% params.node.zaaktype_omschrijving %]"
                    />
                </td>
                <td>
                    <div class="validator rounded">
                        <div class="validate-tip"></div>
                        <div class="validate-content rounded-right">
                            <span></span> De url van dit zaaktype is verplicht.
                        </div>
                    </div>
                    <div class="tooltip-test-wrap">
                        <div class="tooltip-test rounded">Geef een omschrijving van dit zaaktype</div>
                        <div class="tooltip-test-tip"></div>
                    </div>
                </td>
            </tr>

            [% PROCESS textarea_field
                label = 'Wettelijke grondslag'
                fieldname = 'definitie.grondslag'
                value = params.definitie.grondslag
                validator_message = 'Wettelijke grondslag'
                tooltip_message = 'De grondslag geeft aan op welke gronden dit zaaktype bestaat, bijvoorbeeld een bepaalde Wet of verordening.'
            %]

            [% PROCESS textarea_field
                label = 'Lokale grondslag'
                fieldname = 'node.properties.lokale_grondslag'
                value = params.node.properties.lokale_grondslag
                validator_message = 'Lokale grondslag'
                tooltip_message = 'Lokale grondslag'
            %]
        </tbody>
    </table>

    <h3 class="title">AVG</h3>
    <table class="marginbottom form fixed-layout">
        <tbody>
            <tr>
                [% PROCESS gdpr_dropdown_field
                    label = 'Verwerkt persoonsgegevens'
                    fieldname = 'enabled'
                    dropdown_options = [ 'Nee', 'Ja' ]
                    validator_message = 'Verwerkt persoonsgegevens'
                    tooltip_message = 'Het zaaktype verwerkt persoonsgegevens'
                %]
            </tr>

            [%
                PROCESS gdpr_personal_info
                    tooltip = "Verwerkt de volgende persoonsgegevens"
            %]

            [%
                PROCESS gdpr_source_personal_info
                    tooltip = "De bron(nen) van de persooninformatie"
            %]

            [% PROCESS gdpr_dropdown_field
                label = 'Verwerkingstype'
                dropdown_options = [
                    'Delen',
                    'Muteren',
                    'Raadplegen',
                ]
                fieldname = 'processing_type'
                validator_message = 'Verwerkingstype'
                tooltip_message = 'Verwerkingstype'
            %]

            [% PROCESS gdpr_dropdown_field
                label = 'Doorgifte buitenland'
                fieldname = 'process_foreign_country'
                dropdown_options = [ 'Nee', 'Ja' ]
                validator_message = 'Doorgifte buitenland'
                tooltip_message = 'Doorgifte buitenland'
            %]

            [% PROCESS textarea_field
                label = 'Toelichting doorgifte buitenland'
                fieldname = 'node.properties.gdpr.process_foreign_country_reason'
                value = params.node.properties.gdpr.process_foreign_country_reason
                validator_message = 'Toelichting doorgifte buiteland'
                tooltip_message = 'Toelichting doorgifte buiteland'
            %]

            [% PROCESS gdpr_dropdown_field
                label = 'Grondslag verwerking'
                fieldname = 'processing_legal'
                dropdown_options = [
                    'Toestemming',
                    'Overeenkomst',
                    'Wettelijke verplichting',
                    'Publiekrechtelijke taak',
                    'Vitaal belang',
                    'Gerechtvaardigd belang'
                ]
                validator_message = 'Grondslag verwerking'
                tooltip_message = 'Grondslag verwerking'
            %]

            [% PROCESS textarea_field
                label = 'Toelichting verwerking'
                fieldname = 'node.properties.gdpr.processing_legal_reason'
                value = params.node.properties.gdpr.processing_legal_reason
                validator_message = 'Toelichting verwerking'
                tooltip_message = 'Toelichting verwerking'
            %]

        </tbody>
    </table>



    <h3 class="title">Monitoring</h3>

    <table class="fixed-layout">
        <tbody>
                <tr
                    class="ignore-field-definitie.afhandeltermijn_type">
                    <td class="td300">Doorlooptijd wettelijk</td>
                    <td class="td300">
                        [% PROCESS widgets/general/terms.tt
                            terms_name  = "definitie.afhandeltermijn"
                            terms_value = params.definitie.afhandeltermijn
                            terms_name_type = "definitie.afhandeltermijn_type"
                            terms_name_type_value = params.definitie.afhandeltermijn_type
                        %]
                    </td>
                    <td class="td300">
                        <div class="validator rounded">
                            <div class="validate-tip"></div>
                            <div class="validate-content rounded-right">
                                <span></span>
                            </div>
                        </div>
                        <div class="tooltip-test-wrap">
                            <div class="tooltip-test rounded">
                                Wettelijke afhandeltermijn. Deze termijn wordt toegepast voor het bepalen van de dagen die nog over zijn.
                            </div>
                            <div class="tooltip-test-tip"></div>
                        </div>
                    </td>
                </tr>
                               
                <tr
                    class="ignore-field-definitie.servicenorm_type">
                    <td>Doorlooptijd service</td>
                    <td>
                        [% PROCESS widgets/general/terms.tt
                            terms_name  = "definitie.servicenorm"
                            terms_value = params.definitie.servicenorm
                            terms_name_type = "definitie.servicenorm_type"
                            terms_name_type_value = params.definitie.servicenorm_type
                        %]
                    </td>
                    <td>
                        <div class="validator rounded">
                            <div class="validate-tip"></div>
                            <div class="validate-content rounded-right">
                                <span></span>
                            </div>
                        </div>
                        <div class="tooltip-test-wrap">
                            <div class="tooltip-test rounded">
                                Eigen gewenste afhandeltermijn van de organisatie. Deze kan dus afwijken van de wettelijke termijn.
                             </div>
                            <div class="tooltip-test-tip"></div>
                        </div>
                    </td>
                </tr>

                
            </tbody>
        </table>

  
        <div class="form-actions form-actions-sticky clearfix">

            <input type="button" name="goback" value="Annuleren" class="go_back button-secondary button left" />
            
            [% PROCESS beheer/zaaktypen/voortgang.tt %]

            <input type="submit" value="Volgende" class="button-primary button right" />
            <input name="destination" type="hidden" value="" />
        </div>


    </div>
</div>
</form>
</div>
