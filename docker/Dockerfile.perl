# Please see dev-bin/docker-perl for building and pushing this base
# layer

FROM perl:slim-stretch

ENV DEBIAN_FRONTEND=noninteractive \
    NO_NETWORK_TESTING=1

RUN groupadd -g 1001 zaaksysteem \
    && useradd -ms /bin/bash -u 1001 -g 1001 zaaksysteem -d /opt/zaaksysteem \
    && chown -R zaaksysteem /opt/zaaksysteem

COPY dev-bin/cpanm .

COPY cpanfile /opt/zaaksysteem/cpanfile
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
      desktop-file-utils \
      gcc \
      git \
      ca-certificates \
      gpg \
      libc6-dev \
      libexpat1-dev \
      libmagic-dev \
      libpq-dev \
      #libssl-dev \
      openssl \
      zlib1g-dev \
# libssl1.0-dev is required, because Crypt::OpenSSL::{X509,RSA,VerifyX509} do not
# support libssl1.1 yet.
# It can be removed once
# https://github.com/dsully/perl-crypt-openssl-x509/issues/53 and related bugs
# for the other projects are fixed.
      libssl1.0-dev \
      libxml2-dev \
      locales \
      poppler-utils \
      shared-mime-info \
      unzip \
      uuid-dev \
      xmlsec1 \
      curl \
      pdftk \
  && localedef -i nl_NL -c -f UTF-8 \
    -A /usr/share/locale/locale.alias nl_NL.UTF-8 \
# Zaaksysteem's own modules and deps related to those
  && ./cpanm --notest IPC::System::Simple \
  && ./cpanm File::ShareDir::Install \
# IO::Socket::SSL borks when Net::SSLeay isn't installed
  && ./cpanm Net::SSLeay~'>=1.46' \
  && ./cpanm LWP::Protocol::https \
  && ./cpanm DBIx::Class \
  && ./cpanm https://gitlab.com/zaaksysteem/bttw-tools.git@v0.008 \
  && ./cpanm https://gitlab.com/zaaksysteem/syzygy.git@v0.004 \
  && ./cpanm https://gitlab.com/zaaksysteem/zaaksysteem-instance_config.git \
# CPAN modules with issues
  &&  ./cpanm Catalyst::Runtime \
# Systems under load may not have accurate timing differences, and thus test
# fail. It has no dependencies, but just in case.
  && ./cpanm --installdeps Time::Warp \
  && ./cpanm --notest      Time::Warp \
# Some builds failed due to xs errors and the build after it succeeds
# but consecutive builds after that broke due to timing out the build by
# taking longer than half an hour for it to be tested. Unsure what is
# hapenning, running it with --notest seems to fix this timeout issue
  && ./cpanm --installdeps Net::AMQP::RabbitMQ \
  && ./cpanm --notest      Net::AMQP::RabbitMQ \
# Class::C3 installed because there is some dependency resolution thing
# with Catalyst::Plugin::Params::Profile in the cpanfile.
  && ./cpanm Class::C3 \
# GnuPG::Interface requires patches which are found in the bug report,
# but haven't been merged upstream, so we don't require the testsuite
# right now: https://rt.cpan.org/Public/Bug/Display.html?id=102651
  && ./cpanm --installdeps GnuPG::Interface \
  && ./cpanm --notest      GnuPG::Interface \
# 2018-01-11: Compile::WSDL11 is a dependency of Catalyst::Controller::SOAP
# but it has not been declared in the META.json. we potentionally could end up
# that the dependecy tree does not notice it and starts building C::C::SOAP
# before Compile::WSDL11
# https://rt.cpan.org/Public/Bug/Display.html?id=95327
  && ./cpanm XML::Compile::WSDL11 \
# Make sure CGI::Simple is installed at a version which deals with it
# dependencies
  && ./cpanm CGI::Simple~'>=1.14' \
# 2018-03-08: Catalyst::Plugin::Static::Simple has a testsuite bug. See
# https://rt.cpan.org/Public/Bug/Display.html?id=124211 for more info on the fault
  && ./cpanm --installdeps Catalyst::Plugin::Static::Simple \
  && ./cpanm --notest      Catalyst::Plugin::Static::Simple \
  && ./cpanm Class::Accessor::Fast \
# A newer version of Crypt::OpenSSL::RSA has been released on may 31st
# 2018. This breaks Net::SAML2. Force it at 0.28 for the time being
  && ./cpanm "Crypt::OpenSSL::RSA@0.28" \
# Manual addition of a module that isn't indexed on CPAN and creates
# issues when you build via a local mirror, dep of Net::OpenStack::Swift
  && curl -q \
        http://mirror.nl.leaseweb.net/CPAN/authors/id/M/MK/MKODERER/Sys-CPU-0.52.tar.gz \
        --output Sys-CPU-0.52.tar.gz \
    && echo "34305423e86cfca9a631b6f91217f90f  Sys-CPU-0.52.tar.gz" \
        | md5sum -c - \
    && ./cpanm Sys-CPU-0.52.tar.gz \
# CPAN modules via cpanm
  && ./cpanm --installdeps /opt/zaaksysteem \
# Cleanup
  && apt-get purge -yqq \
        gcc \
        libc6-dev\
        libexpat1-dev \
        libmagic-dev \
        libpq-dev \
        libssl-dev \
        libssl1.0-dev \
        libxml2-dev \
        uuid-dev\
        zlib1g-dev \
        curl \
  && apt-get --no-install-recommends -y install \
     libpq5 \
     libssl1.0.2 \
     libmagic1 \
  && apt-get autoremove --purge -yqq\
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/* ~/.cpanm \
  # 2018-06-29: We'll use Pandoc for converting documents.
  # luckely we can use a nice CPAN module
  && perl -MPandoc::Release -E 'get(q{2.2.1}, verbose => 1)->download(bin => q{/opt/local/pandoc}, verbose => 1)->symlink(q{/usr/bin}, verbose => 1)'
