#!/usr/bin/perl
#
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Try::Tiny;
use Time::HiRes qw(gettimeofday tv_interval);
use Zaaksysteem::CLI;
use Log::Log4perl;

my $cli = try {
    Zaaksysteem::CLI->init;
}
catch {
    die $_;
};

my $logger = Log::Log4perl->get_logger('touch_filestore');

my %opt = %{ $cli->options };

if ($opt{filestore_id} && $opt{offset}) {
    $logger->logdie("Conflicting options found: filestore_id and offset");
}

my $args = {};
if (exists $opt{filestore_id}) {
    $args = { 'me.id' => $opt{filestore_id} };
}

my $rs = $cli->schema->resultset('Filestore')->search(
    $args,
    {
        order_by => { -desc => 'me.id' },
        $opt{offset} ? (offset => $opt{offset}) : (),
        $opt{limit}  ? (rows   => $opt{limit}) : (),
    }
);

my $count = $rs->count();
if (!$count) {
    $logger->log->info("No (matching) filestore rows found");
    exit 0;
}

my $files_done = 0;

my $starttime  = [gettimeofday];
while (my $fs = $rs->next) {
    $cli->do_transaction(
        sub {
            my $self   = shift;
            my $schema = shift;

            $files_done++;

            $self->log->info(sprintf("Replicating file %d\n", $fs->id));
            $fs->replicate();
        }
    );
}

1;

__END__

=head1 NAME

touch_filestore.pl - A filestore toucher/replication tool

=head1 SYNOPSIS

touch_filestore.pl OPTIONS [ -o filestore_id=1234 ] || [ -o offset=2 ]

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * o

You can use this specify an option.

=back

=over 8

=item * case

You can use this to update an specific case, not supplying this will touch all cases. If you supply 'emtpy' you will touch empty cases.

=item * offset

In case you want to touch all cases but want to start with a certain offset. This is handy in case the process get's killed and you want to continue the run.
This option conflicts with the case option.

=item * limit

Limits the number of cases touched. Can be combined with C<offset> to touch all cases in
batches.

=item * casetype

Include the casetype relation update for a case.

=back

=over

=item * no-dry

Run it and commit the changes. The default is NOT to commit any changes.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
