BEGIN;

UPDATE bibliotheek_notificaties

SET search_term = CONCAT(email_template.label, ' ', email_template.subject,' ', email_template.message, ' ', email_template.uuid)

FROM bibliotheek_notificaties as email_template

WHERE email_template.id = bibliotheek_notificaties.id AND email_template.search_term IS NULL;

COMMIT;
