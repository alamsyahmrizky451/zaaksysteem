BEGIN;

  DROP INDEX IF EXISTS transaction_record_transaction_id_idx;

  CREATE INDEX
    transaction_record_transaction_id_idx
  ON
    public.transaction_record
  USING
    btree(transaction_id);

COMMIT;
