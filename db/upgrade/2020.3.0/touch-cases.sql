BEGIN;


  INSERT INTO queue (type, label, priority, metadata, data)
  SELECT 'touch_case_v0', 'Performance touch fixes', 950, '{"require_object_model":1, "disable_acl": 1, "target":"backend"}', json_build_object('case_number', id) from zaak where last_modified > '2019-12-23';


COMMIT;
