BEGIN;

    CREATE INDEX IF NOT EXISTS object_data_date_modified ON object_data(CAST(hstore_to_timestamp(index_hstore->'object.date_modified'::text) AS DATE));

COMMIT;
