BEGIN;

UPDATE zaaktype_node SET properties = '{}' WHERE properties IS NULL;
ALTER TABLE zaaktype_node ALTER COLUMN properties SET DEFAULT '{}';
ALTER TABLE zaaktype_node ALTER COLUMN properties SET NOT NULL;

COMMIT;
