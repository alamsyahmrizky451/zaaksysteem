package Zaaksysteem::Test::Object::ConstantTables;

use warnings;
use strict;

use Zaaksysteem::Test;

use BTTW::Tools qw[assert_profile];
use MooseX::Types::Moose qw[Bool];
use Zaaksysteem::Object::ConstantTables qw[MUNICIPALITY_TABLE];
use Zaaksysteem::Types qw[NonEmptyStr UUID];

sub test_municipality_code_table {
    note 'Validating ConstantTables::MUNICIPALITY_TABLE entries';

    for my $entry (@{ MUNICIPALITY_TABLE() }) {
        lives_ok {
            assert_profile($entry, profile => {
                required => {
                    id => UUID,
                    label => NonEmptyStr,
                    dutch_code => 'Int'
                },
                optional => {
                    historical => 'ScalarRef'
                }
            });

            Bool->assert_valid(${ $entry->{ historical } })
        } sprintf('Entry "%s" OK', $entry->{ label });
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
