package Zaaksysteem::Test::Zaken::Roles::ZaakSetup;
use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Zaken::Betrokkenen - Test Zaaksysteem::Zaken::Betrokkenen

=head1 DESCRIPTION

Tests for the old "betrokkenen" handling on the database "zaak" object.

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Zaken::Betrokkenen

=cut

use Zaaksysteem::Test;

sub test__update_case_document_zaaktype_node_ids {
    my %tests_executed;
    my $mock_schema = mock_one(
        resultset => sub {
            my $type = shift;
            if ($type eq 'ZaaktypeKenmerken') {
                return mock_one(
                    search_rs => sub {
                        my $search_params = shift;

                        $tests_executed{zaaktypekenmerken_search_rs} //= 0;
                        $tests_executed{zaaktypekenmerken_search_rs}++;
                        cmp_deeply(
                            $search_params,
                            {
                                bibliotheek_kenmerken_id => Test::Deep::any(122, 123, 124, 125),
                                zaaktype_node_id => 31337,
                            },
                            'ZaaktypeKenmerken search performed correctly'
                        );

                        return mock_one(
                            first => sub {
                                if ($search_params->{bibliotheek_kenmerken_id} != 125) {
                                    return mock_one(
                                        id => sub { return 42; }
                                    );
                                } else {
                                    return;
                                }
                            }
                        );
                    },
                );
            } else {
                die "Unknown resultset requested: $type";
            }
        },
    );

    my @mock_files = (
        # File with no case-document labels
        mock_one(
            case_documents => sub {
                return mock_one(
                    all => sub { return; }
                );
            }
        ),
        # File with one case-document label
        mock_one(
            case_documents => sub {
                return mock_one(
                    all => sub {
                        return (
                            mock_one(
                                case_document_id => sub {
                                    return mock_one(
                                        get_column => sub {
                                            my $column = shift;
                                            die "Unknown column $column" if $column ne 'bibliotheek_kenmerken_id';
                                            return 122;
                                        }
                                    )
                                },
                                delete => sub { fail("Changed-but-existing case document label should not be deleted (122)"); },
                                update => sub {
                                    my $update_params = shift;
                                    cmp_deeply(
                                        $update_params,
                                        { case_document_id => 42 },
                                        "Document label updated correctly to the one from the new casetyle version (122)"
                                    );
                                    $tests_executed{case_document_update} //= 0;
                                    $tests_executed{case_document_update}++;
                                }
                            ),
                        )
                    }
                );
            }
        ),
        # File with more than one case-document label, one of which got deleted
        mock_one(
            case_documents => sub {
                return mock_one(
                    all => sub {
                        return (
                            mock_one(
                                case_document_id => sub {
                                    return mock_one(
                                        get_column => sub {
                                            my $column = shift;
                                            die "Unknown column $column" if $column ne 'bibliotheek_kenmerken_id';
                                            return 123;
                                        }
                                    )
                                },
                                delete => sub { fail("Changed-but-existing case document label should not be deleted (123)"); },
                                update => sub {
                                    my $update_params = shift;
                                    cmp_deeply(
                                        $update_params,
                                        { case_document_id => 42 },
                                        "Document label updated correctly to the one from the new casetyle version (123)"
                                    );
                                    $tests_executed{case_document_update} //= 0;
                                    $tests_executed{case_document_update}++;
                                }
                            ),
                            mock_one(
                                case_document_id => sub {
                                    return mock_one(
                                        get_column => sub {
                                            my $column = shift;
                                            die "Unknown column $column" if $column ne 'bibliotheek_kenmerken_id';
                                            return 124;
                                        }
                                    )
                                },
                                delete => sub { fail("Changed-but-existing case document label should not be deleted (124)"); },
                                update => sub {
                                    my $update_params = shift;
                                    cmp_deeply(
                                        $update_params,
                                        { case_document_id => 42, },
                                        "Document label updated correctly to the one from the new casetyle version (124)"
                                    );
                                    $tests_executed{case_document_update} //= 0;
                                    $tests_executed{case_document_update}++;
                                }
                            ),
                            mock_one(
                                case_document_id => sub {
                                    return mock_one(
                                        get_column => sub {
                                            my $column = shift;
                                            die "Unknown column $column" if $column ne 'bibliotheek_kenmerken_id';
                                            return 125;
                                        }
                                    )
                                },
                                update => sub { fail("Removed case document label should not be updated but deleted (125)"); },
                                delete => sub {
                                    $tests_executed{case_document_delete} //= 0;
                                    $tests_executed{case_document_delete}++;
                                    pass("->delete called on case document label that no longer exists (125)");
                                },
                            ),
                        )
                    }
                );
            }
        ),
    );

    my $mock_rs = mock_one(
        next => sub {
            my $file = shift @mock_files;
            return $file;
        }
    );

    my $mock = Zaaksysteem::Test::Zaken::SetupZaak::MockZaak->new();

    $mock->_update_case_document_zaaktype_node_ids(
        $mock_schema,
        $mock_rs,
        31337,
    );

    cmp_deeply(
        \%tests_executed,
        {
            'case_document_delete' => 1,
            'case_document_update' => 3,
            'zaaktypekenmerken_search_rs' => 4
        },
        "All expected tests executed",
    );
}

package Zaaksysteem::Test::Zaken::SetupZaak::MockZaak {
    use Moose;
    with 'MooseX::Log::Log4perl', 'Zaaksysteem::Zaken::Roles::ZaakSetup';

    __PACKAGE__->meta->make_immutable();
}


__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

