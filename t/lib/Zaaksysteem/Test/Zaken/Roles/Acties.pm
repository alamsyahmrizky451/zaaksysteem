package Zaaksysteem::Test::Zaken::Roles::Acties;

use Zaaksysteem::Test;
use Zaaksysteem::Zaken::Roles::Acties;

=head1 NAME

Zaaksysteem::Test::Zaken::Roles::Acties - Test for the "Acties" role for Zaak instances

=head1 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Zaken::Roles::Acties

=cut

sub test_update_streefafhandeldatum {

    my $now = DateTime->now;
    my $days = 4;

    my $zaak = Zaaksysteem::Test::MockZaak->new(
        status              => "stalled",
        stalled_since       => $now->clone()->subtract(days => $days),
        streefafhandeldatum => $now,
    );

    $zaak->wijzig_status({ status => "new" });

    is($zaak->status, "new", "Status has changed to 'new'");
    cmp_ok(
        $zaak->streefafhandeldatum,
        '>=',
        $now->clone()->add(days => $days),
        "Streefafhandeldatum set"
    );
    cmp_ok(
        $zaak->streefafhandeldatum,
        '<=',
        $now->clone()->add(days => $days, seconds => 2),
        ".. and we allow a small variance due to time"
    );
}

sub test_reopen_case {

    my $now = DateTime->now;
    my $days = 4;

    my $zaak = Zaaksysteem::Test::MockZaak->new(
        status              => "resolved",
        stalled_since       => $now->clone()->subtract(days => $days),
        streefafhandeldatum => $now,
        afhandeldatum       => $now,
        vernietigingsdatum  => $now,
        archival_state      => "Blub",
    );

    $zaak->wijzig_status({ status => "open" });

    is($zaak->status, "open", "Status has changed to 'open'");

    my @empty = qw(
        afhandeldatum
        vernietigingsdatum
        archival_state
    );

    foreach (@empty) {
        is($zaak->$_, undef, "$_ is set to undef");
    }

}

package Zaaksysteem::Test::MockZaak {
    use Moose;
    use Zaaksysteem::Types qw/DateTimeObject/;
    use Zaaksysteem::Test::Mocks qw/mock_one mock_strict/;
    use Zaaksysteem::Constants;

    with qw(
        Zaaksysteem::Zaken::Roles::Acties
        Zaaksysteem::Zaken::Roles::FaseObjecten
    );

    has status => (
        is => 'rw',
        isa => 'Str',
    );

    has streefafhandeldatum => (
        is => 'rw',
    );

    has afhandeldatum => (
        is => 'rw',
    );

    has vernietigingsdatum => (
        is => 'rw',
    );

    has archival_state => (
        is => 'rw',
    );

    has stalled_since => (
        is => 'rw',
    );

    has stalled_until => (
        is => 'rw',
    );

    has current_deadline => (
        is => 'rw',
    );

    has deadline_timeline => (
        is => 'rw',
    );

    sub id { 1 }
    sub behandelaar { 'Henk de Vries' }
    sub coordinator { 'Henk de Vries' }
    sub trigger_logging { mock_one() }
    sub log { mock_one() }
    sub result_source { mock_one(
        format_datetime_object => sub { shift }
    )}

    sub zaaktype_node_id {
        return mock_strict(
            zaaktype_statussen => {
                search => {
                    first => { status => 2 },
                }
            },
        );
    }

    sub milestone { 2 };

    sub update {
        my $self = shift;
        my $opts = shift;
        foreach (keys %$opts) {
            if (!$self->can($_)) {
                die("Unable to update $_ on case");
                return;
            }
            else {
                $self->$_($opts->{$_});
            }
        }
    }

    sub reden_opschorten { }
    sub discard_changes { }
    sub ddd_update_system_attribute  { };
    sub ddd_update_system_attribute_group  { };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
