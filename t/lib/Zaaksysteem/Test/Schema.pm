package Zaaksysteem::Test::Schema;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::Test::Objects;

sub test_multitenant_methods {

    my $schema = get_schema();

    is($schema->is_multi_tenant, 0, "We are not Multi Tenant");
    is($schema->customer_hostnames, undef, ".. and the ->customer_hostnames are empty");

    my %args = (
        customer_instance => {
            VirtualHosts => {
                'bar.zs.nl' => { customer_id => 'bar' }
            },
            instance_hostname => 'mocked.zs.nl',
        }
    );
    $schema = get_schema(%args);
    is($schema->is_multi_tenant, 0, "We are not Multi Tenant with only one hostname");
    is($schema->customer_hostnames, undef, ".. and the ->customer_hostnames are empty");

    $args{customer_instance}{VirtualHosts}{'foo.zs.nl'} = { customer_id => 'bar' };

    is($schema->is_multi_tenant, 1, "We are Multi Tenant");
    cmp_deeply(
        $schema->customer_hostnames,
        [qw(bar.zs.nl foo.zs.nl mocked.zs.nl)],
        ".. and the ->customer_hostnames are correctly set"
    );

}

sub test_clearers {
    my %attrs = (
        betrokkene_pager         => 'foo',
        cache                    => 'foo',
        catalyst_config          => 'foo',
        current_user_ou_ancestry => 'foo',
        current_user             => 'foo',
        customer_instance        => {
            VirtualHosts => {
                'foo.zs.nl' => { customer_id => 'foo' },
                'bar.zs.nl' => { customer_id => 'bar' }
            },
            instance_hostname => 'mocked.zs.nl',
        },
    );

    my $schema = get_schema(%attrs);

    my @cleared = keys %attrs;

    # These are set based on customer_instance values
    push(@cleared, qw(customer_config));

    foreach (@cleared) {
        $schema->$_; # Trigger value setting

        my $attr = $schema->meta->find_attribute_by_name($_);
        ok($attr->has_value($schema), "$_ has a value");
    }

    lives_ok(sub {$schema->clear}, "Clearing the schema object");

    foreach (@cleared) {
        my $attr = $schema->meta->find_attribute_by_name($_);
        ok(!$attr->has_value($schema), "$_ does not have a value");
    }

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Schema - Test methods on the L<Zaaksysteem::Schema> object

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Schema

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
